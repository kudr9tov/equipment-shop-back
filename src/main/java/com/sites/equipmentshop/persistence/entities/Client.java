package com.sites.equipmentshop.persistence.entities;

import com.sites.equipmentshop.endpoints.dto.OrderCustomerDTO;
import com.sites.equipmentshop.endpoints.dto.PriceRequestDTO;

public class Client {
    private String name;
    private String phone;
    private String email;

    public Client(PriceRequestDTO requestDTO) {
        this.name = requestDTO.getName();
        this.phone = requestDTO.getPhone();
        this.email = requestDTO.getEmail();
    }
    public Client(OrderCustomerDTO orderCustomerDTO) {
        this.name = orderCustomerDTO.getName();
        this.phone = orderCustomerDTO.getPhone();
        this.email = orderCustomerDTO.getEmail();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
