package com.sites.equipmentshop.endpoints.dto;

import org.apache.commons.compress.utils.Lists;

import java.util.List;

public class EmailsFilterDTO {
    private List<FilterDTO> filters = Lists.newArrayList();
    private Integer limit = 25; //default
    private Integer page = 1;

    public List<FilterDTO> getFilters() {
        return filters;
    }

    public void setFilters(List<FilterDTO> filters) {
        this.filters = filters;
    }

    public Integer getLimit() {
        return Math.max(limit, 0);
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getRightPage() {
        return Math.max(getPage() - 1, 0);
    }

    public Integer getPage() {
        return Math.max(page, 1);
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
