package com.sites.equipmentshop.service.scrappers;

import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.endpoints.dto.SiteScrappingAssociationDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.models.association.Action;
import com.sites.equipmentshop.service.scrappers.models.association.SiteScrappingAssociation;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.AbatAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.AtesyAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.CrispyAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.HaierproffAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.HurakanAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.MariholodAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.MetalCityAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.PolairAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.PolusAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.PrometAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.scrapper.impl.TDAbstractScrapperImpl;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class ScrapperService {

    final NamedParameterJdbcTemplate productService;
    final FTPUtils ftpUtils;
    final ScrapperDAO scrapperDAO;
    private AbstractScrapper abstractScrapper;
    private final ExecutorService executorService = Executors.newFixedThreadPool(7);
    private final Map<Site, Future<?>> currentExecutions = Maps.newConcurrentMap();

    @Autowired
    public ScrapperService(NamedParameterJdbcTemplate productService,
                           ScrapperDAO scrapperDAO,
                           FTPUtils ftpUtils) {
        this.productService = productService;
        this.ftpUtils = ftpUtils;
        this.scrapperDAO = scrapperDAO;
    }

    public void runScrapper(ScrapperDTO scrapperDTO) {
        switch (scrapperDTO.getSite()) {
            case POLAIR:
                abstractScrapper = new PolairAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case ABAT:
                abstractScrapper = new AbatAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case MARIHOLOD:
                abstractScrapper = new MariholodAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case CRISPY:
                abstractScrapper = new CrispyAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case ATESY:
                abstractScrapper = new AtesyAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case POLUS:
                abstractScrapper = new PolusAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case PROMET:
                abstractScrapper = new PrometAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case HURAKAN:
                abstractScrapper = new HurakanAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case TD:
                abstractScrapper = new TDAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case METALCITY:
                abstractScrapper = new MetalCityAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
            case HAIER:
                abstractScrapper = new HaierproffAbstractScrapperImpl(productService, ftpUtils, scrapperDAO, scrapperDTO);
                break;
        }

        Optional.of(abstractScrapper).ifPresent(present -> {
            final Site site = scrapperDTO.getSite();
            if (!currentExecutions.containsKey(site)) {
                final Future<?> submit = executorService.submit(() -> {
                    present.startJob();
                    currentExecutions.remove(site);
                });
                currentExecutions.put(site, submit);
            }
        });
    }

    public void stopScrapper(Site site) {
        final Future<?> future = currentExecutions.get(site);
        if (Objects.nonNull(future)) {
            future.cancel(Boolean.TRUE);
            currentExecutions.remove(site);
        }
    }

    public String getAllAssociations() {
        return scrapperDAO.getAllAssociations();
    }

    public String getAssociations(Integer siteId) {
        return scrapperDAO.getAssociations(siteId);
    }

    public String getExecutionTasks(Integer siteId) {
        return scrapperDAO.getExecutionTasks(siteId);
    }

    public String getSites() {
        return scrapperDAO.getSites();
    }

    public String updateAssociations(SiteScrappingAssociationDTO dto) {
        if (CollectionUtils.isNotEmpty(dto.getAssociations())) {

            final List<SiteScrappingAssociation> excluded = dto.getAssociations()
                    .stream()
                    .filter(SiteScrappingAssociation::getExclude)
                    .collect(Collectors.toList());

            final Map<Action, List<SiteScrappingAssociation>> actions = dto.getAssociations()
                    .stream()
                    .filter(f -> !f.getExclude())
                    .collect(Collectors.groupingBy(SiteScrappingAssociation::getAction));

            if (CollectionUtils.isNotEmpty(actions.get(Action.INSERT))) {
                scrapperDAO.saveAssociations(actions.get(Action.INSERT));
            }
            if (CollectionUtils.isNotEmpty(actions.get(Action.UPDATE))) {
                scrapperDAO.updateAssociations(actions.get(Action.UPDATE));
            }
            if (CollectionUtils.isNotEmpty(actions.get(Action.REMOVE))) {
                scrapperDAO.deleteAssociations(actions.get(Action.REMOVE));
            }
            if (CollectionUtils.isNotEmpty(excluded)) {
                scrapperDAO.excludeCategory(dto.getSiteId(), excluded);
            }

        }
        return scrapperDAO.getAssociations(dto.getSiteId());
    }
}
