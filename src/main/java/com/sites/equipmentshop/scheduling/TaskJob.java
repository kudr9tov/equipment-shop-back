package com.sites.equipmentshop.scheduling;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sites.equipmentshop.endpoints.dto.EmailDTO;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.service.EmailService;
import com.sites.equipmentshop.service.SchedulingService;
import com.sites.equipmentshop.service.enums.ScheduleJobType;
import com.sites.equipmentshop.service.scrappers.ScrapperService;
import org.apache.commons.lang3.EnumUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Component
public class TaskJob implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskJob.class);

    private ScrapperService scrapperService;
    private EmailService emailService;

    @Override
    public void execute(JobExecutionContext context) {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        final JobDetail jobDetail = context.getJobDetail();
        JobKey key = jobDetail.getKey();
        LOGGER.info("Scheduled task {} appears in job", key);

        final JobDataMap jobDataMap = jobDetail.getJobDataMap();
        String parameters = jobDataMap.getString(SchedulingService.PROPERTIES_KEY);
        ScheduleJobType app = EnumUtils.getEnumIgnoreCase(ScheduleJobType.class, jobDataMap.getString(SchedulingService.APP_KEY));
        try {
            if (ScheduleJobType.SCRAPPER.equals(app)) {
                scrapperService.runScrapper(new ObjectMapper().readValue(parameters, ScrapperDTO.class));
            } else if (ScheduleJobType.MAIL.equals(app)) {
                emailService.sendTemplateToRecipients(new ObjectMapper().readValue(parameters, EmailDTO.class));
            }
        } catch (JsonProcessingException e) {
            LOGGER.error("Can't run job", e);
        }
    }


    @Autowired
    public void setScrapperService(ScrapperService scrapperService) {
        this.scrapperService = scrapperService;
    }

    @Autowired
    public void setScrapperService(EmailService emailService) {
        this.emailService = emailService;
    }
}
