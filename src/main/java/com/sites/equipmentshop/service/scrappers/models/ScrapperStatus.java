package com.sites.equipmentshop.service.scrappers.models;

public enum ScrapperStatus {
    IN_QUEUE, RUNNING, COMPLETED, FAILED
}
