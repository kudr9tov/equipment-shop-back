package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.exception.ProductNotFoundException;
import com.sites.equipmentshop.persistence.entities.Product;
import com.sites.equipmentshop.service.ProductService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/products", consumes = {"*/*", "*", "application/json" }, produces = {"*/*", "application/json"})
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getProducts(@RequestParam(value = "limit", defaultValue = "15") Integer limit,
                                              @RequestParam(value = "skip", defaultValue = "0") Integer offset,
                                              @RequestParam(value = "filter", required = false) String filter) {
        return ResponseEntityUtils.responseEntity(productService.getAllProducts(limit, offset, filter));
    }

    @GetMapping(value = "errors")
    public ResponseEntity<Object> getErrorProducts(@RequestParam(value = "limit", defaultValue = "15") Integer limit,
                                                   @RequestParam(value = "skip", defaultValue = "0") Integer offset) {
        return ResponseEntityUtils.responseEntity(productService.getErrorProducts(limit, offset));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getProducts(@PathVariable Integer id) {
        return ResponseEntityUtils.responseEntity(productService.getProduct(id));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteProduct(@PathVariable Integer id){
        productService.deleteProductById(id);
    }

    @PostMapping()
    public ResponseEntity<Object> createProduct(@RequestBody Product productDTO){
        return ResponseEntityUtils.responseEntity(productService.createProduct(productDTO));
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public void emailExists(HttpServletResponse response, ProductNotFoundException ex) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(), ex.getMessage());
    }

}
