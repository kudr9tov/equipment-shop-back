package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.endpoints.dto.SiteScrappingAssociationDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.service.scrappers.ScrapperService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/scrapper", consumes = {"*/*", "*", "application/json"}, produces = {"*/*", "application/json"})
public class ScrapperController {

    private final ScrapperService scrapperService;

    @Autowired
    public ScrapperController(ScrapperService scrapperService) {
        this.scrapperService = scrapperService;
    }

    @PostMapping(value = "/run")
    public void runScrapper(@RequestBody ScrapperDTO scrapperDTO) {
        scrapperService.runScrapper(scrapperDTO);
    }

    @PostMapping(value = "/stop")
    public void stopScrapper(@RequestBody ScrapperDTO scrapperDTO) {
        scrapperService.stopScrapper(scrapperDTO.getSite());
    }

    @GetMapping(value = "/association")
    public ResponseEntity<Object> getAssociations(@RequestParam(value = "siteId", required = false) Integer siteId) {
        if (Objects.isNull(siteId)) {
            return ResponseEntityUtils.responseEntity(scrapperService.getAllAssociations());
        }
        return ResponseEntityUtils.responseEntity(scrapperService.getAssociations(siteId));
    }

    @PostMapping(value = "/association/update")
    public ResponseEntity<Object> getAssociations(@RequestBody SiteScrappingAssociationDTO dto) {
        return ResponseEntityUtils.responseEntity(scrapperService.updateAssociations(dto));
    }

    @GetMapping(value = "/executions")
    public ResponseEntity<Object> getExecutionTasks(@RequestParam(value = "siteId") Integer siteId) {
        return ResponseEntityUtils.responseEntity(scrapperService.getExecutionTasks(siteId));
    }

    @GetMapping(value = "/sites")
    public ResponseEntity<Object> getSites() {
        return ResponseEntityUtils.responseEntity(scrapperService.getSites());
    }


}
