package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.support.FileUploadResponse;
import com.sites.equipmentshop.support.UploadService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(APIConstants.API_FILES)
public class FilesController {

    private final UploadService uploadService;

    @Autowired
    public FilesController(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @PostMapping(value = "/upload")
    public FileUploadResponse uploadFiles(@RequestParam("file") MultipartFile uploadFile) {
        return uploadService.saveUploadedFiles(uploadFile);
    }

}
