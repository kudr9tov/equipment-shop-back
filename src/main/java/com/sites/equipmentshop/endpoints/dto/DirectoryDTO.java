package com.sites.equipmentshop.endpoints.dto;

import com.sites.equipmentshop.service.enums.Directory;

public class DirectoryDTO {
    private Integer id;
    private Integer parentId;
    private Integer level;
    private String value;
    private String image;
    private Directory type;

    public Integer getId() {
        return id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Directory getType() {
        return type;
    }

    public void setType(Directory type) {
        this.type = type;
    }
}
