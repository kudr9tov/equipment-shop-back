package com.sites.equipmentshop.endpoints.dto;

import java.util.Set;

public class MoveCategoriesDTO {
    private Integer parentId;
    private Set<Integer> categoryIds;

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Set<Integer> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Set<Integer> categoryIds) {
        this.categoryIds = categoryIds;
    }
}
