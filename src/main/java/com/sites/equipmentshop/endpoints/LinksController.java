package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.LinkDTO;
import com.sites.equipmentshop.endpoints.dto.MultiCatalogDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.persistence.repositories.LinksDAO;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/links", consumes = {"*/*", "*", "application/json"}, produces = {"*/*", "application/json"})
public class LinksController {

    private final LinksDAO linksDAO;

    @Autowired
    public LinksController(LinksDAO linksDAO) {
        this.linksDAO = linksDAO;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getLinks() {
        return ResponseEntityUtils.responseEntity(linksDAO.getLinks());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getLink(@PathVariable Integer id) {
        return ResponseEntityUtils.responseEntity(linksDAO.getLink(id));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteLink(@PathVariable Integer id) {
        linksDAO.deleteLink(id);
    }

    @PutMapping(value = "")
    public void updateCatalog(@RequestBody List<LinkDTO> linkDTO) {
        linksDAO.updateLink(linkDTO);
    }

    @PostMapping(value = "")
    public void saveCatalog(@RequestBody List<LinkDTO> linkDTO) {
        linksDAO.createLink(linkDTO);
    }

}
