package com.sites.equipmentshop.service.price.brand.dao;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UpdatePriceDAO {
    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public UpdatePriceDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public Integer getProductByName(String name) {
        return 0;
    }

    public Integer getProductByArticle(String article) {
        return nameJdbcTemplate.queryForObject("SELECT id " +
                "FROM product " +
                " INNER JOIN product_properties pp on product.id = pp.product_id AND pp.attributes_id = 2 AND pp.value = '247' " +
                " INNER JOIN product_properties pp1 on product.id = pp1.product_id AND pp1.attributes_id = 3 " +
                "WHERE pp1.value = :article", ImmutableMap.of("article", article), Integer.class);
    }

    public void updateProductArticle(int productId, String article) {

    }

    public void updatePrice(int productId, long price) {
        nameJdbcTemplate.update("UPDATE product SET price = :price WHERE id = :id",
                ImmutableMap.of(
                        "id", productId,
                        "price", price
                ));
    }
}
