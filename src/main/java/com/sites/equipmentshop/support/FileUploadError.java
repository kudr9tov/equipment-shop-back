package com.sites.equipmentshop.support;

public class FileUploadError extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FileUploadError(Exception e) {
		super(e);
	}
}
