package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.NumberUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class HaierproffAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(HaierproffAbstractScrapperImpl.class);

    public HaierproffAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                          FTPUtils ftpUtils,
                                          ScrapperDAO scrapperDAO,
                                          ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        LOGGER.info("Search categories");

        final Document document = ParserUtils.loadDocument(getSite().getDomain() + StringUtils.defaultString(url));
        final Map<String, String> categories = Maps.newHashMap();
        if (Objects.nonNull(document)) {

            try {
                final Elements elementsByClass = document.getElementsByClass("product-category");
                if (CollectionUtils.isEmpty(elementsByClass)) {
                    return ImmutableMap.of(url, category);
                }
                final Map<String, String> tempCategories = elementsByClass
                        .stream()
                        .map(f -> f.getElementsByTag("a").first())
                        .filter(Objects::nonNull)
                        .collect(Collectors.toMap(f -> f.text().replaceAll("\\([0-9]*\\)", StringUtils.EMPTY).trim(), f -> f.attr("href").replace(getSite().getDomain(), StringUtils.EMPTY)));
                tempCategories.forEach((categoryName, categoryUrl) -> categories.putAll(searchCategories(categoryUrl, categoryName)));


            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        return categories;
    }

    @Override
    public Set<String> searchProductPages(String url) {
        final Set<String> pages = Sets.newHashSet();
        final Document document = ParserUtils.loadDocument(url);
        if (Objects.nonNull(document)) {
            final Element pagination = document.getElementsByClass("page-numbers").first();
            if (Objects.nonNull(pagination)) {
                final List<Integer> collect = pagination.children()
                        .stream()
                        .map(f -> {
                            try {
                                return NumberUtils.parseNumber(f.text(), Integer.class);
                            } catch (Exception e) {
                            }
                            return null;
                        })
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                final int lastPage = collect.get(collect.size() - 1);
                for (int page = 1; page <= lastPage; page++) {
                    pages.add(String.format("%s/page/%d", url, page));
                }
                return pages;
            }
        }
        pages.add(url);
        return pages;
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        LOGGER.info("Search products");
        try {
            final Document document = ParserUtils.loadDocument(url);
            if (Objects.nonNull(document)) {
                final Elements subCategoriesClass = document.getElementsByClass("product");
                final Map<String, String> categories = Maps.newHashMap();
                for (Element productElement : subCategoriesClass) {
                    final Element link = productElement.getElementsByTag("a").first();
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    categories.put(UUID.randomUUID().toString(), href);
                }
                return categories;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(getSite().getDomain() + url);
        try {
            if (Objects.nonNull(document)) {
                final Product product = new Product();

                String image = Optional.ofNullable(document.getElementsByClass("woocommerce-product-gallery__image").first())
                        .flatMap(t -> Optional.ofNullable(t.getElementsByTag("img").first()).map(f -> f.attr("src")))
                        .orElse(null);

                try {
                    Map<String, String> techMap = document.getElementsByClass("attribute_name_values")
                            .stream()
                            .flatMap(f -> f.getElementsByTag("tr").stream())
                            .collect(Collectors.toMap(f -> f.child(0).text(), f -> f.child(1).text()));

                    product.setTechProperties(techMap);
                } catch (Exception ignored) {
                    product.setTechProperties(Maps.newHashMap());
                }

                final Map<String, Object> properties = Maps.newHashMap();
                properties.put("country", 240);
                properties.put("brand", 2431);

                product.setProperties(properties);
                try {
                    final String replace = Optional.ofNullable(document.getElementsByAttributeValue("aria-labelledby", "tab-title-functions").first())
                            .orElse(document.getElementsByAttributeValue("aria-labelledby", "tab-title-description").first())
                            .children()
                            .stream()
                            .flatMap(f -> f.children().stream())
                            .map(f -> f.outerHtml().replaceAll("[A-z-]*=\"[A-z\\/0-9.\\-_]*\"", StringUtils.EMPTY)
                                    .replaceAll("<img\\s*>", StringUtils.EMPTY)
                                    .replaceAll("[\n]*[\\s]{2,}", StringUtils.EMPTY)
                                    .replace(" >", ">")
                                    .replace("<div></div>", StringUtils.EMPTY))
                            .collect(Collectors.joining());
                    product.setDescription(replace);
                } catch (Exception ignored) {
                }


                product.setImage(image);
                product.setName(document.getElementsByClass("product_title entry-title").first().text());
                try {
                    product.setPrice(NumberUtils.parseNumber(document.getElementsByClass("woocommerce-Price-amount amount").text().replaceAll("[А-я₽\\s]*", StringUtils.EMPTY).replace(String.valueOf((char) 160), StringUtils.EMPTY).trim(), Long.class));
                } catch (Exception ignored) {

                }
                product.setSiteUrl(getSite(), url);
                consumer.accept(product);
            }
        } catch (Exception e) {
            LOGGER.error("Can't parse product", e);
        }
    }

    @Override
    protected Site getSite() {
        return Site.HAIER;
    }

}
