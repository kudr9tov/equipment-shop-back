package com.sites.equipmentshop.service.enums;

public enum Directory {
    CATEGORY,
    BRAND,
    COUNTRY,
    CERT
}
