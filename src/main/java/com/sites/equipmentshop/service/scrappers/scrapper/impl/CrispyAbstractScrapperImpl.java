package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CrispyAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(CrispyAbstractScrapperImpl.class);
    private static Map<String, Map<String, Integer>> assosiation = Maps.newHashMap();

    static {
        assosiation.put("Стеллажи", ImmutableMap.of("category", 1241, "subCategory", 1654));
        assosiation.put("Витрины", ImmutableMap.of("category", 1240, "subCategory", 1587));

        assosiation.put("Охлаждаемые стеллажи", ImmutableMap.of("category", 1241, "subCategory", 1654));
        assosiation.put("Гастрономические прилавки", ImmutableMap.of("category", 1241, "subCategory", 1588));
        assosiation.put("Морозильные шкафы", ImmutableMap.of("category", 1238, "subCategory", 1620));

        assosiation.put("Лари-бонеты ItalFrost", ImmutableMap.of("category", 1238, "subCategory", 1596));
        assosiation.put("Надстройка для ларя-бонеты Corsa", ImmutableMap.of("category", 1238, "subCategory", 1596));
        assosiation.put("Лари Italfrost", ImmutableMap.of("category", 1238, "subCategory", 1627));

        assosiation.put("Охлаждаемые шкафы", ImmutableMap.of("category", 1238, "subCategory", 1621));

        assosiation.put("Охлаждаемые столы", ImmutableMap.of("category", 1238, "subCategory", 1634));
        assosiation.put("Морозильные столы", ImmutableMap.of("category", 1238, "subCategory", 1636));
        assosiation.put("Саладетты", ImmutableMap.of("category", 1238, "subCategory", 1630));
        assosiation.put("Столы для пиццы", ImmutableMap.of("category", 1238, "subCategory", 1633));
        assosiation.put("Столы с полипропиленовой столешницей", ImmutableMap.of("category", 1238, "subCategory", 1634));
        assosiation.put("Столы с гранитной столешницей", ImmutableMap.of("category", 1238, "subCategory", 1634));

        assosiation.put("Ванны моечные", ImmutableMap.of("category", 1241, "subCategory", 1656));
        assosiation.put("Стеллажи кухонные", ImmutableMap.of("category", 1241, "subCategory", 1654));
        assosiation.put("Столы производственные", ImmutableMap.of("category", 1241, "subCategory", 1657));
        assosiation.put("Полки", ImmutableMap.of("category", 1241, "subCategory", 1651));
        assosiation.put("Подставки и подтоварники", ImmutableMap.of("category", 1241, "subCategory", 1650));
        assosiation.put("Зонты вытяжные", ImmutableMap.of("category", 1241, "subCategory", 1652));
        assosiation.put("Шкафы и тележки", ImmutableMap.of("category", 1241, "subCategory", 1655));

    }

    public CrispyAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                      FTPUtils ftpUtils,
                                      ScrapperDAO scrapperDAO,
                                      ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + StringUtils.defaultString(url));
        final Map<String, String> categories = Maps.newHashMap();
        if (Objects.nonNull(document)) {

            try {
                final Map<String, String> tempCategories = document.getElementsByClass("row banner-menu").first()
                        .children()
                        .stream()
                        .map(f -> f.getElementsByTag("a").first())
                        .collect(Collectors.toMap(Element::text, f -> f.attr("href")));

                tempCategories.forEach((categoryName, categoryUrl) -> categories.putAll(searchCategories(categoryUrl, categoryName)));
                if (MapUtils.isEmpty(tempCategories)) {
                    return ImmutableMap.of(url, category);
                }
            } catch (Exception e) {
                try {
                    final Map<String, String> tempCategories = document.getElementsByClass("banner-menu-wrapper").first()
                            .children()
                            .stream()
                            .collect(Collectors.toMap(f -> f.getElementsByClass("catalog3-banner-menu-title").first().text(), f -> f.attr("href")));

                    tempCategories.forEach((categoryName, categoryUrl) -> categories.putAll(searchCategories(categoryUrl, categoryName)));
                    if (MapUtils.isEmpty(tempCategories)) {
                        return ImmutableMap.of(category, url);
                    }
                } catch (Exception ex) {
                    return ImmutableMap.of(url, category);
                }
            }
        }
        categories.remove("Стеллажные системы");
        return categories;
    }

    @Override
    public Set<String> searchProductPages(String url) {
        return Sets.newHashSet(url);
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        LOGGER.info("Search products");
        try {
            final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
            if (Objects.nonNull(document)) {
                final Elements subCategoriesClass = document.getElementsByClass("col-md-3 col-xs-6 banner-menu-item-wrapper");
                final Map<String, String> categories = Maps.newHashMap();

                for (Element category : subCategoriesClass) {
                    final Element link = category.getElementsByTag("a").first();
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    categories.put(UUID.randomUUID().toString(), href);
                }
                return categories;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {
                final String imageClass = document.getElementsByClass("flex-active-slide").first().attr("style");
                final Pattern compile = Pattern.compile("\\/upload[\\/A-z0-9-]*.jpg");
                Matcher matcher = compile.matcher(imageClass);
                matcher.find();
                final String image = matcher.group(0);

                final Element specifications = document.getElementById("specifications");
                final Element table = specifications.getElementsByTag("table").first().child(0);

                final Map<String, Object> properties = Maps.newHashMap();
                properties.put("country", 240);
                properties.put("brand", 339);
                final Product product = new Product();
                product.setProperties(properties);
                product.setSiteUrl(getSite(), url);

                final int productsSize = table.child(0).children().size() - 1;
                if (productsSize <= 0) {
                    try {
                        final Element description = document.getElementById("description");
                        final String replace = description.outerHtml()
                                .replace("Сайт: \n" +
                                        " <a href=\"http://unit.cryspi.ru\">unit.cryspi.ru</a>", StringUtils.EMPTY)
                                .replaceAll("style=\\\"[%: #A-z0-9;]*\\\"", StringUtils.EMPTY);
                        product.setDescription(replace);
                    } catch (Exception ignored) {
                    }
                    product.setImage(image);
                    product.setName(document.getElementsByTag("h1").first().text());
                    consumer.accept(product);
                }
                for (int j = 1; j <= productsSize; j++) {
                    try {
                        final Element description = document.getElementById("description");
                        final String replace = description.outerHtml()
                                .replace("Сайт: \n" +
                                        " <a href=\"http://unit.cryspi.ru\">unit.cryspi.ru</a>", StringUtils.EMPTY)
                                .replaceAll("style=\\\"[%: #A-z0-9;]*\\\"", StringUtils.EMPTY);
                        product.setDescription(replace);
                    } catch (Exception ignored) {
                    }
                    product.setImage(image);
                    product.setName(table.child(0).child(j).text());
                    final Map<String, String> techMap = Maps.newHashMap();
                    for (int i = 1; i < table.children().size(); i++) {
                        final Element element = table.child(i);
                        final String key = element.child(0).text();
                        String value;
                        try {
                            value = element.child(j).text();
                        } catch (Exception e) {
                            value = element.child(1).text();
                        }
                        techMap.put(key, value);
                    }
                    product.setTechProperties(techMap);
                    consumer.accept(product);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Can't parse product", e);
        }
    }

    @Override
    protected Site getSite() {
        return Site.CRISPY;
    }

}
