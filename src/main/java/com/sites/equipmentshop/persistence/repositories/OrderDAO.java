package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.sites.equipmentshop.persistence.entities.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;

@Repository
public class OrderDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public OrderDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }


    public Object save(String productJson) {
       return nameJdbcTemplate.queryForObject("SELECT * FROM processing_order(:params)",
                ImmutableMap.of("params", productJson),
                Object.class);
    }

    public String getOrders(Integer limit, Integer offset, String filter) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("limit", limit);
        params.put("offset", offset);
        params.put("filter", filter);
        return nameJdbcTemplate.queryForObject("SELECT coalesce(jsonb_agg(get_order(id)), '[]') FROM orders WHERE status != 'DELETED' OFFSET :offset LIMIT :limit",
                params, String.class);
    }
    public void changeStatus(Integer id, OrderStatus status) {
        nameJdbcTemplate.update("UPDATE orders SET status = :status WHERE id = :id",
                ImmutableMap.of("id", id, "status", status.name()));
    }

    public Optional<String> findById(Integer id) {
        String product = nameJdbcTemplate.queryForObject("SELECT * FROM get_order(:id)",
                ImmutableMap.of("id", id), String.class);
        return Optional.ofNullable(product);
    }
}
