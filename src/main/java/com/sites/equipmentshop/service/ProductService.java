package com.sites.equipmentshop.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sites.equipmentshop.exception.ProductAlreadyExistException;
import com.sites.equipmentshop.exception.ProductNotFoundException;
import com.sites.equipmentshop.persistence.entities.Product;
import com.sites.equipmentshop.persistence.repositories.ProductDAO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ProductService {
    private final ProductDAO productRepository;

    @Autowired
    public ProductService(final NamedParameterJdbcTemplate jdbcTemplate,
                          final ProductDAO productRepository) {
        this.productRepository = productRepository;
    }

    public Boolean findProductByName(String name) {
        return productRepository.findProductByName(name);
    }

    public void deleteProductById(Integer id) {
        productRepository.deleteById(id);
    }

    public String save(String productJson) {
        return productRepository.save(productJson);
    }

    public String getAllProducts(Integer limit,
                                 Integer offset,
                                 String filter) {
        try {
            return productRepository.getProducts(limit >= 0 ? limit : 0, offset, filter);
        } catch (Exception e) {
            throw new IllegalStateException("Can't get products", e);
        }
    }

    public String getProduct(Integer id) {
        return productRepository.findById(id).orElseThrow(ProductNotFoundException::new);
    }

    public String createProduct(Product productDTO) {
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            return productRepository.save(objectMapper.writeValueAsString(productDTO));
        } catch (JsonProcessingException e) {

        }
        return StringUtils.EMPTY;
    }

    public String getMaxMinPrice() {
        return productRepository.getMaxMinPrice();
    }

    public String getErrorProducts(Integer limit, Integer offset) {
        return productRepository.getErrorProducts(limit, offset);
    }
}
