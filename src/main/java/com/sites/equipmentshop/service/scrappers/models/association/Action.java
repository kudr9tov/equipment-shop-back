package com.sites.equipmentshop.service.scrappers.models.association;

public enum Action {
    REMOVE, INSERT, UPDATE
}
