package com.sites.equipmentshop.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sites.equipmentshop.endpoints.dto.PriceRequestDTO;
import com.sites.equipmentshop.persistence.entities.Client;
import com.sites.equipmentshop.persistence.entities.Product;
import com.sites.equipmentshop.persistence.repositories.PriceDAO;
import com.sites.equipmentshop.persistence.repositories.ProductDAO;
import com.sites.equipmentshop.security.services.MailTemplateFactory;
import com.sites.equipmentshop.utils.HTMLUtils;
import com.sites.equipmentshop.utils.shared.SendMailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public class PriceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PriceService.class);

    private final PriceDAO priceDAO;
    private final ProductDAO productDAO;
    private final MailTemplateFactory templateFactory;
    private final SendMailService sendMailService;

    @Autowired
    public PriceService(final JdbcTemplate jdbcTemplate,
                        final PriceDAO priceDAO,
                        final ProductDAO productDAO,
                        final MailTemplateFactory templateFactory,
                        final SendMailService sendMailService) {
        this.priceDAO = priceDAO;
        this.productDAO = productDAO;
        this.templateFactory = templateFactory;
        this.sendMailService = sendMailService;
    }


    public void getProductPrice(PriceRequestDTO requestDTO) {
        priceDAO.saveProductPrice(requestDTO);
        try {
            final Product product = new ObjectMapper().readValue(productDAO.findById(requestDTO.getProductId()).orElse("{}"), Product.class);
            sendMailService.sendMessageMoreThanOne("Запрос на цену",
                    templateFactory.prepareNewNotificationTemplate("Запрос на цену",
                            HTMLUtils.convertProductToHtml(product, null),
                            HTMLUtils.convertClientToHtml(new Client(requestDTO), requestDTO.getComment()),
                            HTMLUtils.getProductsHeader(false),
                            Boolean.TRUE),
                    "kudr9tov@gmail.com", "pashagozasinett@gmail.com", "info@gktorg.ru");
            sendMailService.sendMessageMoreThanOne("Вы оставили запрос на цену товара",
                    templateFactory.prepareNewNotificationTemplate("Запрос на цену",
                            HTMLUtils.convertProductToHtml(product, null),
                            HTMLUtils.convertClientToHtml(new Client(requestDTO), requestDTO.getComment()),
                            HTMLUtils.getProductsHeader(false),
                            Boolean.FALSE),
                    requestDTO.getEmail());
        } catch (MessagingException | JsonProcessingException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }
    }


    public String getProductPriceRequests() {
        return priceDAO.getProductPriceRequests();
    }

    public void deleteProductPriceRequests(Integer id) {
        priceDAO.deleteProductPriceRequests(id);
    }
}
