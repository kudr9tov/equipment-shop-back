package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.SchedulingEmailDTO;
import com.sites.equipmentshop.endpoints.dto.SchedulingScrapperDTO;
import com.sites.equipmentshop.service.enums.ScheduleJobType;
import com.sites.equipmentshop.service.scrappers.models.ScheduleJobModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class SchedulingDAO {
    private final static Logger LOGGER = LoggerFactory.getLogger(SchedulingDAO.class);

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public SchedulingDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public void createScrapperJob(SchedulingScrapperDTO dto, String schedulingId) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("job_type", ScheduleJobType.SCRAPPER.name());
        params.put("scrapper_name", dto.getDto().getSite().name());
        params.put("cron", dto.getCron());
        params.put("scheduled_id", schedulingId);
        nameJdbcTemplate.update("INSERT INTO scheduling_jobs(job_type, scrapper_name, cron, scheduled_id) " +
                "VALUES (:job_type, :scrapper_name, :cron, :scheduled_id);", params);
    }

    public void createMailJob(SchedulingEmailDTO dto, String schedulingId) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("job_type", ScheduleJobType.MAIL.name());
        params.put("cron", dto.getCron());
        params.put("scheduled_id", schedulingId);
        nameJdbcTemplate.update("INSERT INTO scheduling_jobs(job_type, cron, scheduled_id) " +
                "VALUES (:job_type, :cron, :scheduled_id);", params);
    }

    public void deleteJob(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        nameJdbcTemplate.update("DELETE FROM scheduling_jobs " +
                "WHERE id = :id", params);
    }

    public ScheduleJobModel getScheduleJobId(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.queryForObject("SELECT job_type, scrapper_name, scheduled_id  FROM scheduling_jobs " +
                "WHERE id = :id", params, (resultSet, i) -> {
            final ScheduleJobModel scheduleJobModel = new ScheduleJobModel();
            scheduleJobModel.setJobType(resultSet.getString("job_type"));
            scheduleJobModel.setScrapperName(resultSet.getString("scrapper_name"));
            scheduleJobModel.setScheduleId(resultSet.getString("scheduled_id"));
            return scheduleJobModel;
        });
    }

    public String getScrapperJob(Integer siteId) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("siteId", siteId);
        return nameJdbcTemplate.queryForObject("SELECT jsonb_build_object( " +
                "                'id', sj.id, " +
                "                'type', sj.job_type, " +
                "                'scrapper', sj.scrapper_name, " +
                "                'cron', sj.cron, " +
                "                'nextFireTime', next_fire_time " +
                "           ) " +
                "FROM sites s " +
                "  INNER JOIN scheduling_jobs sj ON s.name = sj.scrapper_name " +
                "  INNER JOIN qrtz_job_details d ON sj.scheduled_id = d.job_name " +
                "  INNER JOIN qrtz_triggers qt on d.sched_name = qt.sched_name and d.job_name = qt.job_name and d.job_group = qt.job_group " +
                "WHERE s.id = :siteId " +
                "LIMIT 1;", params, String.class);
    }

    public String getEmailJob() {
        return nameJdbcTemplate.queryForObject("SELECT jsonb_build_object( " +
                "                'id', sj.id, " +
                "                'type', sj.job_type, " +
                "                'cron', sj.cron, " +
                "                'nextFireTime', next_fire_time " +
                "           ) " +
                "FROM scheduling_jobs sj " +
                "  INNER JOIN qrtz_job_details d ON sj.scheduled_id = d.job_name " +
                "  INNER JOIN qrtz_triggers qt on d.sched_name = qt.sched_name and d.job_name = qt.job_name and d.job_group = qt.job_group " +
                "WHERE sj.job_type = 'MAIL' " +
                "LIMIT 1; ", Maps.newHashMap(), String.class);
    }
}
