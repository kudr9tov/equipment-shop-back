package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.CommentDTO;
import com.sites.equipmentshop.endpoints.dto.CommentType;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.persistence.repositories.CommentsDAO;
import com.sites.equipmentshop.utils.APIConstants;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/comments", consumes = {"*/*", "application/json"}, produces = {"*/*", "application/json"})
public class CommentController {

    private final CommentsDAO commentsDAO;

    @Autowired
    public CommentController(CommentsDAO linksDAO) {
        this.commentsDAO = linksDAO;
    }

    @GetMapping(value = "/{type}/{id}")
    public ResponseEntity<Object> getCommentsEntity(@PathVariable String type, @PathVariable Integer id) {
        CommentType commentType = EnumUtils.getEnumIgnoreCase(CommentType.class, type);
        if (Objects.isNull(commentType)) {
            throw new IllegalArgumentException(String.format("Comments module not supported for %s block", type));
        }
        return ResponseEntityUtils.responseEntity(commentsDAO.getEntityComments(commentType, id));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getComment(@PathVariable Integer id) {
        return ResponseEntityUtils.responseEntity(commentsDAO.getComment(id));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteComment(@PathVariable Integer id) {
        commentsDAO.deleteComment(id);
    }

    @PutMapping(value = "")
    public ResponseEntity<Object> updateComment(@RequestBody CommentDTO commentDTO) {
        return ResponseEntityUtils.responseEntity(commentsDAO.updateComment(commentDTO));
    }

    @PostMapping(value = "")
    public ResponseEntity<Object> saveComment(@RequestBody CommentDTO commentDTO) {
        return ResponseEntityUtils.responseEntity(commentsDAO.createComment(commentDTO));
    }

}
