package com.sites.equipmentshop.persistence.repositories;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.ScrapperResult;
import com.sites.equipmentshop.service.scrappers.models.ScrapperStatus;
import com.sites.equipmentshop.service.scrappers.models.association.SiteScrappingAssociation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Repository
public class ScrapperDAO {
    private final static Logger LOGGER = LoggerFactory.getLogger(ScrapperDAO.class);

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public ScrapperDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public Integer createJob(Integer siteId) {
        return nameJdbcTemplate.queryForObject("INSERT INTO scrapper_executions (site_id, status) " +
                        "VALUES (:siteId, :status) " +
                        "RETURNING id;",
                ImmutableMap.of(
                        "siteId", siteId,
                        "status", ScrapperStatus.IN_QUEUE.name()
                ),
                Integer.class);
    }


    public void updateJob(Integer jobId, ScrapperStatus status, ScrapperResult result) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final Map<String, Object> params = Maps.newHashMap();
            params.put("jobId", jobId);
            params.put("status", status.name());
            final String resultJson = Optional.ofNullable(result)
                    .map(f -> {
                        try {
                            return mapper.writeValueAsString(result);
                        } catch (JsonProcessingException e) {
                            return null;
                        }
                    })
                    .orElse(null);
            params.put("result", resultJson);
            nameJdbcTemplate.update("UPDATE scrapper_executions " +
                    " SET " +
                    "   status = :status, " +
                    "   result = cast(:result as jsonb), " +
                    "   updated = current_timestamp " +
                    "WHERE id = :jobId;", params);
        } catch (Exception e) {
            LOGGER.error("Can't update job", e);
        }
    }

    public String getAssociations(Integer siteId) {
        return nameJdbcTemplate.queryForObject("SELECT coalesce((SELECT jsonb_build_object('siteId', id, 'siteBrand', name, 'siteUrl', url, 'associations', " +
                "                                           jsonb_agg(association)) as associations " +
                "                 FROM ( " +
                "                          SELECT s.id, " +
                "                                 s.name, " +
                "                                 s.url, " +
                "                                 jsonb_build_object( " +
                "                                         'siteCategoryId', sc.id, " +
                "                                         'siteCategoryName', sc.category_name, " +
                "                                         'siteCategoryUrl', sc.category_url, " +
                "                                         'isSiteCategoryExcluded', sc.excluded, " +
                "                                         'currentCategoryId', d.id, " +
                "                                         'currentCategoryName', d.name " +
                "                                     ) association " +
                "                          FROM sites s " +
                "                                   INNER JOIN site_category sc on sc.site_id = s.id " +
                "                                   LEFT JOIN site_category_association sca on sca.site_category_id = sc.id " +
                "                                   LEFT JOIN directory d on d.id = sca.current_category_id " +
                "                          WHERE s.id = :siteId " +
                "                          ORDER BY sc.excluded, sca.site_category_id " +
                "                      ) as data " +
                "                 GROUP BY id, name, url), '{}')", ImmutableMap.of("siteId", siteId), String.class);
    }

    public String getExecutionTasks(Integer siteId) {
        return nameJdbcTemplate.queryForObject("SELECT coalesce(jsonb_agg(task), '[]') " +
                "FROM ( " +
                "         SELECT jsonb_build_object( " +
                "                        'id', se.id, " +
                "                        'status', se.status, " +
                "                        'result', se.result, " +
                "                        'created', CAST(EXTRACT(EPOCH FROM se.created) * 1000 AS BIGINT), " +
                "                        'updated', CAST(EXTRACT(EPOCH FROM se.updated) * 1000 AS BIGINT), " +
                "                        'duration', to_char(updated - created, 'HH24:MI:SS') " +
                "                    ) task " +
                "         FROM sites s " +
                "                  INNER JOIN scrapper_executions se on s.id = se.site_id " +
                "         WHERE s.id = :siteId " +
                "         ORDER BY se.id DESC" +
                "         LIMIT 1 " +
                "     ) as data", ImmutableMap.of("siteId", siteId), String.class);
    }

    public String getAllAssociations() {
        return nameJdbcTemplate.queryForObject("SELECT coalesce(jsonb_agg(jsonb_build_object('siteId', id, 'siteBrand', name, 'siteUrl', url, 'associations', associations)), '[]') " +
                "FROM ( " +
                "         SELECT s.*, " +
                "                jsonb_agg(jsonb_build_object( " +
                "                        'siteCategoryId', sc.id, " +
                "                        'siteCategoryName', sc.category_name, " +
                "                        'siteCategoryUrl', sc.category_url, " +
                "                        'isSiteCategoryExcluded', sc.excluded, " +
                "                        'currentCategoryId', d.id, " +
                "                        'currentCategoryName', d.name " +
                "                    )) associations " +
                "         FROM sites s " +
                "                  INNER JOIN site_category sc on sc.site_id = s.id " +
                "                  LEFT JOIN site_category_association sca on sca.site_category_id = sc.id " +
                "                  LEFT JOIN directory d on d.id = sca.current_category_id " +
                "         GROUP BY s.id " +
                "     ) as data;", Collections.emptyMap(), String.class);
    }

    public Integer saveSite(Site site) {
        return nameJdbcTemplate.queryForObject("INSERT INTO sites (name, url) " +
                "VALUES (:name, :url) " +
                "ON CONFLICT (url) DO UPDATE " +
                "    SET name = EXCLUDED.name " +
                "RETURNING id;", ImmutableMap.of("name", site.name(), "url", site.getDomain()), Integer.class);
    }

    public void saveSiteCategories(Site site, Integer siteId, Map<String, String> categories) {
        final SqlParameterSource[] sqlParameterSources = categories.entrySet()
                .stream()
                .map(entry -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("siteId", siteId);
                    parameters.addValue("categoryUrl", site.getDomain() + entry.getKey());
                    parameters.addValue("categoryName", entry.getValue());
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        nameJdbcTemplate.batchUpdate(
                "INSERT INTO site_category (site_id, category_name, category_url) " +
                        "VALUES (:siteId, :categoryName, :categoryUrl) " +
                        "ON CONFLICT (site_id, category_url) DO UPDATE SET category_name = EXCLUDED.category_name, category_url = EXCLUDED.category_url",
                sqlParameterSources
        );
    }

    public void excludeCategory(Integer siteId, List<SiteScrappingAssociation> associations) {
        final SqlParameterSource[] sqlParameterSources = associations
                .stream()
                .map(entry -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("siteCategoryId", entry.getSiteCategoryId());
                    parameters.addValue("excluded", entry.isExclude());
                    parameters.addValue("siteId", siteId);
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        nameJdbcTemplate.batchUpdate(
                "UPDATE site_category " +
                        " SET excluded = :excluded " +
                        "WHERE id = :siteCategoryId AND site_id = :siteId;",
                sqlParameterSources
        );
    }


    public Map<String, String> getSiteCategories(Integer siteId) {
        Map<String, String> categories = Maps.newHashMap();
        nameJdbcTemplate.query(
                "SELECT category_url, category_name " +
                        "FROM site_category " +
                        "WHERE site_id = :siteId " +
                        " AND not excluded",
                ImmutableMap.of("siteId", siteId), (resultSet) -> {
                    categories.put(resultSet.getString("category_url"), resultSet.getString("category_name"));
                }
        );
        return categories;
    }

    public Integer getAssociationBySiteIdAndUrl(Integer siteId, String categoryUrl) {
        try {
            return nameJdbcTemplate.queryForObject(
                    "SELECT sca.current_category_id " +
                            "FROM sites s " +
                            " INNER JOIN site_category sc on s.id = sc.site_id " +
                            " INNER JOIN site_category_association sca ON sc.id = sca.site_category_id " +
                            "WHERE s.id = :siteId " +
                            "  AND sc.category_url = :categoryUrl;",
                    ImmutableMap.of("siteId", siteId, "categoryUrl", categoryUrl),
                    Integer.class
            );
        } catch (Exception e) {
            return null;
        }
    }

    public Set<String> getAssociationBySiteIdAndUrl(Site site) {
        try {
            return Sets.newHashSet(nameJdbcTemplate.queryForList(
                    "SELECT DISTINCT url  " +
                            "FROM product p  " +
                            "   INNER JOIN remote_product rp on p.id = rp.product_id  " +
                            "WHERE rp.url ~ :siteUrl;",
                    ImmutableMap.of("siteUrl", site.getDomain()),
                    String.class
            ));
        } catch (Exception e) {
            return null;
        }
    }

    public String getSites() {
        return nameJdbcTemplate.queryForObject("SELECT coalesce(jsonb_agg(jsonb_build_object(  " +
                "                                  'id', s.id,  " +
                "                                  'siteName', s.name,  " +
                "                                  'siteUrl', s.url,  " +
                "                                  'isAvailableUpdateCategory', s.is_available_update_category,  " +
                "                                  'isAvailableUpdateProductPrice', s.is_available_update_product_price,  " +
                "                                  'isAvailableUpdateProduct', s.is_available_update_product,  " +
                "                                  'status', (SELECT status  " +
                "                                             FROM scrapper_executions  " +
                "                                             WHERE site_id = s.id  " +
                "                                             ORDER BY updated  " +
                "                                             LIMIT 1))  " +
                "                          ORDER BY s.name), '[]')  " +
                "FROM sites s;", Collections.emptyMap(), String.class);
    }

    public void saveAssociations(List<SiteScrappingAssociation> associations) {
        final SqlParameterSource[] sqlParameterSources = associations
                .stream()
                .map(entry -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("siteCategoryId", entry.getSiteCategoryId());
                    parameters.addValue("currentCategoryId", entry.getCurrentCategoryId());
                    parameters.addValue("exclude", entry.isExclude());
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        nameJdbcTemplate.batchUpdate(
                "INSERT INTO site_category_association (site_category_id, current_category_id, exclude) " +
                        "VALUES (:siteCategoryId, :currentCategoryId, :exclude) ON CONFLICT (site_category_id) DO UPDATE SET exclude = excluded.exclude, current_category_id = excluded.current_category_id;",
                sqlParameterSources
        );
    }

    public void updateAssociations(List<SiteScrappingAssociation> associations) {
        final SqlParameterSource[] sqlParameterSources = associations
                .stream()
                .map(entry -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("siteCategoryId", entry.getSiteCategoryId());
                    parameters.addValue("currentCategoryId", entry.getCurrentCategoryId());
                    parameters.addValue("exclude", entry.isExclude());
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        nameJdbcTemplate.batchUpdate(
                "UPDATE site_category_association " +
                        " SET current_category_id = :currentCategoryId," +
                        "     exclude = :exclude " +
                        "WHERE site_category_id = :siteCategoryId;",
                sqlParameterSources
        );
    }

    public void deleteAssociations(List<SiteScrappingAssociation> associations) {
        final SqlParameterSource[] sqlParameterSources = associations
                .stream()
                .map(entry -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("siteCategoryId", entry.getSiteCategoryId());
                    parameters.addValue("currentCategoryId", entry.getCurrentCategoryId());
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        nameJdbcTemplate.batchUpdate(
                "DELETE FROM site_category_association " +
                        "WHERE coalesce(current_category_id, 1) = coalesce(:currentCategoryId, 1) " +
                        "  AND site_category_id = :siteCategoryId;",
                sqlParameterSources
        );
    }
}
