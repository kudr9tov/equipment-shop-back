package com.sites.equipmentshop.service;

import com.sites.equipmentshop.endpoints.dto.CarouselDTO;
import com.sites.equipmentshop.endpoints.dto.CatalogDTO;
import com.sites.equipmentshop.persistence.repositories.CarouselDAO;
import com.sites.equipmentshop.persistence.repositories.CatalogDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarouselService {
    private final CarouselDAO carouselDAO;

    @Autowired
    public CarouselService(final JdbcTemplate jdbcTemplate,
                           final CarouselDAO carouselDAO) {
        this.carouselDAO = carouselDAO;
    }


    public String getSlideTree() {
        return carouselDAO.getSlideTree();
    }

    public String getSlide(Integer id) {
        return carouselDAO.getSlide(id);
    }

    public void deleteSlide(Integer id) {
        carouselDAO.deleteSlide(id);
    }

    public String updateSlide(List<CarouselDTO> catalogDTO) {
        return carouselDAO.updateSlide(catalogDTO);
    }

    public String saveSlide(List<CarouselDTO> catalogDTO) {
        return carouselDAO.saveSlide(catalogDTO);
    }

    public String getSlideByStep(Integer step) {
        return carouselDAO.getSlideByStep(step);
    }
}
