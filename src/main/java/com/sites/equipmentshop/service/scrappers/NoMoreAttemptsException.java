package com.sites.equipmentshop.service.scrappers;

public class NoMoreAttemptsException extends RuntimeException {
    public NoMoreAttemptsException() {
    }

    public NoMoreAttemptsException(String message) {
        super(message);
    }

    public NoMoreAttemptsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoMoreAttemptsException(Throwable cause) {
        super(cause);
    }
}
