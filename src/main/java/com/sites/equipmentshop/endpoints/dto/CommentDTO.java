package com.sites.equipmentshop.endpoints.dto;

public class CommentDTO {
    private Integer id;
    private CommentType commentType;
    private Integer entityLinkedId;
    private Integer commentParentId;
    private String author;
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CommentType getCommentType() {
        return commentType;
    }

    public void setCommentType(CommentType commentType) {
        this.commentType = commentType;
    }

    public Integer getEntityLinkedId() {
        return entityLinkedId;
    }

    public void setEntityLinkedId(Integer entityLinkedId) {
        this.entityLinkedId = entityLinkedId;
    }

    public Integer getCommentParentId() {
        return commentParentId;
    }

    public void setCommentParentId(Integer commentParentId) {
        this.commentParentId = commentParentId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
