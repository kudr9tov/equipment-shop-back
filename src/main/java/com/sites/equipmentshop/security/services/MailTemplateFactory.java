package com.sites.equipmentshop.security.services;

import com.sites.equipmentshop.security.endpoints.dto.NewUserDTO;
import com.sites.equipmentshop.security.persistence.entities.UserEntity;
import org.antlr.stringtemplate.StringTemplate;
import org.apache.logging.log4j.util.Strings;
import org.flywaydb.core.internal.util.FileCopyUtils;
import org.flywaydb.core.internal.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@Component
public class MailTemplateFactory {

    private static final String TEMPLATE_FOLDER = "classpath:templates/";

    @Value("${sem.platform.domain.url}")
    private String domain;
    private final String resetTemplate;
    private final String signTemplate;
    private final String newNotification;

    @Autowired
    public MailTemplateFactory(ResourceLoader resourceLoader) throws IOException {
        Resource resetTemlRes = resourceLoader.getResource(TEMPLATE_FOLDER + "reset_message.st");
        this.resetTemplate = FileCopyUtils.copyToString(new InputStreamReader(resetTemlRes.getInputStream(), StandardCharsets.UTF_8));
        Resource newNotificationRes = resourceLoader.getResource(TEMPLATE_FOLDER + "productNotificationTemplate.st");
        this.newNotification = FileCopyUtils.copyToString(new InputStreamReader(newNotificationRes.getInputStream(), StandardCharsets.UTF_8));
        Resource signTemlRes = resourceLoader.getResource(TEMPLATE_FOLDER + "sign_message.st");
        this.signTemplate = FileCopyUtils.copyToString(new InputStreamReader(signTemlRes.getInputStream(), StandardCharsets.UTF_8));
    }

    public String prepareResetPassTemplate(UserEntity entity, String token) {
        StringTemplate template = new StringTemplate(resetTemplate);
        template.setAttribute("user_name", entity.getUserName());
        template.setAttribute("user_email", entity.getEmail());
        template.setAttribute("platformUrl", domain);
        template.setAttribute("token", token);
        return template.toString();
    }

    public String prepareSignTemplate(NewUserDTO userDto, String password) {
        StringTemplate template = new StringTemplate(signTemplate);
        template.setAttribute("user_name", userDto.getUserName());
        template.setAttribute("user_email", userDto.getEmail());
        template.setAttribute("user_password", password);
        template.setAttribute("platformUrl", domain);
        return template.toString();
    }

    public String prepareTemplate(String templateString, String email) {
        final StringTemplate template = new StringTemplate(templateString);
        template.setAttribute("user_email", email);
        return template.toString();
    }

    public String prepareNewNotificationTemplate(String action, String product, String clientInfo, String headers, boolean admin) {
        final StringTemplate template = new StringTemplate(newNotification);
        template.setAttribute("action", action);
        template.setAttribute("products", product);
        template.setAttribute("productsHeader", headers);
        template.setAttribute("clientInfo", clientInfo);
        template.setAttribute("admin", admin
                ? "<div style=\"font-size: 16px;display: inline-block;letter-spacing: 0.2px;color: #6b6262;font-family: 'Alumni Sans', sans-serif;\">Пожалуйста перейдите в <a href=\"https://gktorg.ru/admin/#/requests\">панель администратора</a>.</div>"
                : "<div style=\"font-size: 16px;display: inline-block;letter-spacing: 0.2px;color: #6b6262;font-family: 'Alumni Sans', sans-serif;\">Вы оставили заявку на сайте <a href=\"https://gktorg.ru\">ГлавКом</a>. Наш менеджер свяжется с вами.</div>");
        return template.toString();
    }

}
