package com.sites.equipmentshop.service.price;

import com.sites.equipmentshop.service.price.brand.UpdatePrice;
import com.sites.equipmentshop.service.price.brand.dao.UpdatePriceDAO;
import com.sites.equipmentshop.service.price.brand.impl.AbatUpdatePriceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Optional;

@Service
public class FilePriceService {

    private final UpdatePriceDAO updatePriceDAO;
    private UpdatePrice updatePrice;

    @Autowired
    public FilePriceService(UpdatePriceDAO updatePriceDAO) {
        this.updatePriceDAO = updatePriceDAO;
    }

    public void updateBrandProductPrice(Brand brand, File file) {

        switch (brand) {
            case ABAT:
                updatePrice = new AbatUpdatePriceImpl(updatePriceDAO);
                break;
            default:
                throw new UnsupportedOperationException(String.format("Not found implementation for %s brand", brand.name()));

        }

        Optional.of(updatePrice).ifPresent(service -> service.update(file));
    }
}
