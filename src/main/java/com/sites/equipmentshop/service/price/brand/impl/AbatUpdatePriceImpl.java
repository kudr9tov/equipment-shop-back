package com.sites.equipmentshop.service.price.brand.impl;

import com.sites.equipmentshop.service.price.brand.UpdatePrice;
import com.sites.equipmentshop.service.price.brand.dao.UpdatePriceDAO;
import com.sites.equipmentshop.service.price.brand.model.AbatPriceProduct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class AbatUpdatePriceImpl implements UpdatePrice {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbatUpdatePriceImpl.class);

    private final UpdatePriceDAO updatePriceDAO;

    public AbatUpdatePriceImpl(UpdatePriceDAO updatePriceDAO) {
        this.updatePriceDAO = updatePriceDAO;
    }

    @Override
    public void update(File file) {
        final List<AbatPriceProduct> products = getProducts(file, AbatPriceProduct.class);
        for (AbatPriceProduct item : products) {
            final String article = item.getArticle();
            try {
                final Integer productId = updatePriceDAO.getProductByArticle(article);
                if (Objects.nonNull(productId) && Objects.nonNull(item.getPriceNds())) {
                    updatePriceDAO.updatePrice(productId, item.getPriceNds());
                    productsCount.incrementAndGet();
                }
            } catch (Exception e) {
                LOGGER.error("can't update price", e);
            }
        }
    }
}
