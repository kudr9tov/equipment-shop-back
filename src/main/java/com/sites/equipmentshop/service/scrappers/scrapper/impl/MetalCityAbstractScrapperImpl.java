package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

public class MetalCityAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetalCityAbstractScrapperImpl.class);
    private Set<String> brands = Sets.newHashSet();
    private Set<String> countries = Sets.newHashSet();

    public MetalCityAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                         FTPUtils ftpUtils,
                                         ScrapperDAO scrapperDAO,
                                         ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        if (!url.contains("catalog")) {
            return Collections.emptyMap();
        }
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
        if (Objects.nonNull(document)) {
            Elements categoriesClass = document.getElementsByClass("heading_c uk-margin-small-bottom user-common-link black-link");
            if (categoriesClass.size() == 0) {
                categoriesClass = document.getElementsByClass("heading_c uk-margin-small-bottom user-common-link");
            }
            final Map<String, String> categories = Maps.newHashMap();
            for (Element item : categoriesClass) {
                categories.putAll(searchCategories(item.attr("href"), item.text()));

            }
            if (MapUtils.isEmpty(categories)) {
                categories.put(url, category);
            }
            return categories;
        }
        return Collections.emptyMap();
    }


    @Override
    public Set<String> searchProductPages(String url) {
        final Document document = ParserUtils.loadDocument(url);
        final Set<String> productPages = Sets.newHashSet(url);
        if (Objects.nonNull(document)) {
            final Element pages = document.getElementsByClass("uk-pagination uk-margin-top uk-margin-bottom pagination").first();
            if (Objects.nonNull(pages)) {
                final Elements children = pages.children();
                final Element element = children.get(children.size() - 1);
                for (int i = NumberUtils.toInt(element.text()); i > 1; i--) {
                    productPages.add(url + i);
                }
            }
        }
        return productPages;
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        final Document document = ParserUtils.loadDocument(url);
        if (Objects.nonNull(document)) {
            final Map<String, String> categories = Maps.newHashMap();
            final Elements productElements = document.getElementsByAttributeValue("itemtype", "http://schema.org/Product");
            for (Element element : productElements) {
                final Element link = element.getElementsByClass("heading_c user-common-link").first();
                if (Objects.nonNull(link)) {
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    categories.put(UUID.randomUUID().toString(), href);
                }
            }
            if (MapUtils.isEmpty(categories)) {
                final String href = url
                        .replace(getSite().getDomain(), StringUtils.EMPTY);
                categories.put(UUID.randomUUID().toString(), href);
            }
            return categories;
        }
        return Collections.emptyMap();
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {

                final Product product = new Product();
                final Element price = document.getElementsByAttributeValue("itemprop", "price").first();

                if (Objects.nonNull(price)) {
                    product.setPrice(Long.valueOf(price.text()));
                }
                final Elements tech = document.getElementsByClass("md-list md-list-right");
                if (Objects.nonNull(tech) && tech.size() == 1) {
                    final Elements properties = tech.first().children();
                    Map<String, String> techProperties = Maps.newHashMap();
                    for (Element element : properties) {
                        final Elements tds = element.getElementsByTag("td");
                        String key = StringUtils.EMPTY;
                        String value = StringUtils.EMPTY;
                        for (Element td : tds) {
                            if (td.className().equalsIgnoreCase("uk-text-small uk-text-right")
                                    || td.className().equalsIgnoreCase("uk-text-small uk-text-right uk-text-capitalize")) {
                                value = td.text();
                            } else {
                                key = td.text();
                            }
                        }
                        if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value)) {
                            techProperties.put(key.trim(), value.trim());
                        }
                    }
                    final String brand = techProperties.remove("Бренд");
                    final String vendorCode = techProperties.remove("Артикул");
                    final String country = techProperties.remove("Страна производства");
                    final Map<String, Object> property = Maps.newHashMap();
                    property.put("brand", brand);
                    property.put("vendorCode", vendorCode);
                    property.put("country", country);

                    brands.add(brand);
                    countries.add(country);

                    product.setTechProperties(techProperties);
                    product.setProperties(property);
                }

                final String name = document.getElementsByTag("head").first().getElementsByTag("title").first().text();
                product.setName(name);

                final Elements images = document.getElementsByClass("fotorama");
                if (Objects.nonNull(images)) {
                    final Element first = images.first();
                    final Elements img = first.getElementsByTag("img");
                    if (Objects.nonNull(img)) {
                        String imageUrl = img.attr("src");
                        product.setImage(imageUrl);
                    }
                }

                document.getElementsByClass("md-card-toolbar-heading-text")
                        .stream()
                        .filter(f -> f.text().trim().equalsIgnoreCase("описание товара"))
                        .findFirst().flatMap(element -> element.parent().parent().children()
                        .stream()
                        .filter(f -> f.className().equalsIgnoreCase("md-card-content"))
                        .findFirst()).ifPresent(f ->
                        product.setDescription(f.children().outerHtml()));

                product.setSiteUrl(getSite(), url);
                consumer.accept(product);
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

    @Override
    protected Site getSite() {
        return Site.METALCITY;
    }

}
