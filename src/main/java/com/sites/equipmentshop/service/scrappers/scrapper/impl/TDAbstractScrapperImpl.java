package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TDAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(TDAbstractScrapperImpl.class);

    private static final Map<String, Object> brans = Maps.newHashMap();
    private static final Map<String, Object> countries = Maps.newHashMap();
    private static final Map<String, String> branST = Maps.newHashMap();

    static {
        branST.put("JEJU", "tm-jeju");
        branST.put("Robot Coupe", "tm-robot_coupe");
        branST.put("Steno", "tm-steno");
        branST.put("Casadio", "tm-casadio");
        branST.put("La Cimbali", "tm-la_cimbali");
        branST.put("Alliance", "tm-alliance");
        branST.put("Stalgast", "tm-stalgast");
        branST.put("SMEG", "tm-smeg");
        branST.put("Rational", "tm-rational");
        branST.put("WellPizza", "tm-wellpizza");
        branST.put("Pavoni", "tm-pavoni");
        branST.put("Liebherr", "tm-liebherr");
        branST.put("Cambro", "tm-cambro");
        branST.put("Eqta", "tm-eqta");
        branST.put("Tecnoeka", "tm-tecnoeka");
        branST.put("Sigma", "tm-sigma");
        branST.put("Eksi", "tm-eksi");
        branST.put("Saeco", "tm-saeco");
        branST.put("Roller Grill", "tm-roller_grill");
        branST.put("WLBake", "tm-wlbake");
        branST.put("Tecnomac", "tm-tecnomac");
        branST.put("Sinmag", "tm-sinmag");
        branST.put("Vortmax", "tm-vortmax");
        branST.put("Fagor", "tm-fagor");
        branST.put("Electrolux", "tm-electrolux");
        branST.put("Unox", "tm-unox");
        branST.put("Sirman", "tm-sirman");
        branST.put("Brema", "tm-brema");

        brans.put("tm-jeju", 887);
        brans.put("tm-robot_coupe", 272);
        brans.put("tm-steno", 886);
        brans.put("tm-casadio", 1755);
        brans.put("tm-la_cimbali", 883);
        brans.put("tm-alliance", 884);
        brans.put("tm-stalgast", 882);
        brans.put("tm-smeg", 878);
        brans.put("tm-rational", 348);
        brans.put("tm-wellpizza", 345);
        brans.put("tm-pavoni", 879);
        brans.put("tm-liebherr", 889);
        brans.put("tm-cambro", 880);
        brans.put("tm-eqta", 875);
        brans.put("tm-tecnoeka", 874);
        brans.put("tm-sigma", 885);
        brans.put("tm-eksi", 267);
        brans.put("tm-saeco", 1753);
        brans.put("tm-roller_grill", 873);
        brans.put("tm-wlbake", 351);
        brans.put("tm-tecnomac", 888);
        brans.put("tm-sinmag", 350);
        brans.put("tm-vortmax", 871);
        brans.put("tm-fagor", 872);
        brans.put("tm-electrolux", 270);
        brans.put("tm-unox", 347);
        brans.put("tm-sirman", 870);
        brans.put("tm-brema", 355);

        countries.put("США", 881);
        countries.put("Чехия", 876);
        countries.put("Словения", 877);
        countries.put("Австрия", 890);
        countries.put("Малайзия", 891);
        countries.put("Румыния", 1754);
        countries.put("Россия", 240);
        countries.put("Бельгия", 241);
        countries.put("Китай", 242);
        countries.put("Польша", 243);
        countries.put("Литва", 244);
        countries.put("Беларусь", 317);
        countries.put("Турция", 346);
        countries.put("Индия", 349);
        countries.put("Швейцария", 1751);
        countries.put("Тайвань", 352);
        countries.put("Сербия", 1752);
        countries.put("Италия", 266);
        countries.put("Германия", 269);
        countries.put("Франция", 271);
        countries.put("Гонконг", 353);
        countries.put("Испания", 354);

    }

    public TDAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                  FTPUtils ftpUtils,
                                  ScrapperDAO scrapperDAO,
                                  ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        LOGGER.info("Search categories");
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
        if (Objects.nonNull(document)) {
            final Elements categoriesClass = document.getElementsByClass("sc-item");
            final Map<String, String> categories = Maps.newHashMap();
            if (CollectionUtils.isNotEmpty(categoriesClass)) {
                for (Element item : categoriesClass) {
                    final Elements a1 = item.getElementsByTag("a");
                    final Element first = a1.first();
                    final String href = first.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    final String categoryName = getText(first);
                    categories.putAll(searchCategories(href, categoryName));
                }
            } else if (StringUtils.isNoneBlank(category)) {
                Map<String, String> sub = Maps.newHashMap();
                final Element subCategories = document.getElementById("leftMenuSub_equipmentGroup");
                if (Objects.nonNull(subCategories)) {
                    final Map<String, String> subCategoriesMap = subCategories.children().stream().collect(Collectors.toMap(f -> f.getElementsByTag("a").attr("href"), f -> f.text()));
                    for (Map.Entry<String, String> entry : subCategoriesMap.entrySet()) {
                        getDoc(getSite().getDomain() + entry.getKey(), doc -> {
                            final Element brands = document.getElementById("leftMenuSub_tms");
                            if (Objects.nonNull(brands) && Objects.nonNull(brands.child(0)) && CollectionUtils.isNotEmpty(brands.children())) {
                                if (brans.keySet().stream().anyMatch(bran -> brands.children().stream().anyMatch(f -> StringUtils.containsIgnoreCase(f.child(0).child(0).attr("href"), bran)))) {
                                    sub.putAll(ImmutableMap.of(entry.getKey(), String.format("%s -> %s", category, entry.getValue())));
                                }
                            }
                        });
                    }
                    return sub;
                }
                return ImmutableMap.of(url, category);
            }
            return categories;
        }
        return Collections.emptyMap();
    }

    private void getDoc(String url, Consumer<Document> consumer) {
        final Document document = ParserUtils.loadDocument(url);
        if (Objects.nonNull(document)) {
            consumer.accept(document);
        }
    }

    private static String getText(Element first) {
        final Elements elementsByClass = first.getElementsByClass("sci-title");
        final String text = elementsByClass.first().textNodes().stream().map(TextNode::getWholeText).collect(Collectors.joining(" "));
        return text
                .replaceAll("[\\n\\r<>br]", StringUtils.EMPTY)
                .replaceAll("- ", "-")
                .replaceAll(" -", "-")
                .replaceAll(" - ", "-");
    }

    @Override
    public Set<String> searchProductPages(String url) {
        final Set<String> pages = Sets.newHashSet();
        Set<String> brands = Sets.newHashSet();

        final Document tDocument = ParserUtils.loadDocument(url);
        if (Objects.nonNull(tDocument)) {
            final Element sBrands = tDocument.getElementById("leftMenuSub_tms");
            if (Objects.nonNull(sBrands) && Objects.nonNull(sBrands.child(0)) && CollectionUtils.isNotEmpty(sBrands.children())) {
                brands = branST.values()
                        .stream()
                        .filter(s -> sBrands.children()
                                .stream()
                                .anyMatch(w -> StringUtils.containsIgnoreCase(w.child(0).child(0).attr("href"), s)))
                        .collect(Collectors.toSet());
            } else {
                return Collections.emptySet();
            }
        }
        for (String brand : brands) {
            final Document document = ParserUtils.loadDocument(url + brand);
            if (Objects.nonNull(document)) {
                final Elements pagination = document.getElementsByClass("page");
                if (CollectionUtils.isNotEmpty(pagination)) {
                    final List<String> tPages = pagination.first().getElementsByClass("pitem").stream().map(Element::text)
                            .collect(Collectors.toList());
                    final int lastPage = NumberUtils.toInt(tPages.get(tPages.size() - 1));

                    for (int i = 2; i <= lastPage; i++) {
                        pages.add(String.format("%s/page-%d", url + brand, i));
                    }
                }
            }
            pages.add(url + brand);
        }
        return pages;
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        LOGGER.info("Search products");
        try {
            final Document document = ParserUtils.loadDocument(url);
            if (Objects.nonNull(document)) {
                final Elements subCategoriesClass = document.getElementsByClass("catalog").first().getElementsByClass("list").first().getElementsByClass("item");
                final Map<String, String> categories = Maps.newHashMap();
                for (Element productElement : subCategoriesClass) {
                    final Element link = productElement.getElementsByTag("a").first();
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);

                    categories.put(UUID.randomUUID().toString(), href);
                }
                return categories;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {
                final Product product = new Product();

                final Elements items = document.getElementsByClass("nolimit");
                final Elements path = document.getElementsByClass("paths");
                final Elements description = document.getElementsByClass("textout");
                final Element first = items.first();
                final Elements item = first.getElementsByClass("item");
                final Element child = item.first().child(0);
                final Element codes = findChildByClassName(child, "codes");
                final Map<String, Object> properties = Maps.newHashMap();
                try {
                    for (Element code : codes.children()) {
                        final String tempCode = code.toString().replaceAll("[<>/\\n]*(strong)*(div)*", StringUtils.EMPTY);
                        final String[] split = tempCode.split(":");

                        if (split[0].trim().equalsIgnoreCase("Артикул")) {
                            properties.put("vendorCode", split[1].trim());
                        }

                        if (split[0].trim().equalsIgnoreCase("Страна")) {
                            properties.put("country", countries.get(split[1].trim()));
                        }

                    }
                } catch (Exception ignored) {
                }
                final Element features = findChildByClassName(child, "features");
                final Map<String, String> tech = getMap(features);

                final Optional<Map.Entry<String, String>> tempBrand = tech.entrySet()
                        .stream()
                        .filter(f -> f.getKey().equalsIgnoreCase("Бренд"))
                        .findFirst();

                if (tempBrand.isPresent()) {
                    branST.entrySet().stream().filter(f -> StringUtils.equalsIgnoreCase(f.getKey(), tempBrand.get().getValue()))
                            .findFirst()
                            .ifPresent(brand -> {
                                properties.put("brand", brans.get(brand.getValue()));
                            });
                    tech.keySet().removeIf(f -> f.equalsIgnoreCase("Бренд"));
                } else {
                    return;
                }
                product.setProperties(properties);
                final Element image = findChildByClassName(first, "image");

                product.setName(path.parents().first().child(1).child(0).text());
                try {
                    product.setDescription(description.first().child(0).text());
                } catch (Exception ignored) {

                }
                product.setTechProperties(tech);

                final Element pricemain = findChildByClassName(item.first(), "pricemain");
                if (Objects.nonNull(pricemain)) {
                    final String price = pricemain.text();
                    Pattern p = Pattern.compile("\\d+");
                    Matcher m = p.matcher(price.replaceAll(StringUtils.SPACE, StringUtils.EMPTY));
                    if (m.find()) {
                        product.setPrice(Long.valueOf(m.group()));
                    }
                }

                final String imageUrl = image.child(0).child(0).child(0).child(1).attr("src");
                product.setImage(imageUrl);
                product.setSiteUrl(getSite(), url);
                consumer.accept(product);
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

    private static Map<String, String> getMap(Element features) {
        Map<String, String> tech = Maps.newHashMap();
        try {
            final Elements children = features.children();
            for (Element element : children) {
                if (isFound("line", element)) {
                    final String key = findChildByClassName(element, "name").text();
                    final String value = findChildByClassName(element, "value").text();
                    tech.put(key, value);
                }
            }
        } catch (Exception ignored) {
        }
        return tech;
    }

    private static Element findChildByClassName(Element element, String className) {
        final Elements children = element.children();
        if (!isFound(className, element) && Objects.nonNull(children)) {
            for (Element child : children) {
                final Element childByClassName = findChildByClassName(child, className);
                if (Objects.nonNull(childByClassName)) {
                    return childByClassName;
                }
            }
        } else if (isFound(className, element)) {
            return element;
        }
        return null;
    }

    private static boolean isFound(String className, Element child) {
        return child.classNames().stream().anyMatch(f -> f.equalsIgnoreCase(className));
    }

    @Override
    protected Site getSite() {
        return Site.TD;
    }

}
