package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.NumberUtils;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

public class MariholodAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(MariholodAbstractScrapperImpl.class);

    public MariholodAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                         FTPUtils ftpUtils,
                                         ScrapperDAO scrapperDAO,
                                         ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
        final Map<String, String> categories = Maps.newHashMap();
        if (Objects.nonNull(document)) {
            final Element products = document.getElementsByClass("products").first();
            if (Objects.nonNull(products)) {
                for (Element vCategory : products.children()) {
                    final String link = vCategory.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    final String name = vCategory.getElementsByClass("category-card__title").first().text();
                    categories.putAll(searchCategories(link, name));
                }
            } else {
                return ImmutableMap.of(url, category);
            }
        }
        return categories;
    }

    @Override
    public Set<String> searchProductPages(String url) {
        return Sets.newHashSet(url);
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        try {
            final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
            if (Objects.nonNull(document)) {
                final Elements subCategoriesClass = document.getElementsByClass("category-product__title");
                final Map<String, String> categories = Maps.newHashMap();

                for (Element category : subCategoriesClass) {
                    final Element link = category.getElementsByTag("a").first();
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    categories.put(link.text(), href);
                }
                return categories;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {
                final Product product = new Product();
                product.setSiteUrl(getSite(), url);
                final Element item = document.getElementsByClass("product").first();
                final Element image = item.getElementsByClass("gallery__active-image").first();
                final Elements tech = document.getElementsByClass("product-table__row");
                final Element price = item.getElementsByClass("product-table").first();

                try {
                    for (Element row : price.children()) {
                        if (row.child(0).text().equalsIgnoreCase("Рекомендованная розничная цена с НДС")) {
                            final String replace = row.child(1).text().replace("₽", StringUtils.EMPTY);
                            product.setPrice(NumberUtils.parseNumber(replace, Long.class));
                            break;
                        }
                    }
                } catch (Exception ignore) {
                }

                Map<String, String> techMap = Maps.newHashMap();
                try {
                    for (Element row : tech) {
                        try {
                            String key = row.child(0).text();
                            String value = row.child(1).text();
                            techMap.put(key, value);
                        } catch (Exception e) {
                            LOGGER.error("", e);
                        }
                    }
                } catch (Exception ignored) {
                }
                techMap.remove("Рекомендованная розничная цена с НДС");
                final String article = techMap.remove("Код завода");
                final Map<String, Object> properties = Maps.newHashMap();
                properties.put("country", 240);
                properties.put("brand", 343);
                properties.put("vendorCode", article);

                product.setName(item.getElementsByClass("product__title").first().text());
                product.setTechProperties(techMap);
                product.setProperties(properties);
                final String imageUrl = image.attr("src")
                        .replace(".pngg", ".png");
                product.setImage(imageUrl);
                consumer.accept(product);
            }
        } catch (Exception e) {
            LOGGER.error("Error product {}.", getSite().getDomain() + url, e);
        }
    }

    @Override
    protected Site getSite() {
        return Site.MARIHOLOD;
    }
}
