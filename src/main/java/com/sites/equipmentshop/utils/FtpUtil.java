package com.sites.equipmentshop.utils;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class FtpUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FtpUtil.class);

    @Value("${ftp.login}")
    private String ftpLogin;
    @Value("${ftp.password}")
    private String ftpPassword;
    @Value("${ftp.host}")
    private String ftpHost;
    private final FTPClient ftpClient = new FTPClient();

    public Boolean share(InputStream inputStream, String fileName) {
        try {
            ftpClient.connect(ftpHost);
            LOGGER.info("Trying login. Login = {}, Password = {}", ftpLogin, ftpPassword);
            ftpClient.login(ftpLogin, ftpPassword);
            return shareFileToFTP(inputStream, fileName);
        } catch (Exception ex) {
            LOGGER.error("", ex);
        }
        return Boolean.FALSE;
    }

    private boolean shareFileToFTP(InputStream inputStream, String fileName) throws IOException {
        Boolean isUploaded = Boolean.FALSE;
        try {
            long startTime = System.currentTimeMillis();
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
            ftpClient.storeFile(fileName, inputStream);

            String time = DurationFormatUtils.formatPeriod(startTime, System.currentTimeMillis(), "HH:mm:ss");
            LOGGER.info("upload file {} to ftp {}. Time: {}",
                    fileName,
                    ftpHost,
                    time);

        } catch (Exception e) {
            LOGGER.error("FTP uploading error.", e);
        } finally {
            closeFtpConnection();
        }
        return isUploaded;
    }

    public void removeFiles(Collection<String> filesToRemove) throws IOException {
        ftpClient.connect(ftpHost);
        LOGGER.info("Trying login. Login = {}, Password = {}", ftpLogin, ftpPassword);
        ftpClient.login(ftpLogin, ftpPassword);
        try {
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);

            for (String file : filesToRemove) {
                ftpClient.deleteFile(file);
            }
        } catch (Exception e) {
            LOGGER.error("FTP uploading error.", e);
        } finally {
            closeFtpConnection();
        }
    }

    public Set<String> getAllFilesFtp() throws IOException {
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.connect(ftpHost);
        LOGGER.info("Trying login. Login = {}, Password = {}", ftpLogin, ftpPassword);
        ftpClient.login(ftpLogin, ftpPassword);
        try {
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);

            return Stream.of(ftpClient.listFiles())
                    .map(FTPFile::getName)
                    .collect(Collectors.toSet());

        } catch (Exception e) {
            LOGGER.error("FTP uploading error.", e);
            throw new IllegalStateException(e);
        } finally {
            closeFtpConnection();
        }
    }

    private void closeFtpConnection() throws IOException {
        if (ftpClient.isConnected()) {
            LOGGER.info("Logout");
            ftpClient.logout();
            LOGGER.info("Disconect");
            ftpClient.disconnect();
        }
    }

}
