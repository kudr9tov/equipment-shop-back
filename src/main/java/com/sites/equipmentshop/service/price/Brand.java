package com.sites.equipmentshop.service.price;

public enum Brand {
    ABAT,
    POLAIR,
    MARIHOLOD,
    ATESY,
    POLUS,
    PROMET,
    CRISPY;
}
