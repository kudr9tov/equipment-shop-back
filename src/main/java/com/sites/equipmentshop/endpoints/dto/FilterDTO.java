package com.sites.equipmentshop.endpoints.dto;

import com.sites.equipmentshop.service.enums.FilterType;

public class FilterDTO {
    private String filterColumn;
    private String queryString;
    private FilterType type;

    public String getFilterColumn() {
        return filterColumn;
    }

    public void setFilterColumn(String filterColumn) {
        this.filterColumn = filterColumn;
    }

    public FilterType getType() {
        return type;
    }

    public void setType(FilterType type) {
        this.type = type;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }
}
