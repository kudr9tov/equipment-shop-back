package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.SchedulingEmailDTO;
import com.sites.equipmentshop.endpoints.dto.SchedulingScrapperDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.service.SchedulingService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/scheduling", consumes = {"*/*", "*", "application/json"}, produces = {"*/*", "application/json"})
public class SchedulingController {

    private final SchedulingService schedulingService;

    public SchedulingController(SchedulingService schedulingService) {
        this.schedulingService = schedulingService;
    }

    @PostMapping(value = "/scrapper")
    public void scheduleScrapper(@RequestBody SchedulingScrapperDTO dto) {
        schedulingService.scheduleScrapper(dto);
    }

    @PostMapping(value = "/mail")
    public void scheduleMail(@RequestBody SchedulingEmailDTO dto) {
        schedulingService.scheduleMail(dto);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteJob(@PathVariable("id") Integer id) {
        schedulingService.deleteJob(id);
    }

    @GetMapping(value = "/scrapper/{siteId}")
    public ResponseEntity<Object> getScrapperJob(@PathVariable("siteId") Integer siteId) {
        return ResponseEntityUtils.responseEntity(schedulingService.getScrapperJob(siteId));
    }


    @GetMapping(value = "/mail")
    public ResponseEntity<Object> getMailJob() {
        return ResponseEntityUtils.responseEntity(schedulingService.getMailJob());
    }

    @PostMapping(value = "/scrapper/{id}")
    public void updateScrapperJob(@PathVariable("id") Integer id, @RequestBody SchedulingScrapperDTO dto) {
        schedulingService.deleteJob(id);
        schedulingService.scheduleScrapper(dto);
    }

    @PostMapping(value = "/mail/{id}")
    public void updateMailJob(@PathVariable("id") Integer id, @RequestBody SchedulingEmailDTO dto) {
        schedulingService.deleteJob(id);
        schedulingService.scheduleMail(dto);
    }

}
