package com.sites.equipmentshop.service.scrappers.utils;

import com.sites.equipmentshop.utils.FtpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.UUID;

@Component
public class FTPUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(FTPUtils.class);

    final FtpUtil ftp;

    @Autowired
    public FTPUtils(FtpUtil ftp) {
        this.ftp = ftp;
    }

    public String copyImageToFTP(String imageUrl) {
        try {
            InputStream in = new URL(imageUrl).openStream();
            final String fileName = UUID.randomUUID().toString() + ".jpg";
            ftp.share(in, fileName);
            return fileName;
        } catch (IOException e) {
            LOGGER.error("Can't copy file to ftp.", e);
        }
        return null;
    }
}
