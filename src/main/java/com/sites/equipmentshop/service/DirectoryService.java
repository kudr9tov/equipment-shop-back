package com.sites.equipmentshop.service;

import com.sites.equipmentshop.endpoints.dto.DirectoryDTO;
import com.sites.equipmentshop.endpoints.dto.MoveCategoriesDTO;
import com.sites.equipmentshop.endpoints.dto.MultiDirectoryDTO;
import com.sites.equipmentshop.persistence.repositories.DirectoryDAO;
import com.sites.equipmentshop.service.enums.Directory;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class DirectoryService {
    private final DirectoryDAO directoryRepo;

    @Autowired
    public DirectoryService(final JdbcTemplate jdbcTemplate,
                            final DirectoryDAO directoryRepo) {
        this.directoryRepo = directoryRepo;
    }

    public String getCategoryConfig() {
        return directoryRepo.getCategoriesConfig();
    }

    public String getCountriesConfig() {
        return directoryRepo.getSimpleDirectoryConfig(Directory.COUNTRY);
    }

    public String getBrandConfig() {
        return directoryRepo.getSimpleDirectoryConfig(Directory.BRAND);
    }

    public String getCertConfig() {
        return directoryRepo.getSimpleDirectoryConfig(Directory.CERT);
    }

    public String getAllConfigs() {
        return directoryRepo.getAllConfigs();
    }

    public void addDirectoryValue(DirectoryDTO directoryDTO) {
        final Directory type = directoryDTO.getType();
        if (Directory.CATEGORY.equals(type)) {
            directoryRepo.addSubCategory(directoryDTO);
        } else {
            directoryRepo.addToDirectory(directoryDTO);
        }
    }

    public void updateDirectoryValue(DirectoryDTO directoryDTO) {
        directoryRepo.updateDirectoryValue(directoryDTO);
    }

    public void deleteDirectoryValue(DirectoryDTO directoryDTO) {
        final Directory type = directoryDTO.getType();
        if (Directory.CATEGORY.equals(type) && Objects.nonNull(directoryDTO.getParentId())) {
            directoryRepo.removeSubCategory(directoryDTO);
        } else {
            directoryRepo.removeDirectoryValue(directoryDTO.getId(), directoryDTO.getType());
        }
    }

    public String addDirectoryValues(MultiDirectoryDTO dto) {
        if (Directory.CATEGORY.equals(dto.getType())) {
            return directoryRepo.addCategoriesToDirectory(dto);
        } else {
            directoryRepo.addValuesToDirectory(dto);
        }
        return new JSONObject().toJSONString();
    }

    public void updateDirectoryValues(MultiDirectoryDTO dto) {
        directoryRepo.updateValuesToDirectory(dto);
    }

    public void moveCategories(MoveCategoriesDTO dto) {
        for (Integer categoryId : dto.getCategoryIds()) {
            directoryRepo.moveCategory(dto.getParentId(), categoryId);
        }
    }
}
