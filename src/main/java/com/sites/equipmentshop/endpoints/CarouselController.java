package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.CarouselDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.service.CarouselService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/carousel", consumes = {"*/*", "*", "application/json"}, produces = {"*/*", "application/json"})
public class CarouselController {

    private CarouselService carouselService;

    @Autowired
    public CarouselController(CarouselService carouselService) {
        this.carouselService = carouselService;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getSlides() {
        return ResponseEntityUtils.responseEntity(carouselService.getSlideTree());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getSlide(@PathVariable Integer id) {
        return ResponseEntityUtils.responseEntity(carouselService.getSlide(id));
    }

    @GetMapping(value = "/step/{step}")
    public ResponseEntity<Object> getSlideByStep(@PathVariable Integer step) {
        return ResponseEntityUtils.responseEntity(carouselService.getSlideByStep(step));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteSlide(@PathVariable Integer id) {
        carouselService.deleteSlide(id);
    }

    @PutMapping(value = "")
    public ResponseEntity<Object> updateSlide(@RequestBody List<CarouselDTO> SlideDTO) {
            return ResponseEntityUtils.responseEntity(carouselService.updateSlide(SlideDTO));
    }

    @PostMapping(value = "")
    public ResponseEntity<Object> saveSlide(@RequestBody List<CarouselDTO> SlideDTO) {
        return ResponseEntityUtils.responseEntity(carouselService.saveSlide(SlideDTO));
    }

}
