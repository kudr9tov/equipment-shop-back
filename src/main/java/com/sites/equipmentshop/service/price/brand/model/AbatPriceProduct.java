package com.sites.equipmentshop.service.price.brand.model;

import com.poiji.annotation.ExcelCell;
import com.poiji.annotation.ExcelRow;

public class AbatPriceProduct {
    @ExcelRow
    private int rowIndex;

    @ExcelCell(0)
    private String article;

    @ExcelCell(1)
    private String description;

    @ExcelCell(7)
    private Long priceNds;

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPriceNds() {
        return priceNds;
    }

    public void setPriceNds(Long priceNds) {
        this.priceNds = priceNds;
    }
}
