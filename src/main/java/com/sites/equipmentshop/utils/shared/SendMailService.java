package com.sites.equipmentshop.utils.shared;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Stream;

@Service
public class SendMailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SendMailService.class);

    private final String emailLogin;
    private final String emailPassword;

    public static SendMailService sendMail(String emailLogin, String emailPassword) {
        return new SendMailService(emailLogin, emailPassword);
    }

    private SendMailService(@Value("${equipments.mail.login}") String emailLogin, @Value("${equipments.mail.password}") String emailPassword) {
        this.emailLogin = emailLogin;
        this.emailPassword = emailPassword;
    }

    public void sendMessage(String person, String toEmail, String subject, String body, String... additionalText) throws MessagingException {
        Message message = baseConfiguration(person);
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(toEmail));
        message.setSubject(subject);
        try {
            Multipart multipart = new MimeMultipart();
            MimeBodyPart htmlBodyPart = new MimeBodyPart();
            htmlBodyPart.setContent(body, "text/html; charset=UTF-8");
            multipart.addBodyPart(htmlBodyPart);

            if (ArrayUtils.isNotEmpty(additionalText) && StringUtils.isNotBlank(additionalText[0])) {
                MimeBodyPart textBodyPart = new MimeBodyPart();
                textBodyPart.setContent(additionalText[0], "text/plain; charset=UTF-8");
                multipart.addBodyPart(textBodyPart);
            }

            message.setContent(multipart);
            String unFollowLink = "<https://gktorg.ru/unfollow?email=" + toEmail + ">";
            message.setHeader("List-Unsubscribe", unFollowLink);
            message.setHeader("Precedence", "bulk");
            LOGGER.debug(emailLogin);
            LOGGER.debug(emailPassword);
            final Transport smtp = message.getSession().getTransport("smtp");
            smtp.connect(emailLogin, emailPassword);
            smtp.sendMessage(message, message.getAllRecipients());
            smtp.close();
        } catch (Exception e) {
            LOGGER.error("error: ",e);
        }
    }


    public void sendMessage(String toEmail, String subject, String body) throws MessagingException {
        Message message = baseConfiguration("ГлавКом Инфо");
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(toEmail));
        message.setSubject(subject);
        try {
            Multipart multipart = new MimeMultipart();
            MimeBodyPart htmlBodyPart = new MimeBodyPart();
            htmlBodyPart.setContent(body, "text/html; charset=UTF-8");
            multipart.addBodyPart(htmlBodyPart);

            message.setContent(multipart);
            String unFollowLink = "<https://gktorg.ru/api/mail/unfollow?email=" + toEmail + ">";
            message.setHeader("List-Unsubscribe", unFollowLink);
            Transport.send(message);
        } catch (Exception e) {
            LOGGER.error("error: ",e);
        }
    }

    private Message baseConfiguration(String... person) throws MessagingException {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.yandex.ru");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.put("mail.smtp.ssl", "true");
//        props.put("mail.smtp.ssl.trust", "*");

        Session session = getSession(props);
        session.setDebug(Boolean.TRUE);
        Message message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(emailLogin, person[0]));
        } catch (Exception e) {
            message.setFrom(new InternetAddress(emailLogin));
        }
        return message;
    }

    public void sendMessageMoreThanOne(String subject, String body, String... toEmail) throws MessagingException {
        Message message = baseConfiguration("ГлавКом Инфо");
        final InternetAddress[] toEmails = Stream.of(toEmail)
                .map(email -> {
                    try {
                        return new InternetAddress(email);
                    } catch (AddressException e) {
                        LOGGER.error("Can't build InternetAddress for email: {}", email, e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .toArray(InternetAddress[]::new);

        message.setRecipients(Message.RecipientType.TO, toEmails);
        message.setSubject(subject);
        Multipart multipart = new MimeMultipart();
        MimeBodyPart htmlBodyPart = new MimeBodyPart();
        htmlBodyPart.setContent(body, "text/html; charset=UTF-8");
        multipart.addBodyPart(htmlBodyPart);
        message.setContent(multipart);
        String unFollowLink = "<https://gktorg.ru/api/mail/unfollow?email=" + toEmail + ">";
        message.setHeader("List-Unsubscribe", unFollowLink);
        Transport.send(message);
    }

    private Session getSession(Properties props) {
        return Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailLogin, emailPassword);
            }
        });
    }

}
