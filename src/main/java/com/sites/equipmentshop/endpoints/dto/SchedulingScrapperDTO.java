package com.sites.equipmentshop.endpoints.dto;

import javax.validation.constraints.NotNull;

public class SchedulingScrapperDTO {
    @NotNull
    private ScrapperDTO dto;
    @NotNull
    private String cron;

    public ScrapperDTO getDto() {
        return dto;
    }

    public void setDto(ScrapperDTO dto) {
        this.dto = dto;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }
}
