package com.sites.equipmentshop.service;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.sites.equipmentshop.endpoints.MailController;
import com.sites.equipmentshop.endpoints.dto.EmailDTO;
import com.sites.equipmentshop.endpoints.dto.EmailsFilterDTO;
import com.sites.equipmentshop.endpoints.dto.FilterDTO;
import com.sites.equipmentshop.persistence.entities.EmailTemplate;
import com.sites.equipmentshop.persistence.repositories.MailDAO;
import com.sites.equipmentshop.security.services.MailTemplateFactory;
import com.sites.equipmentshop.service.enums.FilterType;
import com.sites.equipmentshop.utils.shared.SendMailService;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class EmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailController.class);

    private final MailDAO mailDAO;
    private final SendMailService sendMailService;
    private final MailTemplateFactory templateFactory;
    private final ExecutorService taskExecutor;
    private AtomicReference<Future<?>> task = new AtomicReference<>();

    @Autowired
    public EmailService(final JdbcTemplate jdbcTemplate,
                        final MailDAO mailDAO,
                        final MailTemplateFactory templateFactory,
                        @Qualifier("fixedThreadPool") final ExecutorService taskExecutor,
                        final SendMailService sendMailService) {
        this.mailDAO = mailDAO;
        this.sendMailService = sendMailService;
        this.taskExecutor = taskExecutor;
        this.templateFactory = templateFactory;
    }

    public String getMails(EmailsFilterDTO dto) {

        Map<String, String> association = Maps.newHashMap();
        association.put("isSend", "issend");
        association.put("isSendTemplate", "is_send_template");
        association.put("lastSendMessage", "lastsendmessage");
        List<FilterDTO> filters = dto.getFilters();
        String order = filters
                .stream()
                .filter(f -> !f.getType().equals(FilterType.QUERY))
                .map(f -> String.format("%s %s", Optional.ofNullable(association.get(f.getFilterColumn())).orElse(f.getFilterColumn()), f.getType()))
                .collect(Collectors.joining(","));
        String query = filters
                .stream()
                .filter(f -> f.getType().equals(FilterType.QUERY))
                .map(f -> String.format("cast(%s as TEXT) ~ '%s'", Optional.ofNullable(association.get(f.getFilterColumn())).orElse(f.getFilterColumn()), f.getQueryString()))
                .collect(Collectors.joining(" AND "));

        String mails = mailDAO.getMails(order, query);
        JSONObject object = new JSONObject();

        List mailsList = new Gson().fromJson(mails, List.class);
        Object[] result = mailsList.stream().skip(dto.getRightPage() * dto.getLimit()).limit(dto.getLimit()).toArray();
        object.put("mails", result);
        object.put("count", mailsList.size());
        object.put("page", dto.getPage());
        object.put("limit", dto.getLimit());
        return object.toJSONString();
    }

    public void addMails(List<String> emails) {
        mailDAO.addEmails(emails);
    }

    public EmailTemplate getTemplateById(Integer id) {
        return mailDAO.getMailTemplateById(id);
    }

    public void unfollowMailing(String email) {
        if (StringUtils.hasText(email)) {
            mailDAO.unfollowMailing(email);
        }
    }

    public void sendTemplateToRecipients(EmailDTO emailDTO) {
        if (!CollectionUtils.isEmpty(emailDTO.getEmailIds())) {
            List<String> mails = mailDAO.getMailsByIds(emailDTO.getEmailIds());
            runTask(mails, emailDTO);
        } else if (!CollectionUtils.isEmpty(emailDTO.getEmails())) {
            mailDAO.addEmails(emailDTO.getEmails());
            runTask(emailDTO.getEmails(), emailDTO);
        } else if (emailDTO.getSendAll()) {
            final List<String> allMails = mailDAO.getAllMails()
                    .stream()
                    .limit(50)
                    .collect(Collectors.toList());
            runTask(allMails, emailDTO);
        }
    }

    public Boolean hasActiveTask() {
        return ((ThreadPoolExecutor) taskExecutor).getActiveCount() > 0;
    }

    private void runTask(List<String> emails, EmailDTO emailDTO) {
        final EmailTemplate emailTemplate = mailDAO.getMailTemplateById(emailDTO.getTemplateId());
        AtomicInteger counter = new AtomicInteger();
        if (emailDTO.getSendAll()) {
            task.set(taskExecutor.submit(() -> {
                for (String email : emails) {
                    try {
                        sendMail(emailTemplate, email, emailDTO);
                        LOGGER.info("Sent template to {} successfully. Sent {}:{} mails", email, counter.incrementAndGet(), emails.size());
                        if (Thread.currentThread().isInterrupted()) {
                            LOGGER.info("Stopped mails task");
                            break;
                        }
                        if (emails.size() > 1) {
                            TimeUnit.MINUTES.sleep((long) (Math.random() * 30) + 1);
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        LOGGER.error("Can't sleep subtask", e);
                        break;
                    }
                }
            }));
        } else {
            for (String email : emails) {
                try {
                    sendMail(emailTemplate, email, emailDTO);
                    LOGGER.info("Sent template to {} successfully. Sent {}:{} mails", email, counter.incrementAndGet(), emails.size());
                    if (emails.size() > 1) {
                        TimeUnit.MINUTES.sleep((long) (Math.random() * 1) + 2);
                    }
                } catch (InterruptedException e) {
                    LOGGER.error("Can't sleep subtask", e);
                    break;
                }
            }
        }

    }

    public boolean stopTask() {
        task.get().cancel(Boolean.TRUE);
        return task.get().isCancelled() || task.get().isDone();
    }

    private void sendMail(EmailTemplate emailTemplate, String email, EmailDTO emailDTO) {
        try {
            sendMailService.sendMessage(
                    emailTemplate.getPerson(),
                    email,
                    StringUtils.isEmpty(emailDTO.getSubject()) ? emailTemplate.getSubject() : emailDTO.getSubject(),
                    emailDTO.getSendTemplate()
                            ? templateFactory.prepareTemplate(emailTemplate.getTemplate(), email)
                            : StringUtils.isEmpty(emailDTO.getMessage())
                            ? emailTemplate.getMessage()
                            : emailDTO.getMessage(),
                    emailDTO.getSendTemplate() && StringUtils.isEmpty(emailDTO.getMessage())
                            ? emailTemplate.getMessage()
                            : emailDTO.getMessage()
            );
            mailDAO.updateDateMessage(email, emailDTO.getSendTemplate());
        } catch (MessagingException e) {
            LOGGER.error("Can't send message to {}", email, e);
        }
    }

}
