package com.sites.equipmentshop.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sites.equipmentshop.endpoints.dto.EmailDTO;
import com.sites.equipmentshop.endpoints.dto.EmailsFilterDTO;
import com.sites.equipmentshop.endpoints.dto.FilterDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.service.EmailService;
import com.sites.equipmentshop.service.enums.FilterType;
import com.sites.equipmentshop.utils.APIConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/mail", consumes = {"*/*", "*", "application/json"}, produces = {"*/*", "application/json"})
public class MailController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailController.class);
    private final EmailService emailService;

    @Autowired
    public MailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping(value = "mails")
    public ResponseEntity<Object> getMails(@RequestBody EmailsFilterDTO dto) {
        return ResponseEntityUtils.responseEntity(emailService.getMails(dto));
    }

    @PostMapping(value = "")
    public void addEmail(@RequestBody List<String> emails) {
        emailService.addMails(emails);
    }

    @GetMapping(value = "unfollow")
    public void unfollow(@RequestParam(value = "email") String email) {
        emailService.unfollowMailing(email);
    }

    @PostMapping(value = "stopTask")
    public ResponseEntity<Object> stopMailTask() {
        return ResponseEntityUtils.responseEntity(String.valueOf(emailService.stopTask()));
    }

    @PostMapping(value = "hasTask")
    public ResponseEntity<Object> hasTask() {
        return ResponseEntityUtils.responseEntity(String.valueOf(emailService.hasActiveTask()));
    }

    @GetMapping(value = "get-template")
    public ResponseEntity<Object> hasTask(@RequestParam(value = "id", defaultValue = "1", required = false)  Integer id) {
        try {
            return ResponseEntityUtils.responseEntity(new ObjectMapper().writeValueAsString(emailService.getTemplateById(id)));
        } catch (JsonProcessingException e) {
            return ResponseEntityUtils.responseEntity("{}");
        }
    }

    @PostMapping(value = "send-template")
    public void sendTemplate(@RequestBody EmailDTO emailDTO) {
        emailService.sendTemplateToRecipients(emailDTO);
    }

}
