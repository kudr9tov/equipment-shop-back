package com.sites.equipmentshop.service.scrappers.models.association;

public class SiteScrappingAssociation {
    private Integer siteCategoryId;
    private Integer currentCategoryId;
    private Boolean exclude;
    private Action action;

    public Integer getSiteCategoryId() {
        return siteCategoryId;
    }

    public void setSiteCategoryId(Integer siteCategoryId) {
        this.siteCategoryId = siteCategoryId;
    }

    public Integer getCurrentCategoryId() {
        return currentCategoryId;
    }

    public void setCurrentCategoryId(Integer currentCategoryId) {
        this.currentCategoryId = currentCategoryId;
    }

    public Boolean isExclude() {
        return exclude;
    }

    public Boolean getExclude() {
        return exclude;
    }

    public void setExclude(Boolean exclude) {
        this.exclude = exclude;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
