package com.sites.equipmentshop.endpoints.dto;


import com.sites.equipmentshop.service.scrappers.models.association.SiteScrappingAssociation;

import java.util.List;

public class SiteScrappingAssociationDTO {
    private Integer siteId;
    private List<SiteScrappingAssociation> associations;

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public List<SiteScrappingAssociation> getAssociations() {
        return associations;
    }

    public void setAssociations(List<SiteScrappingAssociation> associations) {
        this.associations = associations;
    }
}


