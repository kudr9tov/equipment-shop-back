FROM openjdk:8 

WORKDIR /opt/equipment-shop

VOLUME /opt/equipment-shop/apps

ADD ./target/equipment-shop-0.0.1-SNAPSHOT.jar /opt/equipment-shop/equipment-shop.jar

EXPOSE 8081 8000
CMD ["java", "-Xdebug", "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8000", "-jar", "equipment-shop.jar"]
