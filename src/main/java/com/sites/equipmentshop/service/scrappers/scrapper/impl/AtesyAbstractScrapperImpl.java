package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.NumberUtils;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

public class AtesyAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(AtesyAbstractScrapperImpl.class);

    public AtesyAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                     FTPUtils ftpUtils,
                                     ScrapperDAO scrapperDAO,
                                     ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + StringUtils.defaultString(url));
        if (Objects.nonNull(document)) {

            final Element menu = document.getElementById("vertical-multilevel-menu");
            if (Objects.nonNull(menu)) {
                Map<String, String> categoryMap = Maps.newHashMap();
                final Elements categories = menu.getElementsByTag("a");
                for (Element categoryElement : categories) {
                    final String link = categoryElement.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    final String name = categoryElement.text();
                    final String parentCategory = getParentCategory(categoryElement);

                    if ("Линия раздачи питания РЕГАТА".equalsIgnoreCase(parentCategory) || "ЛИНИЯ РАЗДАЧИ ПИТАНИЯ РИВЬЕРА".equalsIgnoreCase(parentCategory)) {
                        categoryMap.put(link, parentCategory.toUpperCase() + " " + name.toUpperCase());
                    } else if (name.toUpperCase().equalsIgnoreCase("КАРКАС)")) {
                        categoryMap.put(link, name);
                    } else {
                        categoryMap.put(link, name.toUpperCase());
                    }
                }
                return categoryMap;
            }
        }
        return Collections.emptyMap();
    }

    @Override
    public Set<String> searchProductPages(String url) {
        final Set<String> pages = Sets.newHashSet();
        final Document document = ParserUtils.loadDocument(url);
        if (Objects.nonNull(document)) {
            final Element pagination = document.getElementsByClass("pagination").first();
            if (Objects.nonNull(pagination)) {
                final Element lastPage = pagination.child(0).children().last();
                for (int i = NumberUtils.parseNumber(lastPage.text(), Integer.class); i > 1; i--) {
                    pages.add(url + "?PAGEN_1=" + i);
                }
            }
        }
        pages.add(url);
        return pages;
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        try {
            final Document document = ParserUtils.loadDocument(url);
            if (Objects.nonNull(document)) {
                final Elements subCategoriesClass = document.getElementsByClass("categories-list row ix-all-products").first().children();
                final Map<String, String> categories = Maps.newHashMap();

                for (Element category : subCategoriesClass) {
                    final Element link = category.getElementsByTag("a").first();
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    final String name = category.getElementsByClass("categories-item__name").first().text();
                    categories.put(name, href);
                }
                return categories;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {
                final Map<String, Object> properties = Maps.newHashMap();
                properties.put("country", 240);
                properties.put("brand", 338);

                final Product product = new Product();
                product.setSiteUrl(getSite(), url);
                product.setProperties(properties);

                final Element item = document.getElementsByClass("product-box d-lg-flex").first();
                final Element tech = document.getElementsByClass("oglavl").first();
                final Element price = item.getElementsByClass("product-box__price").first();

                try {
                    final String replace = price.text()
                            .replace("₽", StringUtils.EMPTY)
                            .replace("Купить в 1 клик", StringUtils.EMPTY)
                            .replace(" ", StringUtils.EMPTY);
                    product.setPrice(NumberUtils.parseNumber(replace, Long.class));
                } catch (Exception e) {
                    LOGGER.error("Can't parse price. Type:{} message:{}", e.getClass().getSimpleName(), e.getMessage());
                }

                Map<String, String> techMap = Maps.newHashMap();
                try {
                    for (Element row : tech.children()) {
                        try {
                            String key = row.child(0).text();
                            String value = row.child(1).text();
                            techMap.put(key, value);
                        } catch (Exception e) {
                            LOGGER.error("", e);
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Can't parse tech properties. Type:{} message:{}", e.getClass().getSimpleName(), e.getMessage());
                }

                final Element first = item.getElementsByClass("product-box__name").first();
                try {
                    first.child(0).remove();
                    product.setName(first.text());
                } catch (Exception e) {
                    product.setName(first.text());
                }
                product.setTechProperties(techMap);

                try {
                    final Element image = item.getElementsByAttributeValue("data-fancybox", "gallery").first().getElementsByTag("a").first();
                    final String imageUrl = image.getElementsByTag("img").first().attr("src")
                            .replace(".pngg", ".png");
                    product.setImage(imageUrl);
                } catch (Exception e) {
                    LOGGER.error("Can't parse image. Type:{} message:{}", e.getClass().getSimpleName(), e.getMessage());
                }

                try {
                    final Element description = document.getElementsByClass("points").first();
                    final String replace = description.outerHtml()
                            .replaceAll("style=\\\"[%: \\-#A-z0-9;]*\\\"", StringUtils.EMPTY)
                            .replaceAll("class=\\\"[%: \\-#A-z0-9;]*\\\"", StringUtils.EMPTY)
                            .replaceAll("role=\\\"[%: \\-#A-z0-9;]*\\\"", StringUtils.EMPTY);
                    product.setDescription(replace);
                } catch (Exception e) {
                    LOGGER.error("Can't parse description. Type:{} message:{}", e.getClass().getSimpleName(), e.getMessage());
                }

                consumer.accept(product);
            }
        } catch (Exception e) {
            LOGGER.error("Can't parse product {}. Type:{} message:{}", getSite().getDomain() + url, e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    protected Site getSite() {
        return Site.ATESY;
    }

    private static String getParentCategory(Element category) {
        final Element parent = category.parent();
        if (parent.tagName().equalsIgnoreCase("div")) {
            return category.getElementsByTag("a").first().text();
        }
        return getParentCategory(parent);
    }
}
