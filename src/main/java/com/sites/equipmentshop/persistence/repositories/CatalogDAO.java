package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.CatalogDTO;
import com.sites.equipmentshop.endpoints.dto.MultiCatalogDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class CatalogDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public CatalogDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }


    public String getCatalogTree() {
        return nameJdbcTemplate.queryForObject("SELECT coalesce(jsonb_agg(brand), '[]') " +
                "FROM ( " +
                "         SELECT jsonb_build_object( " +
                "                        'brand', d.name, " +
                "                        'brandId', d.id, " +
                "                        'catalogs', jsonb_agg(jsonb_build_object( " +
                "                         'id', c.id, " +
                "                         'brand', d.name, " +
                "                         'brandId', d.id, " +
                "                         'name', c.name, " +
                "                         'catalogFileUrl', c.file_url " +
                "                     )) " +
                "                    ) brand " +
                "         FROM catalog c " +
                "                  INNER JOIN directory d on c.brand_id = d.id AND d.type = 'BRAND' " +
                "         GROUP BY d.id, d.name " +
                "     ) as data", Collections.emptyMap(), String.class);
    }

    public String getCatalog(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.queryForObject("SELECT jsonb_build_object( " +
                "            'id', c.id, " +
                "            'brand', d.name, " +
                "            'brandId', d.id, " +
                "            'name', c.name, " +
                "            'catalogFileUrl', c.file_url " +
                "           ) " +
                "FROM catalog c " +
                "INNER JOIN directory d on c.brand_id = d.id AND d.type = 'BRAND' " +
                "WHERE c.id = :id", params, String.class);
    }

    public void deleteCatalogById(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        nameJdbcTemplate.update("DELETE FROM catalog WHERE id = :id", params);
    }

    public String updateCatalogById(MultiCatalogDTO catalogDTO) {
        final String data = catalogDTO.getCatalogs()
                .stream()
                .map(f -> "(" + f.getId() + ", '" + f.getName() + "', " + catalogDTO.getBrandId() + " , '" + f.getCatalogFileUrl() + "')")
                .collect(Collectors.joining(","));

        final String query = "WITH data as ( " +
                "        UPDATE catalog  " +
                "         SET " +
                "             name = data.name, " +
                "             brand_id = data.brand_id, " +
                "             file_url = data.file_url " +
                "        FROM ( " +
                "                SELECT c.id, coalesce(f.name, c.name) as name, coalesce(f.brand_id, c.brand_id) as brand_id, coalesce(f.file_url, c.file_url) as file_url " +
                "                FROM catalog c" +
                "                  INNER JOIN (VALUES %s) f(id, name, brand_id, file_url) ON c.id = f.id" +
                "            ) as data " +
                "        WHERE catalog.id = data.id" +
                "        RETURNING  catalog.id,catalog.name,catalog.brand_id,catalog.file_url " +
                ") " +
                "SELECT jsonb_agg(jsonb_build_object( " +
                "               'id', c.id, " +
                "               'brand', d.name, " +
                "               'brandId', d.id, " +
                "               'name', c.name, " +
                "               'catalogFileUrl', c.file_url " +
                "           )) " +
                "FROM data c " +
                "         INNER JOIN directory d on c.brand_id = d.id AND d.type = 'BRAND'";
        return nameJdbcTemplate.queryForObject(String.format(query, data), Collections.emptyMap(), String.class);
    }

    public String save(MultiCatalogDTO catalogDTO) {
        final String data = catalogDTO.getCatalogs().stream().map(f -> "('" + f.getName() + "', " + catalogDTO.getBrandId() + " , '" + f.getCatalogFileUrl() + "')").collect(Collectors.joining(","));

        final String query = "WITH data as ( " +
                "    INSERT INTO catalog (name, brand_id, file_url) " +
                "        SELECT * " +
                "        FROM (VALUES %s) f(name, brandId, fileUrl)" +
                "        RETURNING  catalog.id,catalog.name,catalog.brand_id,catalog.file_url " +
                ") " +
                "SELECT jsonb_agg(jsonb_build_object( " +
                "               'id', c.id, " +
                "               'brand', d.name, " +
                "               'brandId', d.id, " +
                "               'name', c.name, " +
                "               'catalogFileUrl', c.file_url " +
                "           )) " +
                "FROM  data c " +
                "         INNER JOIN directory d on c.brand_id = d.id AND d.type = 'BRAND'";

        return nameJdbcTemplate.queryForObject(String.format(query, data), Collections.emptyMap(), String.class);
    }

    public String getCatalogByBrand(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.queryForObject("SELECT jsonb_agg(jsonb_build_object( " +
                "            'id', c.id, " +
                "            'brand', d.name, " +
                "            'brandId', d.id, " +
                "            'name', c.name, " +
                "            'catalogFileUrl', c.file_url " +
                "           )) " +
                "FROM catalog c " +
                "INNER JOIN directory d on c.brand_id = d.id AND d.type = 'BRAND' " +
                "WHERE c.brand_id = :id", params, String.class);
    }
}
