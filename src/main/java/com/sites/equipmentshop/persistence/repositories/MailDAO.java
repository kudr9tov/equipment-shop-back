package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.sites.equipmentshop.persistence.entities.EmailTemplate;
import com.sites.equipmentshop.persistence.entities.OrganizationContact;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class MailDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public MailDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public EmailTemplate getMailTemplateById(Integer id){
        return nameJdbcTemplate.queryForObject("SELECT * FROM email_template WHERE id = :id", ImmutableMap.of("id", id), (resultSet, i) -> {
            final EmailTemplate emailTemplate = new EmailTemplate();
            emailTemplate.setId(resultSet.getInt("id"));
            emailTemplate.setSubject(resultSet.getString("subject"));
            emailTemplate.setTemplate(resultSet.getString("template"));
            emailTemplate.setMessage(resultSet.getString("message"));
            emailTemplate.setPerson(resultSet.getString("person"));
            return emailTemplate;
        });
    }

    public Integer getCountMails(){
        return nameJdbcTemplate.queryForObject("SELECT count(id) FROM emails", ImmutableMap.of(), Integer.class);
    }

    public String getMails(String... filters) {
        String query = "SELECT json_agg(jsonb_build_object( " +
                "                          'id', id, " +
                "                          'organization', organization, " +
                "                          'director', director, " +
                "                          'phone', phone, " +
                "                          'email', email, " +
                "                          'site', site, " +
                "                          'address', address, " +
                "                          'requisites', cast(requisites as jsonb), " +
                "                          'isSend', isSend, " +
                "                          'isSendTemplate', is_send_template, " +
                "                          'updated', CAST(EXTRACT(EPOCH FROM updated) * 1000 AS BIGINT), " +
                "                          'lastSendMessage', CAST(EXTRACT(EPOCH FROM lastsendmessage) * 1000 AS BIGINT))) " +
                "FROM (SELECT * FROM emails QUERY ORDER) tmep";
        if(ArrayUtils.isNotEmpty(filters) && filters.length > 1 && StringUtils.isNoneBlank(filters[1])){
            query = query.replace("QUERY", " WHERE " + filters[1]);
        } else {
            query = query.replace("QUERY", "");
        }
        if(ArrayUtils.isNotEmpty(filters) && StringUtils.isNoneBlank(filters[0])){
            query = query.replace("ORDER", " ORDER BY " + filters[0]);
        } else {
            query = query.replace("ORDER", "");
        }
        return nameJdbcTemplate.queryForObject(query, Collections.emptyMap(), String.class);
    }

    public void saveContact(OrganizationContact organizationContact) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("organization", organizationContact.getOrganization());
        params.put("director", organizationContact.getDirector());
        params.put("phone", organizationContact.getPhone());
        params.put("site", organizationContact.getSite());
        params.put("address", organizationContact.getAddress());
        params.put("email", organizationContact.getEmail());
        params.put("requisites", organizationContact.getRequisites());
        nameJdbcTemplate.update("INSERT INTO emails (organization, director, phone, email, site, address, requisites) " +
                "VALUES (:organization, :director, :phone, :email, :site, :address, :requisites) ON CONFLICT DO NOTHING", params);
    }

    public void updateDateMessage(String email, Boolean sentTemplate) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("email", email);
        if (sentTemplate) {
            nameJdbcTemplate.update("UPDATE emails SET lastsendmessage = current_timestamp, is_send_template = true WHERE email = :email", params);

        } else {
            nameJdbcTemplate.update("UPDATE emails SET lastsendmessage = current_timestamp WHERE email = :email", params);

        }
    }

    public void unfollowMailing(String email) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("email", email);
        nameJdbcTemplate.update("UPDATE emails SET issend = FALSE WHERE email = :email", params);
    }

    public List<String> getMailsByIds(List<Integer> emailIds) {
        final String ids = StringUtils.join(emailIds, ",");
        return nameJdbcTemplate.queryForList("SELECT email FROM emails WHERE id = ANY (string_to_array(:ids, ',')) AND issend", ImmutableMap.of("ids", ids), String.class);
    }

    public List<String> getAllMails() {
        return nameJdbcTemplate.queryForList("SELECT email FROM emails WHERE issend and not is_send_template", Collections.emptyMap(), String.class);
    }

    public void addEmails(List<String> emails){
        final SqlParameterSource[] sqlParameterSources = emails
                .stream()
                .map(email -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("email", email);
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        nameJdbcTemplate.batchUpdate("INSERT INTO emails (email) VALUES (:email) ON CONFLICT DO NOTHING", sqlParameterSources);
    }
}
