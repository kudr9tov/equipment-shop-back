package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.DirectoryDTO;
import com.sites.equipmentshop.endpoints.dto.MultiDirectoryDTO;
import com.sites.equipmentshop.service.enums.Directory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class DirectoryDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public DirectoryDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public String getCategoriesConfig() {
        return nameJdbcTemplate.queryForObject("SELECT * " +
                "FROM get_categories()", Collections.emptyMap(), String.class);
    }

    public String getSimpleDirectoryConfig(Directory directory) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("type", directory.name());
        return nameJdbcTemplate.queryForObject("SELECT jsonb_agg(jsonb_build_object('id', id, 'name', name, 'image', image)) " +
                "FROM directory " +
                "WHERE type = :type;", params, String.class);
    }

    public String getAllConfigs() {
        return nameJdbcTemplate.queryForObject("SELECT jsonb_object_agg(config_name, config_value) " +
                "FROM ( " +
                "         SELECT 'category' as config_name, get_categories() as config_value " +
                "         UNION ALL " +
                "         SELECT 'brand', jsonb_agg(jsonb_build_object('id', id, 'name', name)) " +
                "         FROM directory " +
                "         WHERE type = 'BRAND' " +
                "         UNION ALL " +
                "         SELECT 'country', jsonb_agg(jsonb_build_object('id', id, 'name', name)) " +
                "         FROM directory " +
                "         WHERE type = 'COUNTRY' " +
                "     ) configs", Collections.emptyMap(), String.class);
    }

    public void addToDirectory(DirectoryDTO dto) {

        final Map<String, Object> params = Maps.newHashMap();
        params.put("value", dto.getValue());
        params.put("type", dto.getType().name());
        params.put("image", dto.getImage());
        nameJdbcTemplate.update("INSERT INTO directory (name, type, image) VALUES(:value, :type, :image)", params);
    }

    public void addValuesToDirectory(MultiDirectoryDTO dto) {

        final SqlParameterSource[] sqlParameterSources = dto.getEntries()
                .stream()
                .map(entry -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("name", entry.getName());
                    parameters.addValue("image", entry.getImage());
                    parameters.addValue("type", dto.getType().name());
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        nameJdbcTemplate.batchUpdate("INSERT INTO directory (name, image, type) VALUES (:name, :image, :type)", sqlParameterSources);
    }

    public String addCategoriesToDirectory(MultiDirectoryDTO dto) {
        String sql = "WITH category_temp as ( " +
                "    INSERT INTO directory (name, image, type) " +
                "        VALUES %s " +
                "        returning directory.id " +
                ")  " +
                "INSERT INTO category_matching (parent_category_id, category_id, level) " +
                "SELECT coalesce(:parentCategoryId, id), id, :level " +
                "FROM category_temp";
        final Map<String, Object> params = Maps.newHashMap();
        params.put("parentCategoryId", dto.getParentId());
        params.put("level", dto.getLevel());

        final String values = dto.getEntries()
                .stream()
                .map(entry -> "('" +
                        entry.getName() +
                        "','" +
                        entry.getImage() +
                        "','" +
                        dto.getType() +
                        "')")
                .collect(Collectors.joining(","));
        nameJdbcTemplate.update(String.format(sql, values), params);
        return nameJdbcTemplate.queryForObject("SELECT " +
                "                       jsonb_agg(jsonb_build_object('id', d.id, 'name', d.name, 'image', d.image, 'parentCategoryId', " +
                "                                                    cm.parent_category_id, 'level', cm.level)) category " +
                "                FROM category_matching cm " +
                "                         INNER JOIN directory d ON d.id = cm.category_id " +
                "                WHERE cm.parent_category_id = :parentCategoryId " +
                "                GROUP BY cm.parent_category_id, cm.level;", params, String.class);
    }

    public void addSubCategory(DirectoryDTO directoryDTO) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("value", directoryDTO.getValue());
        params.put("image", directoryDTO.getImage());
        params.put("type", directoryDTO.getType().name());
        params.put("parentCategoryId", directoryDTO.getParentId());
        params.put("level", directoryDTO.getLevel());
        nameJdbcTemplate.update("WITH category_temp as ( " +
                "    INSERT INTO directory (name, type, image) " +
                "        VALUES (:value, :type, :image) " +
                "        returning directory.id " +
                ") " +
                "INSERT INTO category_matching (parent_category_id, category_id, level) " +
                "SELECT coalesce(:parentCategoryId, id), id, :level " +
                "FROM category_temp", params);
    }

    public void updateDirectoryValue(DirectoryDTO dto) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("value", dto.getValue());
        params.put("image", dto.getImage());
        params.put("id", dto.getId());
        nameJdbcTemplate.update("UPDATE directory SET name = :value, image = :image WHERE id = :id", params);
    }

    public void removeDirectoryValue(Integer id, Directory directory) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        nameJdbcTemplate.update("DELETE FROM directory WHERE id = :id", params);
    }

    public void removeSubCategory(DirectoryDTO directoryDTO) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("parenCategoryId", directoryDTO.getParentId());
        params.put("categoryId", directoryDTO.getId());
        params.put("level", directoryDTO.getLevel());
        nameJdbcTemplate.update("WITH category_match_temp as ( " +
                "    DELETE FROM category_matching " +
                "        WHERE parent_category_id = :parenCategoryId AND category_id = :categoryId AND level = :level" +
                "        RETURNING category_matching.category_id " +
                ") " +
                "DELETE FROM directory " +
                "WHERE id = (SELECT category_id FROM category_match_temp); ", params);
    }

    public void updateValuesToDirectory(MultiDirectoryDTO dto) {
        final SqlParameterSource[] sqlParameterSources = dto.getEntries()
                .stream()
                .map(entry -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("id", entry.getId());
                    parameters.addValue("name", entry.getName());
                    parameters.addValue("image", entry.getImage());
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        nameJdbcTemplate.batchUpdate("UPDATE directory SET name = :name, image = :image WHERE id = :id", sqlParameterSources);
    }

    public void moveCategory(Integer parentId, Integer categoryId) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("categoryId", categoryId);
        params.put("parentCategoryId", parentId);
        nameJdbcTemplate.queryForObject("SELECT move_category(:categoryId, :parentCategoryId);", params, Boolean.class);
    }
}
