package com.sites.equipmentshop.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sites.equipmentshop.endpoints.dto.OrderDTO;
import com.sites.equipmentshop.endpoints.dto.ProductOrderDTO;
import com.sites.equipmentshop.exception.OrderNotFoundException;
import com.sites.equipmentshop.persistence.entities.Client;
import com.sites.equipmentshop.persistence.entities.OrderStatus;
import com.sites.equipmentshop.persistence.entities.Product;
import com.sites.equipmentshop.persistence.repositories.OrderDAO;
import com.sites.equipmentshop.persistence.repositories.ProductDAO;
import com.sites.equipmentshop.security.services.MailTemplateFactory;
import com.sites.equipmentshop.utils.HTMLUtils;
import com.sites.equipmentshop.utils.shared.SendMailService;
import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PriceService.class);
    private final OrderDAO orderDAO;
    private final SendMailService sendMailService;
    private final MailTemplateFactory templateFactory;
    private final ProductDAO productDAO;

    @Autowired
    public OrderService(final NamedParameterJdbcTemplate jdbcTemplate,
                        final SendMailService sendMailService,
                        final MailTemplateFactory templateFactory,
                        final ProductDAO productDAO,
                        final OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
        this.productDAO = productDAO;
        this.sendMailService = sendMailService;
        this.templateFactory = templateFactory;
    }

    public String getAllOrders(Integer limit,
                               Integer offset,
                               String filter) {
        try {
            return orderDAO.getOrders(limit, offset, filter);
        } catch (Exception e) {
            throw new IllegalStateException("Can't get products", e);
        }
    }

    public String getOrderById(Integer orderId) {
        return orderDAO.findById(orderId).orElseThrow(OrderNotFoundException::new);
    }

    public Object createOrder(OrderDTO orderDTO) {
        Object save = null;
        try {
            save = orderDAO.save(new ObjectMapper().writeValueAsString(orderDTO));
            final List<Product> products = productDAO.findByIds(orderDTO.getProducts().stream().map(ProductOrderDTO::getProductId).collect(Collectors.toList()))
                    .stream()
                    .map(f -> {
                        try {
                            return new ObjectMapper().readValue(f, Product.class);
                        } catch (JsonProcessingException e) {
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            String productsHTML = HTMLUtils.convertProductToHtml(products, orderDTO.getProducts());
            final Product product = new Product();
            product.setPrice(orderDTO.getTotalCost());
            productsHTML += HTMLUtils.convertProductToHtml(product, "Итого");
            sendMailService.sendMessageMoreThanOne("Новый заказ",
                    templateFactory.prepareNewNotificationTemplate("Новый заказ",
                            productsHTML,
                            HTMLUtils.convertClientToHtml(new Client(orderDTO.getCustomerInfo()), orderDTO.getComment()),
                            HTMLUtils.getProductsHeader(true),
                            Boolean.TRUE),
                    "kudr9tov@gmail.com", "pashagozasinett@gmail.com", "info@gktorg.ru");

            sendMailService.sendMessageMoreThanOne("Ваш заказ",
                    templateFactory.prepareNewNotificationTemplate("Ваш заказ",
                            productsHTML,
                            HTMLUtils.convertClientToHtml(new Client(orderDTO.getCustomerInfo()), orderDTO.getComment()),
                            HTMLUtils.getProductsHeader(true),
                            Boolean.FALSE),
                    orderDTO.getCustomerInfo().getEmail());

        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Can't save order", e);
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return save;
    }

    public void updateOrderStatus(Integer id, String status) {
        final OrderStatus orderStatus = EnumUtils.getEnumIgnoreCase(OrderStatus.class, status);
        if (Objects.isNull(orderStatus)) {
            throw new IllegalStateException("Incorrect status value");
        }
        orderDAO.changeStatus(id, orderStatus);
    }

    public void deleteOrderById(Integer id) {
        orderDAO.changeStatus(id, OrderStatus.DELETED);
    }
}
