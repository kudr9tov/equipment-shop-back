package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.NumberUtils;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class PolusAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(PolusAbstractScrapperImpl.class);

    private static Map<String, Map<String, Integer>> assosiation = Maps.newHashMap();

    static {
        assosiation.put("Настольные витриныВитрины A57 (ARGO XL)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Настольные витриныВитрины A87 (Argo)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Настольные витриныВитрины A37 (Sushi Bar)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Настольные витриныВитрины А37 (BAR)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Настольные витриныВитрины АC37 (Cube BAR)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Настольные витриныВитрины AC37 (Cube Sushi Bar)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Настольные витриныВитрины A89 (Argus)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Настольные витриныВитрины АС87 (Argo2)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Холодильные шкафыCarboma PRO со средним уровнем контроля влажности", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыCarboma PRO с контролем уровня влажности для хлебопекарных производств", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыCarboma PRO с высоким уровнем контроля влажности", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыДля напитков", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыС металлическими дверьми", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыСо стеклянными дверьми", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыCarboma D4 PRO с высоким уровнем контроля влажности", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Пристенные витриныНейтральные пристенные витрины CARBOMA", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины F13-07 (Britany)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины FC14-06, FC16-06, FC18-06 (VIVARA)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины F20-08 (Crete)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины FC20-08 (Cube)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины FC20-07 (Cube)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины F16-08 (Tokyo)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины FC20-08 X7 (Cube Flesh) для демонстрации мяса", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины F20-07 (Provance)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Пристенные витриныВитрины FC20-07 X7 (Cube Flesh) для демонстрации мяса", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрины FC14-06, FC16-06, FC18-06 (VIVARA)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрина PI11 (Fudzi)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрина PF11-13 (Fudzi)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрина PG07 (ODA)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрина PG11 (Fudzi)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрина PF07-12 (ODA)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрина PI07 (ODA)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрина PА90 (Tongo)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Промо витриныВитрина PF11-12 (Fudzi)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Гастрономические витриныВИТРИНЫ GC95 (Palm 2)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Гастрономические витриныВИТРИНЫ GC110 (Bavaria 2)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Гастрономические витриныВитрины GC120 (Atrium 2)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Гастрономические витриныНеохлаждаемые прилавки", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Гастрономические витриныВитрины G95 (Palm)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Гастрономические витриныВитрины G110 (Bavaria)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Гастрономические витриныВитрины G120 (Atrium)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Гастрономические витриныВитрины G85 (Praia)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины K70 (Flandria)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныКондитерские D4 (Latium)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины КС82 (VISION)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины К85 (Scania)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины КС84 (COSMO)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины KC70 (Cube)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины КС71BUILT-IN (COSMO)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины КС78 (COSMO)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины KC80 (Borneo)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины К95 (Plum)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины КС95 (Casablanca)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Кондитерские витриныВитрины КС71 (COSMO)", ImmutableMap.of("category", 1240, "subCategory", 1587));
        assosiation.put("Холодильные столыРасстоечные базы с контролем влажности для теста", ImmutableMap.of("category", 1238, "subCategory", 1634));


    }

    public PolusAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                     FTPUtils ftpUtils,
                                     ScrapperDAO scrapperDAO,
                                     ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }


    @Override
    public Map<String, String> searchCategories(String url, String category) {
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + StringUtils.defaultString(url));
        final Map<String, String> categories = Maps.newHashMap();
        if (Objects.nonNull(document)) {

            if (StringUtils.isNotBlank(category)) {
                final Elements items = document.getElementsByClass("c-showcase h-offset");
                final Map<String, String> tempCategories = items
                        .stream()
                        .flatMap(f -> f.children().stream())
                        .map(f -> {
                            final Element link = f.getElementsByTag("a").first();
                            final Element first = f.getElementsByClass("c-showcase__title c-title-sm").first();

                            return new AbstractMap.SimpleEntry<>(first.text(), link.attr("href"));
                        })
                        .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

                tempCategories.forEach((categoryName, categoryUrl) -> categories.putAll(searchCategories(categoryUrl, categoryName)));
                if (MapUtils.isEmpty(tempCategories)) {
                    return ImmutableMap.of(url, category);
                }

            } else {
                final Map<String, String> tempCategories = document.getElementById("group_tab_1")
                        .child(0)
                        .children()
                        .stream()
                        .map(f -> {
                            final Element link = f.getElementsByTag("a").first();
                            final Element first = f.getElementsByClass("c-tile__label").first();

                            return new AbstractMap.SimpleEntry<>(first.text(), link.attr("href"));
                        })
                        .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

                tempCategories.forEach((categoryName, categoryUrl) -> categories.putAll(searchCategories(categoryUrl, categoryName)));

                if (MapUtils.isEmpty(tempCategories)) {
                    return ImmutableMap.of(url, category);
                }
            }
        }
        return categories;
    }

    @Override
    public Set<String> searchProductPages(String url) {
        return Sets.newHashSet(url);
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        try {
            final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
            if (Objects.nonNull(document)) {
                final Element subCategoriesClass = document.getElementsByClass("items-list cl").first();
                final Map<String, String> categories = Maps.newHashMap();
                for (Element category : subCategoriesClass.children()) {
                    final Element link = category.getElementsByClass("name").first().getElementsByTag("a").first();
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    categories.put(link.text(), href);
                }
                return categories;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        if (Objects.nonNull(document)) {
            try {
                Map<String, String> techMap = Maps.newHashMap();
                final Element description = document.getElementById("tabs_1_1").child(0);

                final Element productsTable = document.getElementById("tabs_1_2").getElementsByTag("tbody").first();
                final List<String> headers = productsTable.child(0).children()
                        .stream()
                        .filter(f -> !f.text().equalsIgnoreCase("Модель"))
                        .filter(f -> !f.text().equalsIgnoreCase("Габаритные размеры, мм"))
                        .filter(f -> !f.text().equalsIgnoreCase("Цена, руб"))
                        .map(Element::text)
                        .collect(Collectors.toList());

                final int size = productsTable.children().size();

                for (int i = 2; i < size; i++) {
                    Element productLine = productsTable.child(i);
                    final Product product = new Product();
                    product.setSiteUrl(getSite(), url);
                    final Map<String, Object> properties = Maps.newHashMap();
                    properties.put("country", 240);
                    properties.put("brand", 342);
                    product.setProperties(properties);

                    try {
                        final String imageLink = productLine.child(0).child(1).attr("src");
                        product.setImage(getSite().getDomain() + imageLink);
                    } catch (Exception ignored) {
                        final String imageLink = document.getElementById("product_photo").attr("src");
                        product.setImage(getSite().getDomain() + imageLink);
                    }

                    final String productName = productLine.child(1).getElementsByTag("strong").text();

                    final int columns = productLine.children().size();
                    try {
                        final Long price = NumberUtils.parseNumber(productLine.child(columns - 1).text()
                                .replaceAll("[\\s,A-zА-я.]", StringUtils.EMPTY), Long.class);
                        product.setPrice(price);
                    } catch (Exception ignored) {

                    }

                    techMap.put("Длина, мм", productLine.child(2).text());
                    techMap.put("Ширина, мм", productLine.child(3).text());
                    techMap.put("Высота, мм", productLine.child(4).text());

                    for (int j = 5; j < columns - 1; j++) {
                        techMap.put(headers.get(j - 5), productLine.child(j).text());
                    }

                    product.setTechProperties(techMap);
                    product.setName(productName);
                    product.setDescription(description.outerHtml());
                    consumer.accept(product);
                }

            } catch (Exception e) {
                throw new IllegalStateException(getSite().getDomain() + url);
            }
        }
    }

    @Override
    protected Site getSite() {
        return Site.POLUS;
    }
}
