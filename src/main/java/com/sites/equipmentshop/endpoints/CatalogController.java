package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.CatalogDTO;
import com.sites.equipmentshop.endpoints.dto.DirectoryDTO;
import com.sites.equipmentshop.endpoints.dto.MultiCatalogDTO;
import com.sites.equipmentshop.endpoints.dto.MultiDirectoryDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.service.CatalogService;
import com.sites.equipmentshop.service.DirectoryService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/catalog", consumes = {"*/*", "*", "application/json"}, produces = {"*/*", "application/json"})
public class CatalogController {

    private CatalogService catalogService;

    @Autowired
    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getCatalogs() {
        return ResponseEntityUtils.responseEntity(catalogService.getCatalogTree());
    }

    @GetMapping(value = "/brand/{id}")
    public ResponseEntity<Object> getCatalogByBrand(@PathVariable Integer id) {
        return ResponseEntityUtils.responseEntity(catalogService.getCatalogByBrand(id));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getCatalog(@PathVariable Integer id) {
        return ResponseEntityUtils.responseEntity(catalogService.getCatalog(id));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteCatalog(@PathVariable Integer id) {
        catalogService.deleteCatalog(id);
    }

    @PutMapping(value = "")
    public ResponseEntity<Object> updateCatalog(@RequestBody MultiCatalogDTO catalogDTO) {
        return ResponseEntityUtils.responseEntity(catalogService.updateCatalog(catalogDTO));
    }

    @PostMapping(value = "")
    public ResponseEntity<Object> saveCatalog(@RequestBody MultiCatalogDTO catalogDTO) {
        return ResponseEntityUtils.responseEntity(catalogService.saveCatalog(catalogDTO));
    }

}
