package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.UpdatePriceDTO;
import com.sites.equipmentshop.service.price.FilePriceService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/prices", consumes = {"*/*", "*", "application/json"}, produces = {"*/*", "application/json"})
public class UpdatePriceController {

    private FilePriceService filePriceService;

    @Autowired
    public UpdatePriceController(FilePriceService filePriceService) {
        this.filePriceService = filePriceService;
    }

    @PostMapping(value = "/run")
    public void runScrapper(@RequestBody UpdatePriceDTO updatePriceDTO) {
//        filePriceService.updateBrandProductPrice(updatePriceDTO.getBrand(), updatePriceDTO.getFileUrl());
    }

}
