package com.sites.equipmentshop.service.scrappers.models;

public class ScheduleJobModel {
    private String scheduleId;
    private String jobType;
    private String scrapperName;

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getScrapperName() {
        return scrapperName;
    }

    public void setScrapperName(String scrapperName) {
        this.scrapperName = scrapperName;
    }
}
