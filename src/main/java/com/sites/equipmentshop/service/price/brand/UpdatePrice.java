package com.sites.equipmentshop.service.price.brand;

import com.poiji.bind.Poiji;
import com.poiji.option.PoijiOptions;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public interface UpdatePrice {
    AtomicInteger productsCount = new AtomicInteger();

    default <T> List<T> getProducts(File file, Class<T> clazz) {
        PoijiOptions options = PoijiOptions.PoijiOptionsBuilder.settings()
                .addListDelimiter(";")
                .build();
        return Poiji.fromExcel(file, clazz, options);
    }

    void update(File file);

}
