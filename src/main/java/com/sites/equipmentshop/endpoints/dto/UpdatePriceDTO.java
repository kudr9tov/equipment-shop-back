package com.sites.equipmentshop.endpoints.dto;

import com.sites.equipmentshop.service.price.Brand;
import org.apache.commons.lang3.EnumUtils;

public class UpdatePriceDTO {
    private Brand brand;
    private String fileUrl;

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public void setBrand(String brand) {
        this.brand = EnumUtils.getEnumIgnoreCase(Brand.class, brand);
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
