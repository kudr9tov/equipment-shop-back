package com.sites.equipmentshop.service.scrappers.scrapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.models.ScrapperResult;
import com.sites.equipmentshop.service.scrappers.models.ScrapperStatus;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jooq.lambda.Unchecked;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public abstract class AbstractScrapper {
    private final static Logger LOGGER = LoggerFactory.getLogger(AbstractScrapper.class);
    private final static String BASE_IMAGE_DOMAIN = "https://gktorg.ru/images/";
    protected ScrapperDTO scrapperDTO;
    protected AtomicReference<Integer> jobId = new AtomicReference<>();
    protected final NamedParameterJdbcTemplate productService;
    protected final FTPUtils ftpUtils;
    protected final ScrapperDAO scrapperDAO;
    protected ScrapperResult result;

    public AbstractScrapper(NamedParameterJdbcTemplate productService, FTPUtils ftpUtils, ScrapperDAO scrapperDAO, ScrapperDTO scrapperDTO) {
        this.productService = productService;
        this.ftpUtils = ftpUtils;
        this.scrapperDAO = scrapperDAO;
        this.scrapperDTO = scrapperDTO;
        this.result = new ScrapperResult();
    }

    protected void saveSiteCategories(Integer siteId, Map<String, String> categories) {
        result.getCategories().addAndGet(MapUtils.size(categories));
        scrapperDAO.saveSiteCategories(getSite(), siteId, categories);
    }

    protected void processingProduct(Integer categoryId,
                                     Product productInfo) {

        if (Objects.nonNull(productInfo)) {
            try {
                productService.update("INSERT INTO processed_remote_product (url, execution_id) VALUES (:url, :id) ON CONFLICT DO NOTHING ", ImmutableMap.of(
                        "url", productInfo.getSiteUrl(),
                        "id", jobId.get()
                ));
                if (scrapperDTO.isUpdateProductsPrice()) {
                    updatePrice(productInfo);
                }
                if (scrapperDTO.isUpdateProducts()) {
                    updateProduct(categoryId, productInfo);
                }

                LOGGER.info("Product {} processed. Count {}", productInfo.getName(), result.getFoundedProducts().incrementAndGet());
            } catch (Exception e) {
                LOGGER.error("", e);
            }
        }
    }

    private void updatePrice(Product productInfo) {
        try {
            final Integer productId = productService.queryForObject("SELECT DISTINCT id " +
                            "FROM product p  " +
                            "   INNER JOIN remote_product rp on p.id = rp.product_id  " +
                            "WHERE url = :productUrl  " +
                            " AND lower(name) = lower(:productName)",
                    ImmutableMap.of("productName", productInfo.getName(), "productUrl", productInfo.getSiteUrl()), Integer.class);

            if (Objects.isNull(productId) && Objects.isNull(productInfo.getPrice())) {
                return;
            }

            final Map<String, Object> params = Maps.newHashMap();
            params.put("id", productId);
            params.put("price", productInfo.getPrice());

            productService.update("UPDATE product SET price = :price, updated = current_timestamp WHERE id = :id", params);
            result.getPriceProductsUpdated().incrementAndGet();

            productService.queryForObject("SELECT store_product_changes(:executionId, :productId)", ImmutableMap.of(
                    "productId", productId,
                    "executionId", jobId.get()
            ), Boolean.class);

            productService.update("INSERT INTO remote_product VALUES (:id, :url) ON CONFLICT DO NOTHING ", ImmutableMap.of(
                    "url", productInfo.getSiteUrl(),
                    "id", productId
            ));
        } catch (Exception e) {
            LOGGER.error(String.format("Product `%s` not found. Reason %s", productInfo.getName(), e.getMessage()));
        }
    }

    private void updateProduct(Integer categoryId, Product productInfo) throws JsonProcessingException {
        Integer productId;
        try {
            productId = productService.queryForObject("SELECT DISTINCT id " +
                            "FROM product p  " +
                            "   INNER JOIN remote_product rp on p.id = rp.product_id  " +
                            "WHERE url = :productUrl  " +
                            " AND lower(name) = lower(:productName)",
                    ImmutableMap.of("productName", productInfo.getName(), "productUrl", productInfo.getSiteUrl()), Integer.class);
            productInfo.setId(productId);
        } catch (Exception e) {

        }
        final ObjectMapper objectMapper = new ObjectMapper();

        try { // if assosiation has latest category add category to product
            productService.queryForObject("SELECT true FROM category_matching WHERE parent_category_id = :categoryId", ImmutableMap.of("categoryId", categoryId), Boolean.class);
        } catch (Exception categoryException) {
            final Map<String, Object> properties = productInfo.getProperties();
            try {
                if (Objects.nonNull(categoryId)) {
                    properties.put("subCategory", categoryId);
                }
            } catch (Exception ignored) {

            }
        }

        try {
            if (StringUtils.isNoneBlank(productInfo.getImage())) {
                final String fileName = ftpUtils.copyImageToFTP(productInfo.getImage());
                if (StringUtils.isNotBlank(fileName)) {
                    productInfo.setImage(BASE_IMAGE_DOMAIN + fileName);
                }
            }
        } catch (Exception ignored) {
        }

        productId = productService.queryForObject("SELECT js->>'id' FROM processing_product(:params) js",
                ImmutableMap.of("params", objectMapper.writeValueAsString(productInfo)),
                Integer.class);
        result.getNewProducts().incrementAndGet();

        if (!productInfo.getProperties().containsKey("subCategory")) {
            productService.update("INSERT INTO product_errors VALUES (:id, 'Невозможно определить категорию', :url)", ImmutableMap.of(
                    "url", productInfo.getSiteUrl(),
                    "id", productId
            ));
        }

        productService.update("INSERT INTO remote_product VALUES (:id, :url) ON CONFLICT DO NOTHING ", ImmutableMap.of(
                "url", productInfo.getSiteUrl(),
                "id", productId
        ));
    }


    protected void processingProducts(Integer siteId, String categoryUrl) {
        searchProductPages(categoryUrl).forEach(page -> {
            final Map<String, String> products = searchProducts(page);
            if (MapUtils.isNotEmpty(products)) {
                final Integer categoryId = scrapperDAO.getAssociationBySiteIdAndUrl(siteId, categoryUrl);
                if (Objects.nonNull(categoryId)) {
                    products.values().forEach((productLink) ->
                            getProductInfo(productLink, product ->
                                    processingProduct(categoryId, product)));
                }
            }
        });
    }

    protected void processingPriceProducts() {
        final Set<String> products = scrapperDAO.getAssociationBySiteIdAndUrl(getSite());
        AtomicInteger processedProducts = new AtomicInteger();

        if (CollectionUtils.isNotEmpty(products)) {
            for (String productLink : products) {
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
                getProductInfo(productLink, Unchecked.consumer(product -> {
                    processingProduct(null, product);
                    LOGGER.info("Product {}:{} processed", processedProducts.incrementAndGet(), products.size());
                }));
            }
        }
    }

    protected abstract Map<String, String> searchCategories(String url, String category);

    protected abstract Set<String> searchProductPages(String url);

    protected abstract Map<String, String> searchProducts(String url);

    protected abstract void getProductInfo(String url, Consumer<Product> consumer);

    protected abstract Site getSite();

    protected Document getProductUrl(String url) {
        if (url.contains(getSite().getDomain())) {
            return ParserUtils.loadDocument(url);
        } else {
            return ParserUtils.loadDocument(getSite().getDomain() + url);
        }
    }

    public void startJob() {
        try {

            Thread.currentThread().setName(getSite().name());
            final Integer siteId = scrapperDAO.saveSite(getSite());
            jobId.set(scrapperDAO.createJob(siteId));
            LOGGER.info("Run {} scrapper", getSite());
            scrapperDAO.updateJob(jobId.get(), ScrapperStatus.RUNNING, null);
            if (scrapperDTO.isUpdateCategories()) {
                final Map<String, String> categories = searchCategories(getSite().getCatalog(), null);
                saveSiteCategories(siteId, categories);
            }
            if (scrapperDTO.isUpdateProducts()) {
                final Map<String, String> categories = scrapperDAO.getSiteCategories(siteId);
                AtomicInteger processedCategories = new AtomicInteger();
                categories
                        .keySet()
                        .forEach((categoryUrl) -> {
                            processingProducts(siteId, categoryUrl);
                            LOGGER.info("Processed {}:{} categories", processedCategories.incrementAndGet(), categories.size());
                        });
            }
            if (scrapperDTO.isUpdateProductsPrice()) {
                processingPriceProducts();
            }
            scrapperDAO.updateJob(jobId.get(), ScrapperStatus.COMPLETED, result);
            LOGGER.info("Job completed");
        } catch (Exception e) {
            result.setLastError(e);
            LOGGER.info("Job processed with error", e);
            scrapperDAO.updateJob(jobId.get(), ScrapperStatus.FAILED, result);
        }
    }

}
