package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.NumberUtils;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class PrometAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrometAbstractScrapperImpl.class);

    private static Map<String, Map<String, Integer>> assosiation = Maps.newHashMap();

    static {
        assosiation.put("Многоящичные шкафы", ImmutableMap.of("category", 1756, "subCategory", 2038));
        assosiation.put("Тумбы мобильные", ImmutableMap.of("category", 1756, "subCategory", 2039));
        assosiation.put("Темпокассы", ImmutableMap.of("category", 1756, "subCategory", 2036));
        assosiation.put("Депозитные ячейки", ImmutableMap.of("category", 1756, "subCategory", 2037));
        assosiation.put("Cпециализированная мебель", ImmutableMap.of("category", 1756, "subCategory", 2034));
        assosiation.put("Тумбы", ImmutableMap.of("category", 1756, "subCategory", 2035));
        assosiation.put("Аксессуары на экран", ImmutableMap.of("category", 1756, "subCategory", 2033));
        assosiation.put("Офисные столы", ImmutableMap.of("category", 1756, "subCategory", 2031));
        assosiation.put("Мебель для балконов", ImmutableMap.of("category", 1756, "subCategory", 2032));
        assosiation.put("Шкафы универсальные", ImmutableMap.of("category", 1756, "subCategory", 2028));
        assosiation.put("Индивидуальные шкафы кассира", ImmutableMap.of("category", 1756, "subCategory", 1874));
        assosiation.put("Тяжелые модульные шкафы серии HARD", ImmutableMap.of("category", 1756, "subCategory", 1828));
        assosiation.put("Огнестойкие шкафы", ImmutableMap.of("category", 1756, "subCategory", 2027));
        assosiation.put("Шкафы для раздевалок (локеры)", ImmutableMap.of("category", 1756, "subCategory", 1864));
        assosiation.put("Шкафы для раздевалок", ImmutableMap.of("category", 1756, "subCategory", 1864));
        assosiation.put("Скамейки, подставки, цоколи и опоры", ImmutableMap.of("category", 1756, "subCategory", 1816));
        assosiation.put("Cушильные шкафы", ImmutableMap.of("category", 1756, "subCategory", 1866));
        assosiation.put("Тележки инструментальные", ImmutableMap.of("category", 1756, "subCategory", 1822));
        assosiation.put("Офисные тумбы", ImmutableMap.of("category", 1756, "subCategory", 1943));
        assosiation.put("Ключницы", ImmutableMap.of("category", 1756, "subCategory", 1868));
        assosiation.put("Армейские шкафы", ImmutableMap.of("category", 1756, "subCategory", 1861));
        assosiation.put("Шкафы для офиса", ImmutableMap.of("category", 1756, "subCategory", 1939));
        assosiation.put("Шкафы инструментальные тяжелые AMH TC", ImmutableMap.of("category", 1756, "subCategory", 1869));
        assosiation.put("Кэшбоксы", ImmutableMap.of("category", 1756, "subCategory", 1844));
        assosiation.put("Гостиничные сейфы", ImmutableMap.of("category", 1756, "subCategory", 1847));
        assosiation.put("Дополнительные аксессуары для сейфов", ImmutableMap.of("category", 1756, "subCategory", 2025));
        assosiation.put("Сейфы Европейской сертификации", ImmutableMap.of("category", 1756, "subCategory", 2023));
        assosiation.put("Депозитные сейфы", ImmutableMap.of("category", 1756, "subCategory", 1848));
        assosiation.put("Эксклюзивные сейфы", ImmutableMap.of("category", 1756, "subCategory", 2024));
        assosiation.put("SMART-сейфы", ImmutableMap.of("category", 1756, "subCategory", 2026));
        assosiation.put("Встраиваемые сейфы", ImmutableMap.of("category", 1756, "subCategory", 1849));
        assosiation.put("Верстаки серии Profi W", ImmutableMap.of("category", 1756, "subCategory", 1838));
        assosiation.put("Верстаки серии MASTER", ImmutableMap.of("category", 1756, "subCategory", 1837));
        assosiation.put("Сейфы, сочетающие огнестойкость и устойчивость к взлому", ImmutableMap.of("category", 1756, "subCategory", 1850));
        assosiation.put("Взломостойкие сейфы V класса", ImmutableMap.of("category", 1756, "subCategory", 2022));
        assosiation.put("Взломостойкие сейфы II класса", ImmutableMap.of("category", 1756, "subCategory", 1856));
        assosiation.put("Взломостойкие сейфы III класса", ImmutableMap.of("category", 1756, "subCategory", 1855));
        assosiation.put("Офисные кресла", ImmutableMap.of("category", 1756, "subCategory", 1941));
        assosiation.put("Взломостойкие сейфы IV класса", ImmutableMap.of("category", 1756, "subCategory", 1858));
        assosiation.put("Автоматические камеры хранения", ImmutableMap.of("category", 1756, "subCategory", 1863));
        assosiation.put("Огнестойкие сейфы", ImmutableMap.of("category", 1756, "subCategory", 1845));
        assosiation.put("Мебельные и офисные сейфы", ImmutableMap.of("category", 1756, "subCategory", 1853));
        assosiation.put("Бухгалтерские шкафы", ImmutableMap.of("category", 1756, "subCategory", 1874));
        assosiation.put("Взломостойкие сейфы I класса", ImmutableMap.of("category", 1756, "subCategory", 1854));
        assosiation.put("Почтовые ящики", ImmutableMap.of("category", 1756, "subCategory", 1871));
        assosiation.put("Шкафы инструментальные легкие ТС", ImmutableMap.of("category", 1756, "subCategory", 1828));
        assosiation.put("Верстаки серии Garage", ImmutableMap.of("category", 1756, "subCategory", 1633));
        assosiation.put("Стулья промышленные", ImmutableMap.of("category", 1756, "subCategory", 1827));
        assosiation.put("Картотеки", ImmutableMap.of("category", 1756, "subCategory", 1862));
        assosiation.put("Огнестойкие картотеки", ImmutableMap.of("category", 1756, "subCategory", 1862));
        assosiation.put("Верстаки серии EXPERT WS", ImmutableMap.of("category", 1756, "subCategory", 1831));
        assosiation.put("Оружейные шкафы и сейфы", ImmutableMap.of("category", 1756, "subCategory", 1861));
        assosiation.put("Абонентские шкафы", ImmutableMap.of("category", 1756, "subCategory", 1867));
        assosiation.put("Картотеки больших форматов", ImmutableMap.of("category", 1756, "subCategory", 1862));

        assosiation.put("MS Hard (1000 кг на секцию)", ImmutableMap.of("category", 1756, "subCategory", 2043));
        assosiation.put("MS Standart (500 кг на секцию)", ImmutableMap.of("category", 1756, "subCategory", 2043));
        assosiation.put("ES легкие стеллажи (120 кг на секцию)", ImmutableMap.of("category", 1756, "subCategory", 2044));
        assosiation.put("MS Pro (до 4000 кг на секцию)", ImmutableMap.of("category", 1756, "subCategory", 2043));
        assosiation.put("MS Strong (750 кг на секцию)", ImmutableMap.of("category", 1756, "subCategory", 2043));

    }

    public PrometAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                      FTPUtils ftpUtils,
                                      ScrapperDAO scrapperDAO,
                                      ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        LOGGER.info("Search categories");
        if ((getSite().getDomain() + StringUtils.defaultString(url)).equals("https://www.safe.ru/catalog/meditsinskaya-mebel-i-oborudovanie/")) {
            return Maps.newHashMap();
        }
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + StringUtils.defaultString(url));
        final Map<String, String> categories = Maps.newHashMap();
        if (Objects.nonNull(document)) {

            try {
                final Elements elementsByClass = document.getElementsByClass("col-lg-4 col-sm-6 col-xs-12 category-card-wrap");
                if (elementsByClass.size() < 1) {
                    throw new IllegalStateException("new format");
                }
                final Map<String, String> tempCategories = elementsByClass
                        .stream()
                        .map(f -> f.getElementsByTag("a").first())
                        .collect(Collectors.toMap(Element::text, f -> f.attr("href")));
                tempCategories.forEach((categoryName, categoryUrl) -> categories.putAll(searchCategories(categoryUrl, categoryName)));

                if (MapUtils.isEmpty(tempCategories)) {
                    return ImmutableMap.of(url, category);
                }


            } catch (Exception e) {
                try {
                    final Map<String, String> tempCategories = document.getElementsByClass("catalog__card")
                            .stream()
                            .filter(f -> Objects.isNull(f.getElementsByClass("tile-card__price").first()))
                            .collect(Collectors.toMap(Element::text, f -> f.getElementsByTag("a").first().attr("href")));
                    tempCategories.forEach((categoryName, categoryUrl) -> categories.putAll(searchCategories(categoryUrl, categoryName)));
                    if (MapUtils.isEmpty(tempCategories)) {
                        return ImmutableMap.of(url, category);
                    }
                } catch (Exception ex) {
                    return categories;
                }
            }
        }
        categories.keySet().removeIf(f -> f.equalsIgnoreCase("Металлические двери"));
        return categories;
    }

    @Override
    public Set<String> searchProductPages(String url) {
        final Set<String> pages = Sets.newHashSet();
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
        if (Objects.nonNull(document)) {
            final Element pagination = document.getElementsByClass("pagination").first();
            if (Objects.nonNull(pagination)) {
                final Elements children = pagination.children();
                children.remove(0);
                children.remove(0);
                children.remove(children.size() - 1);
                for (Element page : children) {
                    pages.add(page.getElementsByTag("a").first().attr("href"));
                }
            }
        }
        pages.add(url);
        return pages;
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        LOGGER.info("Search products");
        try {
            final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
            if (Objects.nonNull(document)) {
                final Elements subCategoriesClass = document.getElementsByClass("catalog__card");
                final Map<String, String> categories = Maps.newHashMap();
                for (Element productElement : subCategoriesClass) {
                    final Element link = productElement.getElementsByTag("a").first();
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    categories.put(UUID.randomUUID().toString(), href);
                }
                return categories;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {
                String image = Optional.ofNullable(document.getElementsByClass("card-slider__main-slide").first())
                        .flatMap(t -> Optional.ofNullable(t.getElementsByTag("img").first()).map(f -> f.attr("src")))
                        .orElse(null);

                Map<String, String> techMap = Maps.newHashMap();
                try {
                    final Element table = document.getElementsByClass("card__characteristics table box-block").first();
                    final Elements specifications = table.getElementsByTag("tr");

                    for (Element entry : specifications) {
                        techMap.put(entry.child(0).text(), entry.child(1).text());
                    }
                    techMap.remove("Страна:");
                    techMap.remove("Производитель:");
                } catch (Exception ignored) {
                }

                final Map<String, Object> properties = Maps.newHashMap();
                properties.put("country", 240);
                properties.put("brand", 1958);

                final Product product = new Product();
                product.setProperties(properties);
                product.setTechProperties(techMap);
                try {
                    final Element description = document.getElementById("tab_6");
                    final String replace = description.outerHtml()
                            .replaceAll("style=\\\"[%: #A-z0-9;]*\\\"", StringUtils.EMPTY)
                            .replaceAll("class=\\\"[%: #A-z0-9;]*\\\"", StringUtils.EMPTY)
                            .replaceAll("role=\\\"[%: #A-z0-9;]*\\\"", StringUtils.EMPTY);
                    product.setDescription(replace);
                } catch (Exception ignored) {
                }


                product.setImage(getSite().getDomain() + image);
                product.setName(Optional.ofNullable(document.getElementsByTag("h1").first()).map(Element::text).orElse(null));
                try {
                    product.setPrice(NumberUtils.parseNumber(document.getElementsByAttributeValue("itemprop", "price").first().attr("content"), Long.class));
                } catch (Exception ignored) {

                }
                product.setSiteUrl(getSite(), url);
                consumer.accept(product);
            }
        } catch (Exception e) {
            LOGGER.error("Can't parse product", e);
        }
    }

    @Override
    protected Site getSite() {
        return Site.PROMET;
    }

}
