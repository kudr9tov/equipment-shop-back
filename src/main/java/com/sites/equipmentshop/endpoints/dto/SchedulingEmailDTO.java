package com.sites.equipmentshop.endpoints.dto;

import javax.validation.constraints.NotNull;

public class SchedulingEmailDTO {
    @NotNull
    private EmailDTO dto;
    @NotNull
    private String cron;

    public EmailDTO getDto() {
        return dto;
    }

    public void setDto(EmailDTO dto) {
        this.dto = dto;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }
}
