package com.sites.equipmentshop.service;

import com.sites.equipmentshop.endpoints.dto.CatalogDTO;
import com.sites.equipmentshop.endpoints.dto.MultiCatalogDTO;
import com.sites.equipmentshop.persistence.repositories.CatalogDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class CatalogService {
    private final CatalogDAO catalogDAO;

    @Autowired
    public CatalogService(final JdbcTemplate jdbcTemplate,
                          final CatalogDAO catalogDAO) {
        this.catalogDAO = catalogDAO;
    }


    public String getCatalogTree() {
        return catalogDAO.getCatalogTree();
    }

    public String getCatalog(Integer id) {
        return catalogDAO.getCatalog(id);
    }

    public void deleteCatalog(Integer id) {
        catalogDAO.deleteCatalogById(id);
    }

    public String updateCatalog(MultiCatalogDTO catalogDTO) {
        return catalogDAO.updateCatalogById(catalogDTO);
    }

    public String saveCatalog(MultiCatalogDTO catalogDTO) {
        return catalogDAO.save(catalogDTO);
    }

    public String getCatalogByBrand(Integer id) {
        return catalogDAO.getCatalogByBrand(id);
    }
}
