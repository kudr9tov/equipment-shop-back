package com.sites.equipmentshop.endpoints.dto;

import com.sites.equipmentshop.persistence.entities.OrderStatus;

import java.util.List;

public class OrderDTO {
    private Integer id;
    private List<ProductOrderDTO> products;
    private OrderCustomerDTO customerInfo;
    private String comment;
    private Long totalCost;
    private OrderStatus status = OrderStatus.CREATED; //Default value

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ProductOrderDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductOrderDTO> products) {
        this.products = products;
    }

    public OrderCustomerDTO getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(OrderCustomerDTO customerInfo) {
        this.customerInfo = customerInfo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Long totalCost) {
        this.totalCost = totalCost;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }
}
