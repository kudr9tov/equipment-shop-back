package com.sites.equipmentshop.service.price.brand.model;

import com.poiji.annotation.ExcelRow;

public class PriceProduct {
    @ExcelRow
    private int rowIndex;
    private String name;
    private String article;
    private String description;
    private Integer priceNds;

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPriceNds() {
        return priceNds;
    }

    public void setPriceNds(Integer priceNds) {
        this.priceNds = priceNds;
    }
}
