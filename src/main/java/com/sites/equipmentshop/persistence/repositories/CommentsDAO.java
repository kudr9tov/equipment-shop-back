package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.CommentDTO;
import com.sites.equipmentshop.endpoints.dto.CommentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class CommentsDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public CommentsDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public String getEntityComments(CommentType commentType, Integer entityId) {
        return nameJdbcTemplate.queryForObject("SELECT get_comments(:entityId, :type)", ImmutableMap.of("entityId", entityId, "type", commentType.name()), String.class);
    }

    public String getComment(Integer id) {
        return nameJdbcTemplate.queryForObject("SELECT jsonb_build_object('id', id,'entityId', entity_linked_id,'parentId', comment_parent_id,'author', author,'comment', comment,'created', CAST(EXTRACT(EPOCH FROM created) * 1000 AS BIGINT)) FROM comments WHERE id = :id", ImmutableMap.of("id", id), String.class);
    }

    public void deleteComment(Integer id) {
        nameJdbcTemplate.update("DELETE FROM comments WHERE id = :id", ImmutableMap.of("id", id));
    }

    public String updateComment(CommentDTO dto) {
        return nameJdbcTemplate.queryForObject("UPDATE comments SET comment = :comment WHERE id = :id RETURNING jsonb_build_object('id', id,'entityId', entity_linked_id,'parentId', comment_parent_id,'author', author,'comment', comment,'created', CAST(EXTRACT(EPOCH FROM created) * 1000 AS BIGINT));", ImmutableMap.of("id", dto.getId(), "comment", dto.getComment()), String.class);
    }

    public String createComment(CommentDTO dto) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("entity_linked_id", dto.getEntityLinkedId());
        params.put("comment_parent_id", dto.getCommentParentId());
        params.put("author", dto.getAuthor());
        params.put("comment", dto.getComment());
        params.put("type", dto.getCommentType().name());

        return nameJdbcTemplate.queryForObject("INSERT INTO comments (entity_linked_id, comment_parent_id, author, comment, type) VALUES (:entity_linked_id, :comment_parent_id, :author, :comment, :type) RETURNING jsonb_build_object('id', id,'entityId', entity_linked_id,'parentId', comment_parent_id,'author', author,'comment', comment,'created', CAST(EXTRACT(EPOCH FROM created) * 1000 AS BIGINT));", params, String.class);
    }
}
