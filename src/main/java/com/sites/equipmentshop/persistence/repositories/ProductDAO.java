package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ProductDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public ProductDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }


    public String save(String productJson) {
        return nameJdbcTemplate.queryForObject("SELECT * FROM processing_product(:params)",
                ImmutableMap.of("params", productJson),
                String.class);
    }

    public String getProducts(Integer limit, Integer offset, String filter) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("limit", limit);
        params.put("offset", offset);
        params.put("filter", filter);
        return nameJdbcTemplate.queryForObject("SELECT * FROM get_products(:limit,:offset,:filter)",
                params, String.class);
    }

    public Boolean findProductByName(String name) {
        try {
            return nameJdbcTemplate.queryForObject("SELECT true FROM product WHERE name = :name",
                    ImmutableMap.of("name", name), Boolean.class);
        } catch (DataAccessException e) {
            return Boolean.FALSE;
        }
    }

    public void deleteById(Integer id) {
        nameJdbcTemplate.update("DELETE FROM product WHERE id = :id",
                ImmutableMap.of("id", id));
    }

    public Optional<String> findById(Integer id) {
        String product = nameJdbcTemplate.queryForObject("SELECT * FROM get_product(:id)",
                ImmutableMap.of("id", id), String.class);
        return Optional.ofNullable(product);
    }
    public List<String> findByIds(List<Integer> ids) {
        return nameJdbcTemplate.queryForList("SELECT * " +
                        "FROM ( " +
                        "         SELECT get_product(id::integer) products " +
                        "         FROM unnest(string_to_array(:ids, ',')) id " +
                        "     ) data " +
                        "WHERE products IS NOT NULL",
                ImmutableMap.of("ids", ids.stream().map(Object::toString).collect(Collectors.joining(","))), String.class);
    }

    public String getMaxMinPrice() {
        return nameJdbcTemplate.queryForObject("SELECT jsonb_build_object('max', max(coalesce(price, 0)),'min', min(coalesce(price, 0))) FROM product",
                Collections.emptyMap(), String.class);
    }

    public String getErrorProducts(Integer limit, Integer offset) {
        return nameJdbcTemplate.queryForObject("WITH data as ( " +
                        "    SELECT * " +
                        "    FROM ( " +
                        "             SELECT p.id, p.name, array_agg(pp.attributes_id) attr " +
                        "             FROM product_properties pp " +
                        "                      INNER JOIN product p on pp.product_id = p.id " +
                        "             GROUP BY 1, 2 " +
                        "         ) as producs " +
                        "    WHERE NOT 441 = ANY (attr) " +
                        ") " +
                        "SELECT jsonb_build_object('products', jsonb_agg(jsonb_build_object('id', id, 'name', name, 'reason', reason, 'url', url)), 'total', " +
                        "                          (SELECT count(id) FROM data)) " +
                        "FROM ( " +
                        "         SELECT id, name, reason, url " +
                        "         from data " +
                        "                  LEFT JOIN product_errors ON data.id = product_errors.product_id " +
                        "         ORDER BY id " +
                        "         OFFSET :offset LIMIT :limit " +
                        "     ) as tmp",
                ImmutableMap.of("limit", limit, "offset", offset), String.class);
    }
}
