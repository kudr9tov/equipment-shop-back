package com.sites.equipmentshop.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sites.equipmentshop.endpoints.dto.OrderDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.exception.ProductNotFoundException;
import com.sites.equipmentshop.persistence.entities.Product;
import com.sites.equipmentshop.service.OrderService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/order", consumes = {"*/*", "*", "application/json" }, produces = {"*/*", "application/json"})
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getOrders(@RequestParam(value = "limit", defaultValue = "15") Integer limit,
                                              @RequestParam(value = "skip", defaultValue = "0") Integer offset,
                                              @RequestParam(value = "filter", required = false) String filter) {
        return ResponseEntityUtils.responseEntity(orderService.getAllOrders(limit, offset, filter));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getProducts(@PathVariable Integer id) {
        return ResponseEntityUtils.responseEntity(orderService.getOrderById(id));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteProduct(@PathVariable Integer id){
        orderService.deleteOrderById(id);
    }

    @PutMapping
    public ResponseEntity<Object> updateOrder(@RequestBody OrderDTO orderDTO) throws JsonProcessingException {
        return ResponseEntityUtils.responseEntity(new ObjectMapper().writeValueAsString(orderService.createOrder(orderDTO)));
    }

    @PostMapping(value = "/{id}/{status}")
    public void updateStatusOrder(@PathVariable Integer id, @PathVariable String status) throws JsonProcessingException {
        orderService.updateOrderStatus(id, status);
    }

    @PostMapping()
    public ResponseEntity<Object> createOrder(@RequestBody OrderDTO orderDTO) throws JsonProcessingException {
        return ResponseEntityUtils.responseEntity(new ObjectMapper().writeValueAsString(orderService.createOrder(orderDTO)));
    }

}
