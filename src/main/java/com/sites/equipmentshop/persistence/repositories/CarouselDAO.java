package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.CarouselDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class CarouselDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public CarouselDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public String getSlideTree() {
        return nameJdbcTemplate.queryForObject("SELECT coalesce(jsonb_agg(slide), '[]') " +
                "FROM ( " +
                "         SELECT jsonb_build_object('id', id, " +
                "                                   'image', image, " +
                "                                   'title', title, " +
                "                                   'link', link, " +
                "                                   'step', step, " +
                "                                   'description', description) slide " +
                "         FROM carousel " +
                "         ORDER BY step " +
                "     ) as data", Collections.emptyMap(), String.class);
    }

    public String getSlide(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.queryForObject("SELECT jsonb_build_object('id', id, " +
                "                                    'image', image, " +
                "                                    'title', title, " +
                "                                    'link', link, " +
                "                                    'step', step, " +
                "                                    'description', description) " +
                "FROM carousel WHERE id = :id", params, String.class);
    }

    public void deleteSlide(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        nameJdbcTemplate.update("DELETE FROM carousel WHERE id = :id", params);
    }

    public String updateSlide(List<CarouselDTO> carouselDTO) {
        final String data = carouselDTO
                .stream()
                .map(f -> "(" + f.getId() + ",'" + f.getImage() + "', '"+ f.getTitle() + "', '" + f.getDescription() + "', '" + f.getLink() + "' , " + f.getStep() + ")")
                .collect(Collectors.joining(","));
        final String query = "WITH data as ( " +
                "    UPDATE carousel " +
                "        SET " +
                "            image = data.image, " +
                "            title = data.title, " +
                "            description = data.description, " +
                "            link = data.link, " +
                "            step = data.step " +
                "        FROM ( " +
                "            SELECT c.id," +
                "                   coalesce(f.image, c.image)        as image, " +
                "                   coalesce(f.title, c.title)        as title, " +
                "                   coalesce(f.description, c.description) as description, " +
                "                   coalesce(f.link, c.link) as link, " +
                "                   coalesce(f.step, c.step) as step " +
                "            FROM carousel c" +
                "            INNER JOIN (VALUES %s) f(id, image, title, description, link, step) ON c.id = f.id" +
                "        ) as data " +
                "        WHERE carousel.id = data.id" +
                "        RETURNING carousel.id, carousel.image, carousel.title, carousel.description, carousel.link, carousel.step " +
                ") " +
                "SELECT coalesce(jsonb_agg(jsonb_build_object('id', id,  " +
                "                          'image', image,  " +
                "                          'title', title," +
                "                          'description', description," +
                "                          'link', link," +
                "                          'step', step)), '[]')  " +
                "FROM data";
        return nameJdbcTemplate.queryForObject(String.format(query, data), Collections.emptyMap(), String.class);
    }

    public String saveSlide(List<CarouselDTO> carouselDTO) {
        final String data = carouselDTO
                .stream()
                .map(f -> "('" + f.getImage() + "', '" + f.getTitle()+ "', '" + f.getDescription() + "', '" + f.getLink() + "' , " + f.getStep() + ")")
                .collect(Collectors.joining(","));

        final String query = "WITH data as ( " +
                "    INSERT INTO carousel (image, title, description, link, step) " +
                "        SELECT * " +
                "        FROM (VALUES %s) f(image, title, description, link, step)" +
                "        RETURNING carousel.id, carousel.image, carousel.title, carousel.description, carousel.link, carousel.step" +
                ") " +
                "SELECT coalesce(jsonb_agg(jsonb_build_object('id', id, " +
                "                          'image', image, " +
                "                          'title', title, " +
                "                          'description', description, " +
                "                          'link', link," +
                "                          'step', step)), '[]') " +
                "FROM  data";
        return nameJdbcTemplate.queryForObject(String.format(query, data), Collections.emptyMap(), String.class);
    }

    public String getSlideByStep(Integer step) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("step", step);
        return nameJdbcTemplate.queryForObject("SELECT jsonb_build_object('id', id, " +
                "                                    'image', image, " +
                "                                    'title', title, " +
                "                                    'link', link, " +
                "                                    'step', step, " +
                "                                    'description', description) " +
                "FROM carousel WHERE step = :step", params, String.class);
    }
}
