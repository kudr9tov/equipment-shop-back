package com.sites.equipmentshop.support;

public class FileUploadResponse {

	private final String link;
	private final String fileName;

	public String getLink() {
		return link;
	}

	public String getFileName() {
		return fileName;
	}

	public FileUploadResponse(String link, String fileName) {
		this.link = link;
		this.fileName = fileName;
	}
	
}
