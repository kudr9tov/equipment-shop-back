package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.PriceRequestDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.service.PriceService;
import com.sites.equipmentshop.service.ProductService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/price", consumes = {"*/*", "*", "application/json"}, produces = {"*/*", "application/json"})
public class PriceController {

    private ProductService productService;
    private PriceService priceService;

    @Autowired
    public PriceController(ProductService productService,
                           PriceService priceService) {
        this.productService = productService;
        this.priceService = priceService;
    }

    @GetMapping(value = "maxmin")
    public ResponseEntity<Object> getSetPrice() {
        return ResponseEntityUtils.responseEntity(productService.getMaxMinPrice());
    }

    @PostMapping
    public void getProductPrice(@RequestBody PriceRequestDTO priceRequestDTO) {
        priceService.getProductPrice(priceRequestDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteProductPriceRequests(@PathVariable Integer id) {
        priceService.deleteProductPriceRequests(id);
    }

    @GetMapping
    public ResponseEntity<Object> getProductPriceRequests() {
        return ResponseEntityUtils.responseEntity(priceService.getProductPriceRequests());
    }


}
