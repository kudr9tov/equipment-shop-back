package com.sites.equipmentshop.endpoints.dto;

import java.util.List;

public class EmailDTO {
    private String message;
    private String subject;
    private Integer templateId = 1; //default value
    private List<Integer> emailIds;
    private List<String> emails;
    private Boolean sendTemplate = Boolean.FALSE;
    private Boolean sendAll = Boolean.FALSE;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public List<Integer> getEmailIds() {
        return emailIds;
    }

    public void setEmailIds(List<Integer> emailIds) {
        this.emailIds = emailIds;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public Boolean getSendTemplate() {
        return sendTemplate;
    }

    public void setSendTemplate(Boolean sendTemplate) {
        this.sendTemplate = sendTemplate;
    }

    public Boolean getSendAll() {
        return sendAll;
    }

    public void setSendAll(Boolean sendAll) {
        this.sendAll = sendAll;
    }
}
