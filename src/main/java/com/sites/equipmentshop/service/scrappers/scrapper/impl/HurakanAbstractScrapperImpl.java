package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class HurakanAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(HurakanAbstractScrapperImpl.class);

    Map<String, String> productByCategory = Maps.newHashMap();

    public HurakanAbstractScrapperImpl(NamedParameterJdbcTemplate productService, FTPUtils ftpUtils, ScrapperDAO scrapperDAO, ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    protected Map<String, String> searchCategories(String url, String category) {
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
        final Map<String, String> categories = Maps.newHashMap();
        if (Objects.nonNull(document)) {
            try {
                final Elements categoriesNode = document.getElementsByClass("category");
                for (Element categoryNode : categoriesNode) {
                    final Element link = categoryNode.getElementsByClass("link").first().child(0);
                    categories.putAll(searchCategories(link.attr("href"), link.text()));
                }
                if (MapUtils.isEmpty(categories)) {
                    final Map<String, String> tempCategories = document.getElementsByAttribute("data-filter")
                            .stream()
                            .collect(Collectors.toMap(f -> f.attr("data-filter").replaceAll("[.]", ""), Element::text));
                    tempCategories.remove("*");
                    tempCategories.forEach((key, value) -> {
                        final String href = document.getElementsByClass(key).first().attr("href");
                        Pattern p = Pattern.compile("(\\/[A-z-0-9]*)$");
                        Matcher m = p.matcher(href);
                        if (m.find()) {
                            productByCategory.put(m.replaceAll(StringUtils.EMPTY), key);
                            categories.put(m.replaceAll(StringUtils.EMPTY), value);
                        }
                    });
                    if (MapUtils.isNotEmpty(categories)) {
                        return categories;
                    }
                    return ImmutableMap.of(url, category);
                }
            } catch (Exception e) {

            }
        }
        return categories;
    }

    @Override
    protected Set<String> searchProductPages(String url) {
        return Sets.newHashSet(url);
    }

    @Override
    protected Map<String, String> searchProducts(String url) {
        try {
            final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
            if (Objects.nonNull(document)) {
                final Elements subCategoriesClass = document.getElementsByClass(productByCategory.get(url));
                final Map<String, String> products = Maps.newHashMap();
                for (Element product : subCategoriesClass) {

                    final String href = product.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    final String category = product.text();
                    products.put(category, href);
                }
                return products;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    protected void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {
                final Element dec = document.getElementsByClass("product_desc").first();
                final Element image = document.getElementsByClass("product_img").first().getElementsByTag("a").first();
                final Element tech = dec.getElementsByClass("agenda").first();
                String description = dec.child(0).outerHtml();

                Map<String, String> techMap = Maps.newHashMap();
                try {
                    for (Element row : tech.children()) {
                        try {
                            String key = row.child(0).text();
                            String value = row.child(1).text();
                            techMap.put(key, value);
                        } catch (Exception e) {
                            String key = "Другая информация";
                            String value = row.child(0).child(0).text();
                            techMap.put(key, value);

                            LOGGER.info(description);
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Can't parse techProperties. Type:{} message:{}", e.getClass().getSimpleName(), e.getMessage());
                }
                final Map<String, Object> properties = Maps.newHashMap();

                properties.put("country", 240);
                properties.put("brand", 2413);

                final Product product = new Product();
                product.setSiteUrl(getSite(), url);
                product.setProperties(properties);
                product.setTechProperties(techMap);
                product.setName(document.getElementsByClass("article__title").first().text());
                product.setDescription(description);
                final String imageUrl = image.attr("href");
                product.setImage(getSite().getDomain() + imageUrl);
                consumer.accept(product);
            }
        } catch (Exception e) {
            throw new IllegalStateException(getSite().getDomain() + url);
        }
    }

    @Override
    protected Site getSite() {
        return Site.HURAKAN;
    }
}
