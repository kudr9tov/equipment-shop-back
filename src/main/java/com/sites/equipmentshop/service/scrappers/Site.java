package com.sites.equipmentshop.service.scrappers;

public enum Site {
    ABAT("https://abat.ru", "/production"),
    POLAIR("https://www.polair-shop.ru", "/catalog"),
    MARIHOLOD("https://www.mariholod.com", "/products"),
    ATESY("https://atesy.ru", "/catalog"),
    POLUS("https://carboma.com", "/catalog"),
    PROMET("https://www.safe.ru", "/catalog/"),
    HURAKAN("https://hurakan.ru", "/catalog"),
    TD("https://t-d.ru", "/catalog"),
    METALCITY("https://www.metalcity.ru", "/catalog/"),
    CRISPY("https://cryspi.ru", "/products/"),
    HAIER("https://www.ic21.ru/", "/catalog/");

    final String domain;
    final String catalog;

    public String getDomain() {
        return domain;
    }

    public String getCatalog() {
        return catalog;
    }

    Site(String domain, String catalog) {
        this.domain = domain;
        this.catalog = catalog;
    }
}
