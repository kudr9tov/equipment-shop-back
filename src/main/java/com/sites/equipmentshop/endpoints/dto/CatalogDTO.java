package com.sites.equipmentshop.endpoints.dto;

public class CatalogDTO {
    private Integer id;
    private String name;
    private String catalogFileUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCatalogFileUrl() {
        return catalogFileUrl;
    }

    public void setCatalogFileUrl(String catalogFileUrl) {
        this.catalogFileUrl = catalogFileUrl;
    }
}
