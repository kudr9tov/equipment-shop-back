package com.sites.equipmentshop.endpoints.dto;

import java.util.List;

public class MultiCatalogDTO {
    private Integer brandId;
    private List<CatalogDTO> catalogs;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public List<CatalogDTO> getCatalogs() {
        return catalogs;
    }

    public void setCatalogs(List<CatalogDTO> catalogs) {
        this.catalogs = catalogs;
    }
}
