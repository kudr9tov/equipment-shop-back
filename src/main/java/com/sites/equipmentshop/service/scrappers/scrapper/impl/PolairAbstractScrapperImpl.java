package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.NumberUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class PolairAbstractScrapperImpl extends AbstractScrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(PolairAbstractScrapperImpl.class);

    private static Map<String, Map<String, Integer>> assosiation = Maps.newHashMap();

    static {
        assosiation.put("Шкафы шоковой заморозкиШокеры POLAIR-Light", ImmutableMap.of("category", 1238, "subCategory", 1618));
        assosiation.put("Шкафы шоковой заморозкиШокеры POLAIR-Grande", ImmutableMap.of("category", 1238, "subCategory", 1618));
        assosiation.put("Холодильные шкафыДля вина", ImmutableMap.of("category", 1238, "subCategory", 1619));
        assosiation.put("Холодильные шкафыPOLAIR BAKERY", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыСпециализированные шкафы", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыС металлическими дверьми", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыИз нержавеющей стали", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыСо стеклянными дверьми", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные шкафыФармацевтические", ImmutableMap.of("category", 1238, "subCategory", 1621));
        assosiation.put("Холодильные столыC охлаждаемой столешницей", ImmutableMap.of("category", 1238, "subCategory", 1634));
        assosiation.put("Холодильные столыСреднетемпературные", ImmutableMap.of("category", 1238, "subCategory", 1634));
        assosiation.put("Холодильные столыНизкотемпературные", ImmutableMap.of("category", 1238, "subCategory", 1634));
        assosiation.put("Холодильные столыДля приготовления пиццы", ImmutableMap.of("category", 1238, "subCategory", 1633));
        assosiation.put("Холодильные столыСаладетта", ImmutableMap.of("category", 1238, "subCategory", 1630));
        assosiation.put("Холодильные столыСо стеклянными дверьми", ImmutableMap.of("category", 1238, "subCategory", 1634));
        assosiation.put("Холодильные столыНастольные витрины", ImmutableMap.of("category", 1238, "subCategory", 1634));
        assosiation.put("Морозильные лариС плоскими раздвижными стеклами", ImmutableMap.of("category", 1238, "subCategory", 1627));
        assosiation.put("Морозильные лариС изогнутыми раздвижными стеклами", ImmutableMap.of("category", 1238, "subCategory", 1628));
        assosiation.put("Морозильные лариС глухими поднимающимися крышками", ImmutableMap.of("category", 1238, "subCategory", 1629));
        assosiation.put("Холодильные машиныМоноблоки", ImmutableMap.of("category", 1238, "subCategory", 1610));
        assosiation.put("Холодильные машиныСплит-системы", ImmutableMap.of("category", 1238, "subCategory", 1600));
        assosiation.put("Холодильные машиныДополнительное оборудование", ImmutableMap.of("category", 1238, "subCategory", 1627));
        assosiation.put("Барные  холодильные столы/шкафы", ImmutableMap.of("category", 1238, "subCategory", 2015));
        assosiation.put("Камеры шоковой заморозки", ImmutableMap.of("category", 1238, "subCategory", 1618));
        assosiation.put("Холодильные камерыСтандартные решения (80 и 100 мм)", ImmutableMap.of("category", 1238, "subCategory", 1597));
        assosiation.put("Холодильные камерыMinicella (80 мм)", ImmutableMap.of("category", 1238, "subCategory", 1597));
        assosiation.put("Холодильные камерыПрофессиональные решения (80 и 100 мм)", ImmutableMap.of("category", 1238, "subCategory", 1597));
        assosiation.put("Холодильные камерыСо стеклянным фронтом", ImmutableMap.of("category", 1238, "subCategory", 1597));
        assosiation.put("Холодильные камерыРасширительные пояса", ImmutableMap.of("category", 1238, "subCategory", 2012));
        assosiation.put("Холодильные камерыДверные блоки", ImmutableMap.of("category", 1238, "subCategory", 2012));
        assosiation.put("Холодильные камерыДополнительное оборудование", ImmutableMap.of("category", 1238, "subCategory", 2012));
    }

    public PolairAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                      FTPUtils ftpUtils,
                                      ScrapperDAO scrapperDAO,
                                      ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
        Map<String, String> categories = Maps.newHashMap();
        if (Objects.nonNull(document)) {
            categories = document.getElementsByClass("depth_1")
                    .stream()
                    .flatMap(f -> {
                        final Element depth_1_a = f.getElementsByClass("depth_1_a").first();
                        final Elements depth_2_a = f.getElementsByClass("depth_2_a");
                        if (depth_2_a.size() > 0) {
                            return depth_2_a.stream().collect(Collectors.toMap(w -> w.attr("href"), w -> String.format("%s -> %s", depth_1_a.text(), w.text()))).entrySet().stream();
                        }
                        final Map<String, String> simple = Maps.newHashMap();
                        simple.put(depth_1_a.attr("href"), depth_1_a.text());
                        return simple.entrySet().stream();
                    })
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }
        categories.keySet().removeIf(f -> f.equalsIgnoreCase("Модульные стеллажные системы"));
        return categories;
    }

    @Override
    public Set<String> searchProductPages(String url) {
        final Document document = ParserUtils.loadDocument(url);
        final Set<String> pages = Sets.newHashSet();

        if (Objects.nonNull(document)) {
            final Elements elementsByClass = document.getElementsByClass("modern-page-navigation");
            if (elementsByClass.size() > 0) {
                final Long maxPage = elementsByClass.first().children()
                        .stream()
                        .map(f -> {
                            try {
                                return Long.parseLong(f.text());
                            } catch (Exception e) {
                                return null;
                            }
                        })
                        .filter(Objects::nonNull)
                        .max(Comparator.naturalOrder())
                        .orElse(0L);
                for (long page = maxPage; page > 1; page--) {
                    pages.add(String.format("%s?PAGEN_1=%d", url, page));
                }
            }
        }
        pages.add(url);
        return pages;
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        try {
            final Document document = ParserUtils.loadDocument(url);
            if (Objects.nonNull(document)) {
                return document.getElementsByClass("b_product_list prod_col_3 clearfix")
                        .first()
                        .children()
                        .stream()
                        .filter(f -> f.tagName().equals("li"))
                        .map(f -> f.getElementsByTag("a").first())
                        .collect(Collectors.toMap(f -> UUID.randomUUID().toString(), f -> f.attr("href")));
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {
                final Product product = new Product();

                final Element item = document.getElementsByClass("b_product_detail clearfix").first();
                product.setSiteUrl(getSite(), url);

                String image = null;

                try {
                    image = item.getElementsByClass("pictures")
                            .first()
                            .children()
                            .stream()
                            .flatMap(f -> f.getElementsByTag("a").stream())
                            .findFirst().map(f -> f.attr("href")).orElse(null);
                } catch (Exception ignore) {

                }


                final String price = item.getElementsByClass("price").text()
                        .replaceAll("[^0-9]*", "");

                try {
                    product.setPrice(NumberUtils.parseNumber(price, Long.class));
                } catch (Exception ignore) {
                }

                try {
                    Map<String, String> techMap = Maps.newHashMap();
                    final Elements tech = document.getElementsByClass("full_chars")
                            .first()
                            .children();
                    for (int i = 2; i < tech.size(); i += 2) {
                        try {
                            String key = tech.get(i).text();
                            String value = tech.get(i + 1).text();
                            if (StringUtils.isNoneBlank(value)) {
                                techMap.put(key, value);
                            }
                        } catch (Exception e) {
                            LOGGER.error("", e);
                        }
                    }
                    product.setTechProperties(techMap);
                } catch (Exception ignored) {

                }


                final Map<String, Object> properties = Maps.newHashMap();
                properties.put("country", 240);
                properties.put("brand", 344);

                product.setName(document.getElementsByTag("title").first().text());
                product.setProperties(properties);

                try {
                    final String description = item
                            .getElementsByClass("description_01")
                            .first()
                            .child(0).outerHtml().replaceAll("style=\\\"[%: \\-#A-z0-9;]*\\\"", StringUtils.EMPTY)
                            .replaceAll("class=\\\"[%: \\-#A-z0-9;]*\\\"", StringUtils.EMPTY)
                            .replaceAll("role=\\\"[%: \\-#A-z0-9;]*\\\"", StringUtils.EMPTY);
                    product.setDescription(description);
                } catch (Exception ignored) {

                }

                if (StringUtils.isNoneBlank(image)) {
                    product.setImage(getSite().getDomain() + image);
                }
                consumer.accept(product);
            }
        } catch (Exception e) {
            throw new IllegalStateException(getSite().getDomain() + url);
        }
    }

    @Override
    protected Site getSite() {
        return Site.POLAIR;
    }
}
