package com.sites.equipmentshop.service.scrappers.models;

import java.util.concurrent.atomic.AtomicInteger;

public class ScrapperResult {
    private AtomicInteger categories = new AtomicInteger();
    private AtomicInteger foundedProducts = new AtomicInteger();
    private AtomicInteger newProducts = new AtomicInteger();
    private AtomicInteger priceProductsUpdated = new AtomicInteger();
    private Exception lastError;

    public AtomicInteger getCategories() {
        return categories;
    }

    public void setCategories(AtomicInteger categories) {
        this.categories = categories;
    }

    public AtomicInteger getFoundedProducts() {
        return foundedProducts;
    }

    public void setFoundedProducts(AtomicInteger foundedProducts) {
        this.foundedProducts = foundedProducts;
    }

    public AtomicInteger getNewProducts() {
        return newProducts;
    }

    public void setNewProducts(AtomicInteger newProducts) {
        this.newProducts = newProducts;
    }

    public AtomicInteger getPriceProductsUpdated() {
        return priceProductsUpdated;
    }

    public void setPriceProductsUpdated(AtomicInteger priceProductsUpdated) {
        this.priceProductsUpdated = priceProductsUpdated;
    }

    public Exception getLastError() {
        return lastError;
    }

    public void setLastError(Exception lastError) {
        this.lastError = lastError;
    }
}
