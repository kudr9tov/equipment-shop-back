package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.LinkDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class LinksDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public LinksDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public void createLink(List<LinkDTO> dto) {
        final SqlParameterSource[] sqlParameterSources = dto
                .stream()
                .map(link -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("name", link.getName());
                    parameters.addValue("seo_url", link.getSeoUrl());
                    parameters.addValue("content", link.getContent());
                    parameters.addValue("image_url", link.getImageUrl());
                    parameters.addValue("system", link.isSystem());
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        final String query = "INSERT INTO links (name, seo_url, content, image_url, system) " +
                " VALUES (:name, :seo_url, :content, :image_url, :system)";
        nameJdbcTemplate.batchUpdate(query, sqlParameterSources);
    }

    public void updateLink(List<LinkDTO> dto) {

        final SqlParameterSource[] sqlParameterSources = dto
                .stream()
                .map(link -> {
                    MapSqlParameterSource parameters = new MapSqlParameterSource();
                    parameters.addValue("id", link.getId());
                    parameters.addValue("name", link.getName());
                    parameters.addValue("seo_url", link.getSeoUrl());
                    parameters.addValue("content", link.getContent());
                    parameters.addValue("image_url", link.getImageUrl());
                    parameters.addValue("system", link.isSystem());
                    return parameters;
                })
                .toArray(SqlParameterSource[]::new);
        final String query = "UPDATE links " +
                "SET name      = :name, " +
                "    seo_url   = :seo_url, " +
                "    content   = :content, " +
                "    image_url = :image_url, " +
                "    system = :system " +
                "WHERE links.id = cast(:id as integer) ";
        nameJdbcTemplate.batchUpdate(query, sqlParameterSources);
    }

    public String getLink(Integer id) {
        final String query = "SELECT jsonb_build_object( " +
                "               'id', id, " +
                "               'name', name, " +
                "               'seoUrl', seo_url, " +
                "               'content', content, " +
                "               'imageUrl', image_url, " +
                "               'system', system, " +
                "               'created', CAST(EXTRACT(EPOCH FROM created) * 1000 AS BIGINT) " +
                "           ) " +
                "FROM links " +
                "WHERE id = :id";
        return nameJdbcTemplate.queryForObject(query, ImmutableMap.of("id", id), String.class);
    }

    public String getLinks(List<Integer> ids) {
        final String query = "SELECT jsonb_agg(jsonb_build_object( " +
                "               'id', id, " +
                "               'name', name, " +
                "               'seoUrl', seo_url, " +
                "               'content', content, " +
                "               'imageUrl', image_url, " +
                "               'system', system, " +
                "               'created', CAST(EXTRACT(EPOCH FROM created) * 1000 AS BIGINT) " +
                "           )) " +
                "FROM links " +
                "WHERE id = ANY(string_to_array(:ids, ','))";
        return nameJdbcTemplate.queryForObject(query, ImmutableMap.of("ids", ids.stream().map(Objects::toString).collect(Collectors.joining(","))), String.class);
    }

    public String getLinks() {
        final String query = "SELECT jsonb_agg(jsonb_build_object( " +
                "               'id', id, " +
                "               'name', name, " +
                "               'seoUrl', seo_url, " +
                "               'content', content, " +
                "               'imageUrl', image_url, " +
                "               'system', system, " +
                "               'created', CAST(EXTRACT(EPOCH FROM created) * 1000 AS BIGINT) " +
                "           )) " +
                "FROM links ";
        return nameJdbcTemplate.queryForObject(query, Maps.newHashMap(), String.class);
    }

    public void deleteLink(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        final String query = "DELETE FROM links WHERE id = :id";
        nameJdbcTemplate.update(query, params);
    }
}
