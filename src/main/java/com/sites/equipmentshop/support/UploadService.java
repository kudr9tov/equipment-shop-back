package com.sites.equipmentshop.support;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class UploadService {

    private String uploadFolder;

    @Value("${sem.platform.admin.download.url}")
    private String downloadUrl;

    public UploadService(@Value("${sem.platform.admin.upload.folder}") String uploadFolder) {
        this.uploadFolder = uploadFolder;
        File directory = new File(uploadFolder);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    public FileUploadResponse saveUploadedFiles(MultipartFile file) {
        try {
            String uploadedFileName = RandomStringUtils.randomAlphanumeric(5) + "_" + file.getOriginalFilename();
            final String safeFileName = getSafeFileName(uploadedFileName);
            Path path = Paths.get(uploadFolder + safeFileName);
            Files.copy(file.getInputStream(), path);
            return new FileUploadResponse(String.format("%s%s", downloadUrl, safeFileName), uploadedFileName);
        } catch (IOException e) {
            throw new FileUploadError(e);
        }
    }

    private String getSafeFileName(String fileName) {
        return fileName.replaceAll("[%#?]", StringUtils.EMPTY);
    }
}
