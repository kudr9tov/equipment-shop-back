package com.sites.equipmentshop.security.config;

import com.sites.equipmentshop.security.filters.AuthLogoutFilter;
import com.sites.equipmentshop.security.filters.AuthRestoreFilter;
import com.sites.equipmentshop.security.filters.CorsFilter;
import com.sites.equipmentshop.security.filters.JWTAuthenticationFilter;
import com.sites.equipmentshop.security.filters.JWTLoginFilter;
import com.sites.equipmentshop.security.persistence.entities.UserRoles;
import com.sites.equipmentshop.security.persistence.repositories.JPAUserRepository;
import com.sites.equipmentshop.security.services.PlatformAuthenticationProvider;
import com.sites.equipmentshop.security.services.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.CorsConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.session.SessionManagementFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String ADMIN_ROLE = "ADMIN";
    private final PlatformAuthenticationProvider authenticationProvider;
    private final TokenAuthenticationService authenticationService;
    private final JPAUserRepository userRepo;
    private final Boolean isEnabled;

    @Autowired
    public WebSecurityConfig(
            PlatformAuthenticationProvider authenticationProvider,
            TokenAuthenticationService authenticationService,
            JPAUserRepository userRepo,
            @Value("${security.enabled}") Boolean isEnabled) {
        this.userRepo = userRepo;
        this.isEnabled = isEnabled;
        this.authenticationService = authenticationService;
        this.authenticationProvider = authenticationProvider;
    }

    @Bean
    public JWTAuthenticationFilter jwtAuthenticationFilter() {
        return new JWTAuthenticationFilter(authenticationService, userRepo);
    }

    @Bean
    public JWTLoginFilter jwtLoginFilter() throws Exception {
        return new JWTLoginFilter("/api/users/login", authenticationManager(), authenticationService, userRepo);
    }

    @Bean
    public AuthRestoreFilter rememberCookieFilter() {
        return new AuthRestoreFilter("/api/auth/restore", authenticationService, userRepo);
    }

    @Bean
    public AuthLogoutFilter logoutFilter() {
        return new AuthLogoutFilter("/api/auth/logout", authenticationService);
    }

    @Bean
    CorsFilter corsFilter() {
        return new CorsFilter();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if (Boolean.TRUE.equals(isEnabled)) {
            http.csrf().disable()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.POST, "/api/users/login").permitAll()
                    .antMatchers(HttpMethod.POST, "/api/users/registration").permitAll()
                    .antMatchers(HttpMethod.POST, "/api/users/password/**").permitAll()
                    .antMatchers(HttpMethod.GET, "/api/users/current-user").permitAll()
                    .antMatchers("/api/users").hasRole(UserRoles.ADMIN.name())
                    .antMatchers(HttpMethod.GET,"/api/carousel").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/catalog/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/catalog").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/directory/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/directory").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/links/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/links").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/price/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/price").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/comments").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/comments/**").permitAll()
                    .antMatchers(HttpMethod.POST,"/api/comments/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/products/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/api/products").permitAll()
                    .antMatchers(HttpMethod.POST,"/api/order").permitAll()
                    .antMatchers(HttpMethod.POST,"/api/price").permitAll()
                    .antMatchers("/api/**").authenticated()
                    .anyRequest().permitAll()
                    .and()
                    .addFilterBefore(corsFilter(), UsernamePasswordAuthenticationFilter.class)
                    .addFilterAfter(jwtAuthenticationFilter(), CorsFilter.class)
                    .addFilterBefore(rememberCookieFilter(), JWTAuthenticationFilter.class)
                    .addFilterBefore(logoutFilter(), JWTAuthenticationFilter.class);
        } else {
            http.csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/**").permitAll();
        }
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
