package com.sites.equipmentshop.persistence.entities;

public enum OrderStatus {
    CREATED(1),
    PROCESSING(2),
    WAITING_PAYMENT(3),
    COMPLETED(4),
    DELETED(0);

    private final Integer status;

    OrderStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }
}
