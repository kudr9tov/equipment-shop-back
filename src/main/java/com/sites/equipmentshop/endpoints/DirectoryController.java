package com.sites.equipmentshop.endpoints;

import com.sites.equipmentshop.endpoints.dto.DirectoryDTO;
import com.sites.equipmentshop.endpoints.dto.MoveCategoriesDTO;
import com.sites.equipmentshop.endpoints.dto.MultiDirectoryDTO;
import com.sites.equipmentshop.endpoints.responses.ResponseEntityUtils;
import com.sites.equipmentshop.service.DirectoryService;
import com.sites.equipmentshop.utils.APIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = APIConstants.API_ROOT + "/directory", consumes = {"*/*", "*", "application/json" }, produces = {"*/*", "application/json"})
public class DirectoryController {

    private DirectoryService directoryService;

    @Autowired
    public DirectoryController(DirectoryService directoryService) {
        this.directoryService = directoryService;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getAllConfigs() {
        return ResponseEntityUtils.responseEntity(directoryService.getAllConfigs());
    }

    @GetMapping(value = "/category")
    public ResponseEntity<Object> categoryConfig() {
        return ResponseEntityUtils.responseEntity(directoryService.getCategoryConfig());
    }

    @GetMapping(value = "/countries")
    public ResponseEntity<Object> countriesConfig() {
        return ResponseEntityUtils.responseEntity(directoryService.getCountriesConfig());
    }

    @GetMapping(value = "/brands")
    public ResponseEntity<Object> brandConfig() {
        return ResponseEntityUtils.responseEntity(directoryService.getBrandConfig());
    }

    @GetMapping(value = "/cert")
    public ResponseEntity<Object> certConfig() {
        return ResponseEntityUtils.responseEntity(directoryService.getCertConfig());
    }

    @PostMapping()
    public void addDirectoryValue(@RequestBody DirectoryDTO directoryDTO){
        directoryService.addDirectoryValue(directoryDTO);
    }

    @PostMapping(value = "/batch")
    public ResponseEntity<Object> addDirectoryValues(@RequestBody MultiDirectoryDTO dto){
        return ResponseEntityUtils.responseEntity(directoryService.addDirectoryValues(dto));
    }

    @PostMapping(value = "/category/move")
    public void moveCategories(@RequestBody MoveCategoriesDTO dto){
        directoryService.moveCategories(dto);
    }

    @PutMapping(value = "/batch")
    public void updateDirectoryValues(@RequestBody MultiDirectoryDTO dto){
        directoryService.updateDirectoryValues(dto);
    }

    @PutMapping()
    public void updateDirectoryValue(@RequestBody DirectoryDTO directoryDTO){
        directoryService.updateDirectoryValue(directoryDTO);
    }

    @PostMapping(value = "/delete")
    public void removeDirectoryValue(@RequestBody DirectoryDTO directoryDTO){
        directoryService.deleteDirectoryValue(directoryDTO);
    }
}
