package com.sites.equipmentshop.endpoints.dto;

import com.sites.equipmentshop.service.enums.Directory;

import java.util.Set;

public class MultiDirectoryDTO {
    private Integer parentId;
    private Directory type;
    private Integer level;
    private Set<EntryDTO> entries;

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Directory getType() {
        return type;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public void setType(Directory type) {
        this.type = type;
    }

    public Set<EntryDTO> getEntries() {
        return entries;
    }

    public void setEntries(Set<EntryDTO> entries) {
        this.entries = entries;
    }


}
