package com.sites.equipmentshop.utils;

import com.sites.equipmentshop.endpoints.dto.ProductOrderDTO;
import com.sites.equipmentshop.persistence.entities.Client;
import com.sites.equipmentshop.persistence.entities.Product;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class HTMLUtils {
    public static String convertProductToHtml(List<Product> products, List<ProductOrderDTO> productsCount) {
        final Map<Integer, Integer> map = productsCount
                .stream()
                .collect(Collectors.toMap(ProductOrderDTO::getProductId, ProductOrderDTO::getQuantity));
        return products
                .stream()
                .map(product -> HTMLUtils.convertProductToHtml(product, map.get(product.getId())))
                .collect(Collectors.joining("\n"));
    }

    public static String convertProductToHtml(Product product, Object quantity) {
        return new StringBuilder().append("<tr style=\"margin-bottom: 20px\" valign=\"top\">")
                .append("<td valign = \"center\" align=\"left\" style=\"width: 80%; font-size: 12pt;\"><a href=\"https://gktorg.ru/catalog/product/").append(product.getId()).append("\">").append(StringUtils.defaultString(product.getName())).append("</a></td>")
                .append(Objects.nonNull(quantity) ? "<td valign = \"center\" style=\"width: 10%; font-size: 12pt;\">" + quantity + "</td>" : "")
                .append("<td valign = \"center\" align = \"right\" style=\"width: 10%; font-size: 12pt;\">").append(ObjectUtils.defaultIfNull(product.getPrice(), "")).append("</td>")
                .append("</tr>")
                .toString();
    }

    public static String convertClientToHtml(Client client, String comment) {
        return new StringBuilder()
                .append("<div style=\"width:100%;text-align:left;margin:10px 0;\">")
                .append("<div style=\"font-size: 14pt;\"><b>Информация о клиенте</b></div>")
                .append("<div style=\"font-size: 14pt;\"><b>Имя:</b> ").append(StringUtils.defaultString(client.getName())).append("</div>")
                .append("<div style=\"font-size: 14pt;\"><b>Телефон:</b> ").append(StringUtils.defaultString(client.getPhone())).append("</div>")
                .append("<div style=\"font-size: 14pt;\"><b>Email:</b> ").append(StringUtils.defaultString(client.getEmail())).append("</div>")
                .append("<div style=\"font-size: 14pt;\"><b>Комментарий:</b> ").append(StringUtils.defaultString(comment)).append("</div>")
                .append("</div>")
                .toString();
    }

    public static String getProductsHeader(Boolean order) {
        return new StringBuilder()
                .append("<tr style=\"margin-bottom: 20px\" valign=\"top\">")
                .append("<td align=\"left\" style=\"width: 80%; font-size: 16pt;\">Товар</td>")
                .append(order ? "<td style=\"width: 10%; font-size: 16pt;\">Количество</td>" : "")
                .append("<td align = \"right\" style=\"width: 10%; font-size: 16pt;\">Цена</td>")
                .append("</tr>")
                .toString();
    }
}
