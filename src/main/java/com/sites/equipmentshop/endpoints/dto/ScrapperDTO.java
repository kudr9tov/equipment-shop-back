package com.sites.equipmentshop.endpoints.dto;

import com.sites.equipmentshop.service.scrappers.Site;
import org.apache.commons.lang3.EnumUtils;

public class ScrapperDTO {
    private Site site;
    private boolean isUpdateCategories = Boolean.FALSE;
    private boolean isUpdateProducts = Boolean.FALSE;
    private boolean isUpdateProductsPrice = Boolean.FALSE;

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public void setSite(String site) {
        this.site = EnumUtils.getEnumIgnoreCase(Site.class, site);
    }

    public boolean isUpdateCategories() {
        return isUpdateCategories;
    }

    public void setUpdateCategories(boolean updateCategories) {
        isUpdateCategories = updateCategories;
    }

    public boolean isUpdateProducts() {
        return isUpdateProducts;
    }

    public void setUpdateProducts(boolean updateProducts) {
        isUpdateProducts = updateProducts;
    }

    public boolean isUpdateProductsPrice() {
        return isUpdateProductsPrice;
    }

    public void setUpdateProductsPrice(boolean updateProductsPrice) {
        isUpdateProductsPrice = updateProductsPrice;
    }
}
