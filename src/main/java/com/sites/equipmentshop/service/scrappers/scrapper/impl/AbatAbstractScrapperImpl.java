package com.sites.equipmentshop.service.scrappers.scrapper.impl;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.ScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.ScrapperDAO;
import com.sites.equipmentshop.service.scrappers.Site;
import com.sites.equipmentshop.service.scrappers.models.Product;
import com.sites.equipmentshop.service.scrappers.scrapper.AbstractScrapper;
import com.sites.equipmentshop.service.scrappers.utils.FTPUtils;
import com.sites.equipmentshop.service.scrappers.utils.ParserUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

public class AbatAbstractScrapperImpl extends AbstractScrapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbatAbstractScrapperImpl.class);

    public AbatAbstractScrapperImpl(NamedParameterJdbcTemplate productService,
                                    FTPUtils ftpUtils,
                                    ScrapperDAO scrapperDAO,
                                    ScrapperDTO scrapperDTO) {
        super(productService, ftpUtils, scrapperDAO, scrapperDTO);
    }

    @Override
    public Map<String, String> searchCategories(String url, String category) {
        final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
        if (Objects.nonNull(document)) {
            final Elements categoriesClass = document.getElementsByClass("prod-categories");
            final Map<String, String> categories = Maps.newHashMap();

            for (Element vCategory : categoriesClass.first().children()) {
                final Element link = vCategory.getElementsByTag("a").first();
                final String href = link.attr("href")
                        .replace(getSite().getDomain(), StringUtils.EMPTY);

                categories.put(href, link.child(0).text());
            }
            return categories;
        }
        return Collections.emptyMap();
    }

    @Override
    public Set<String> searchProductPages(String url) {
        return Sets.newHashSet(url);
    }

    @Override
    public Map<String, String> searchProducts(String url) {
        try {
            final Document document = ParserUtils.loadDocument(getSite().getDomain() + url);
            if (Objects.nonNull(document)) {
                final Element categoriesClass = document.getElementsByClass("production-catalog").first();
                final Elements subCategoriesClass = categoriesClass.getElementsByClass("prod-list");
                final Map<String, String> products = Maps.newHashMap();

                for (Element product : subCategoriesClass.first().child(0).children()) {
                    final Element link = product.getElementsByClass("image").first().getElementsByTag("a").first();
                    final String href = link.attr("href")
                            .replace(getSite().getDomain(), StringUtils.EMPTY);
                    final String category = product.getElementsByClass("name").first().text().replace("Категория:", StringUtils.EMPTY)
                            .replaceAll("\\u00a0", StringUtils.EMPTY)
                            .trim();
                    products.put(category, href);
                }
                return products;
            }
            return Collections.emptyMap();
        } catch (Exception e) {
            return Collections.emptyMap();
        }

    }

    @Override
    public void getProductInfo(String url, Consumer<Product> consumer) {
        final Document document = getProductUrl(url);
        try {
            if (Objects.nonNull(document)) {
                final Element item = document.getElementsByClass("product-info").first();
                final Element info = item.getElementsByClass("info").first();
                final Element image = info.getElementsByClass("product-gallery").first().child(0).child(0);
                final Element tech = info.getElementsByClass("product-details").first()
                        .getElementsByTag("table").first()
                        .getElementsByTag("tbody").first();
                String description = item.getElementsByClass("description").first().text();

                Map<String, String> techMap = Maps.newHashMap();
                for (Element row : tech.children()) {
                    try {
                        String key = row.child(0).text();
                        String value = row.child(1).text();
                        techMap.put(key, value);
                    } catch (Exception e) {
                        String key = "Другая информация";
                        String value = row.child(0).child(0).text();
                        techMap.put(key, value);

                        LOGGER.info(description);
                    }
                }
                final String vendorCode = techMap.remove("Артикул");
                final Map<String, Object> properties = Maps.newHashMap();

                properties.put("country", 242);
                properties.put("brand", 247);
                properties.put("vendorCode", vendorCode);

                final Product product = new Product();
                product.setSiteUrl(getSite(), url);
                product.setProperties(properties);
                product.setTechProperties(techMap);
                product.setName(item.getElementsByClass("title").first().text());
                product.setDescription(description);
                final String imageUrl = image.attr("href");
                product.setImage(getSite().getDomain() + imageUrl);
                consumer.accept(product);
            }
        } catch (Exception e) {
            throw new IllegalStateException(getSite().getDomain() + url);
        }
    }

    @Override
    protected Site getSite() {
        return Site.ABAT;
    }
}
