package com.sites.equipmentshop.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sites.equipmentshop.endpoints.dto.SchedulingEmailDTO;
import com.sites.equipmentshop.endpoints.dto.SchedulingScrapperDTO;
import com.sites.equipmentshop.persistence.repositories.SchedulingDAO;
import com.sites.equipmentshop.scheduling.TaskJob;
import com.sites.equipmentshop.service.enums.ScheduleJobType;
import com.sites.equipmentshop.service.scrappers.models.ScheduleJobModel;
import org.apache.commons.lang3.StringUtils;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

@Component
public class SchedulingService {

    private final SchedulerFactoryBean schedulerFactory;
    private final SchedulingDAO schedulingDAO;
    public static final String PROPERTIES_KEY = "props";
    public static final String APP_KEY = "app";


    @Autowired
    public SchedulingService(@Qualifier("schedulerFactoryBean") final SchedulerFactoryBean schedulerFactory,
                             final SchedulingDAO schedulingDAO) {
        this.schedulerFactory = schedulerFactory;
        this.schedulingDAO = schedulingDAO;
    }

    public void scheduleScrapper(SchedulingScrapperDTO dto) {
        try {
            final String scheduleId = schedule(new ObjectMapper().writeValueAsString(dto.getDto()), ScheduleJobType.SCRAPPER.name(), dto.getCron(), dto.getDto().getSite().name());
            schedulingDAO.createScrapperJob(dto, scheduleId);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }


    public void scheduleMail(SchedulingEmailDTO dto) {
        try {
            final String scheduleId = schedule(new ObjectMapper().writeValueAsString(dto.getDto()), ScheduleJobType.MAIL.name(), dto.getCron(), null);
            schedulingDAO.createMailJob(dto, scheduleId);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    private String schedule(String parameters, String jobType, String cron, String scrapperType) {
        Map<JobDetail, Set<? extends Trigger>> jobMap = Maps.newHashMap();
        final String id = UUID.randomUUID().toString();
        JobKey key = new JobKey(id, String.format("%s-%s", jobType, StringUtils.defaultString(scrapperType, jobType)));
        JobDetail details = JobBuilder.newJob(TaskJob.class)
                .withIdentity(key)
                .usingJobData(PROPERTIES_KEY, parameters)
                .usingJobData(APP_KEY, jobType)
                .storeDurably(true)
                .build();
        Trigger trigger = buildJobTrigger(jobType, cron);
        jobMap.put(details, Sets.newHashSet(trigger));
        try {
            schedulerFactory.getScheduler().scheduleJobs(jobMap, true);
            return id;
        } catch (SchedulerException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }


    private Trigger buildJobTrigger(String jobType, String cron) {
        TriggerKey triggerKey = new TriggerKey(UUID.randomUUID().toString(), jobType);
        return TriggerBuilder.newTrigger()
                .withIdentity(triggerKey)
                .withSchedule(CronScheduleBuilder.cronSchedule(cron)
                        .withMisfireHandlingInstructionDoNothing()
                        .inTimeZone(TimeZone.getDefault()))
                .build();
    }

    public void deleteJob(Integer id) {
        final ScheduleJobModel scheduleJob = schedulingDAO.getScheduleJobId(id);
        final JobKey jobKey = new JobKey(scheduleJob.getScheduleId(), String.format("%s-%s", scheduleJob.getJobType(), StringUtils.defaultString(scheduleJob.getScrapperName(), scheduleJob.getJobType())));
        try {
            final boolean removed = schedulerFactory.getScheduler().deleteJob(jobKey);
            if (removed) {
                schedulingDAO.deleteJob(id);
            } else {
                throw new IllegalStateException("Job not removed");
            }
        } catch (SchedulerException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    public String getScrapperJob(Integer siteId) {
        return schedulingDAO.getScrapperJob(siteId);
    }

    public String getMailJob() {
        return schedulingDAO.getEmailJob();
    }
}
