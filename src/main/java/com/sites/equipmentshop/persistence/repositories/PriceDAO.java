package com.sites.equipmentshop.persistence.repositories;

import com.google.common.collect.Maps;
import com.sites.equipmentshop.endpoints.dto.PriceRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Map;

@Repository
public class PriceDAO {

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public PriceDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }


    public String getProductPriceRequests() {
        return nameJdbcTemplate.queryForObject("SELECT jsonb_agg(jsonb_build_object( " +
                "        'id', pr.id, " +
                "        'product', jsonb_build_object('id', p.id, " +
                "                                      'name', p.name), " +
                "        'userInfo', jsonb_build_object('phone', pr.phone, " +
                "                                       'email', pr.email, " +
                "                                       'city', pr.city, " +
                "                                       'name', pr.name), " +
                "        'comment', pr.comment," +
                "        'created', CAST(EXTRACT(EPOCH FROM pr.updated) * 1000 AS BIGINT)" +
                "    )) " +
                "FROM price_request pr " +
                "         INNER JOIN product p on pr.product_id = p.id", Collections.emptyMap(), String.class);
    }

    public void saveProductPrice(PriceRequestDTO requestDTO) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("productId", requestDTO.getProductId());
        params.put("phone", requestDTO.getPhone());
        params.put("email", requestDTO.getEmail());
        params.put("city", requestDTO.getCity());
        params.put("name", requestDTO.getName());
        params.put("comment", requestDTO.getComment());
        nameJdbcTemplate.update("INSERT INTO price_request (product_id, phone, email, city, name, comment) " +
                "VALUES (:productId, :phone, :email, :city, :name, :comment)", params);
    }

    public void deleteProductPriceRequests(Integer id) {
        final Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        nameJdbcTemplate.update("DELETE FROM price_request WHERE id = :id", params);
    }
}
