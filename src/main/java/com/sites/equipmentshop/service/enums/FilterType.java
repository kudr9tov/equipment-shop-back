package com.sites.equipmentshop.service.enums;

public enum FilterType {
    ASC, DESC, QUERY
}
