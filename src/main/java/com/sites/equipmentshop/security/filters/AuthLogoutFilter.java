package com.sites.equipmentshop.security.filters;

import com.sites.equipmentshop.security.services.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthLogoutFilter extends GenericFilterBean {

    private RequestMatcher requestMatcher;
    private TokenAuthenticationService authenticationService;

    private static final Logger log = LoggerFactory.getLogger(AuthLogoutFilter.class);

    public AuthLogoutFilter(String url, TokenAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
        requestMatcher = new AntPathRequestMatcher(url, HttpMethod.GET.toString());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (requestMatcher.matches((HttpServletRequest) request)) {
            ((HttpServletResponse) response).addCookie(authenticationService.deleteAccessCookie());
        } else {
            chain.doFilter(request, response);
        }
    }

}
