package com.sites.equipmentshop.service.scrappers.utils;

import com.sites.equipmentshop.service.scrappers.NoMoreAttemptsException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ParserUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ParserUtils.class);
    private static AtomicInteger ATTEMPTS = new AtomicInteger(0);

    public static Document loadDocument(String url) {
        try {
            randomSleep(url);
            final Connection connection = Jsoup
                    .connect(url)
                    .timeout(10000)
                    .followRedirects(Boolean.TRUE);
            final String s = connection.get().outerHtml();
            return Jsoup.parse(s);
        } catch (IOException e) {
            if (ATTEMPTS.incrementAndGet() == 30) {
                LOGGER.error("NO_MORE_ATTEMPTS", e);
                ATTEMPTS.set(0);
                throw new NoMoreAttemptsException("NO_MORE_ATTEMPTS", e);
            }
            LOGGER.error(url, e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return null;
    }

    private static void randomSleep(String url) throws InterruptedException {
        final long timeout = (long) (Math.random() * 2) + 3;
        LOGGER.debug("Sleep {}s.", timeout);
        TimeUnit.SECONDS.sleep(1);
        LOGGER.debug("Scraping: {}", url);
    }
}
