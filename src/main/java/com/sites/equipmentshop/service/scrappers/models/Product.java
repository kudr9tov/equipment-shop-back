package com.sites.equipmentshop.service.scrappers.models;

import com.sites.equipmentshop.service.scrappers.Site;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class Product {
    private Integer id;
    private String name;
    private String image;
    private String description;
    private Long price;
    private Integer discount;
    private Map<String, String> techProperties;
    private Map<String, Object> properties;
    private String siteUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Map<String, String> getTechProperties() {
        return techProperties;
    }

    public void setTechProperties(Map<String, String> techProperties) {
        this.techProperties = techProperties;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(Site site, String siteUrl) {
        if (siteUrl.contains(site.getDomain())) {
            this.siteUrl = siteUrl;
        } else {
            this.siteUrl = site.getDomain() + siteUrl;
        }
    }

    public boolean valid() {

        return StringUtils.isNotBlank(name)
                && MapUtils.isNotEmpty(properties);
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", techProperties=" + techProperties +
                '}';
    }
}
