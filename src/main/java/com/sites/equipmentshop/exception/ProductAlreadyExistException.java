package com.sites.equipmentshop.exception;

public class ProductAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ProductAlreadyExistException(String name) {
        super(String.format("Product `%s` already exist", name));
    }

}
